<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsBlastTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_contacts',function(Blueprint $table){
           $table->increments('id');
           $table->string('phone')->nullable();
           $table->string('email')->nullable();
           $table->string('name')->nullable();
            $table->integer('store_id')->unsigned();
            $table->foreign('store_id')->references('id')->on('stores');
           $table->timestamps();
           $table->unique(['phone','email','store_id']);
        });

        Schema::create('blast_templates',function(Blueprint $table){
            $table->increments('id');
            $table->string('language')->nullable();
            $table->text('content');
            $table->text('extra_data')->nullable();
            $table->integer('store_id')->unsigned();
            $table->tinyInteger('type')->unsigned()->nullable();
            $table->foreign('store_id')->references('id')->on('stores');
            $table->timestamp('last_sent_at')->nullable();
            $table->timestamps();
        });

        Schema::create('blast_contact',function(Blueprint $table){
            $table->increments('id');
            $table->integer('blast_template_id')->unsigned();
            $table->foreign('blast_template_id')->references('id')->on('blast_templates');
            $table->integer('client_contact_id')->unsigned();
            $table->foreign('client_contact_id')->references('id')->on('client_contacts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blast_contact');
        Schema::dropIfExists('blast_templates');
        Schema::dropIfExists('client_contacts');
    }
}

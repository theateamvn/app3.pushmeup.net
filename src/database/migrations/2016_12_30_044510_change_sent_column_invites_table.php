<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeSentColumnInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invites', function (Blueprint $table) {
            $table->boolean('email_sent')->nullable()->change();
            $table->boolean('phone_sent')->nullable()->change();
            $table->boolean('mms_sent')->nullable()->change();
        });
        DB::table('invites')->update([
            'email_sent' => null,
            'phone_sent' => null,
            'mms_sent' => null
        ]);
        Schema::table('invites', function (Blueprint $table) {
            $table->dateTime('email_sent')->default(null)->nullable()->change();
            $table->dateTime('phone_sent')->default(null)->nullable()->change();
            $table->dateTime('mms_sent')->default(null)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('invites')->update([
            'email_sent' => null,
            'phone_sent' => null,
            'mms_sent' => null
        ]);
        Schema::table('invites', function (Blueprint $table) {
            $table->boolean('email_sent')->default(false)->change();
            $table->boolean('phone_sent')->default(false)->change();
            $table->boolean('mms_sent')->default(false)->change();
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemakeReviewsTableIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reviews', function (Blueprint $table) {
            $table->dropForeign(['store_id']);
            $table->dropUnique(['store_id','provider_name','author','rating']);
            $table->string('review_unique_id');
            $table->foreign('store_id')->references('id')->on('stores');
        });
        \App\Models\Review::all()->each(function(\App\Models\Review $review){
            $key = implode('-',[
                $review->store_id,
                $review->content,
                $review->author,
                $review->rating,
                $review->published_at
            ]);
            $review->review_unique_id = hash('md5',$key);
            $review->save();
        });
        Schema::table('reviews', function (Blueprint $table) {
            $table->unique('review_unique_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reviews', function (Blueprint $table) {
            $table->dropColumn('review_unique_id');
        });
    }
}

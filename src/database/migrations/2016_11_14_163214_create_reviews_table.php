<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id')->unsigned();
            $table->foreign('store_id')->references('id')->on('stores');
            $table->string('reviewer_id')->nullable();
            $table->text('content')->nullable();
            $table->float('rating')->unsigned();
            $table->string('author');
            $table->dateTime('published_at');
            $table->string('provider_name',30);
            $table->string('url')->nullable();
            $table->timestamps();

            $table->unique(['store_id','provider_name','author','rating']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}

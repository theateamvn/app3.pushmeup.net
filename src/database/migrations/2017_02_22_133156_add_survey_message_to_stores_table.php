<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSurveyMessageToStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores',function (Blueprint $table) {
            $table->text('survey_message_1')->nullable();
            $table->text('survey_message_2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores',function (Blueprint $table) {
            $table->dropColumn('survey_message_1');
            $table->dropColumn('survey_message_2');
        });
    }
}

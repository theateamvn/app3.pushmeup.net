<?php

use App\Models\InviteMessage;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    private $faker;

    /**
     * UsersTableSeeder constructor.
     */
    public function __construct()
    {
        $this->faker = Faker\Factory::create();
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('store_users')->delete();
        DB::table('users')->delete();
        DB::table('stores')->delete();

        $limit = 10;
        $password = bcrypt('123456');
        $this->createUser([
            'username' => 'admin',
            'full_name' => $this->faker->firstName,
            'website' => $this->faker->url,
            'email' => 'admin@gmail.com',
            'password' => $password,
            'phone' => $this->faker->phoneNumber,
            'role' => 1,
            'status' => 1
        ]);
        $this->createUser([
            'username' => 'user',
            'full_name' => $this->faker->firstName,
            'website' => $this->faker->url,
            'email' => 'user@gmail.com',
            'password' => $password,
            'phone' => $this->faker->phoneNumber,
            'role' => 0,
            'status' => 1
        ]);
    }

    private function createUser(array $data)
    {
        $user = User::create($data);
        $this->createStore($user);
        return $user;
    }

    private function createStore(User $user)
    {
        $store = \App\Models\Store::create([
            'name' => $this->faker->name,
            'address' => $this->faker->address,
            'url' => $this->faker->url
        ]);

        $user->storeUsers()->attach($store->id);
        return $store;
    }
}

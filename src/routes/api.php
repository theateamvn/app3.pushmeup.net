<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('/stores/upload-company-logo', 'StoreAPIController@uploadCompanyLogo')->name('stores.upload_company_logo');
    Route::post('/stores/upload-background', 'StoreAPIController@uploadRedirectPageBackground')->name('stores.upload_background');
    Route::resource('stores', 'StoreAPIController');

    Route::get('stores/{id}/smsMessage', 'StoreAPIController@smsMessage');
    Route::get('stores/{id}/emailMessage', 'StoreAPIController@emailMessage');
    Route::get('stores/{id}/mmsMessage', 'StoreAPIController@mmsMessage');
    Route::get('stores/{id}/smsCheckinMessage', 'StoreAPIController@smsCheckinMessage');
    Route::get('stores/{id}/emailCheckinMessage', 'StoreAPIController@emailCheckinMessage');

    Route::resource('store_users', 'StoreUserAPIController');

    Route::get('/settings/by-user', 'SettingAPIController@getSettingByUser')->name('settings.by_user');
    Route::get('/settings/link-review/by-user', 'SettingAPIController@getReviewLinkByUser')->name('settings.review_link_by_user');
    Route::get('/settings/link-review/{id}', 'SettingAPIController@getReviewLinkUpdate')->name('settings.update_review_link_by_user');
    Route::resource('settings', 'SettingAPIController');

    Route::get('/review_providers/by-user/{provider}', 'ReviewProviderAPIController@getProviderByUser')->name('review_providers.by_user.provider');
    Route::get('/review_providers/by-user', 'ReviewProviderAPIController@getListProviderByUser')->name('review_providers.by_user');
    Route::get('/test-auto', 'ReviewProviderAPIController@testAutoReview')->name('review_providers.test_auto');
    Route::resource('review_providers', 'ReviewProviderAPIController');

    Route::get('/reviews/all', 'ReviewAPIController@all')->name('review.all');
    Route::get('/reviews/get', 'ReviewAPIController@get')->name('review.get_all_review');
    Route::resource('reviews', 'ReviewAPIController');

    Route::get('/bad-reviews/all', 'BadReviewAPIController@all')->name('bad_review.all');
    Route::get('/bad-reviews/get', 'BadReviewAPIController@get')->name('bad_review.get_all_review');
    Route::resource('bad-reviews', 'ReviewAPIController');

    Route::post('/invite_messages/upload-mms-picture', 'InviteMessageAPIController@uploadMmsPicture')->name('invite_messages.upload_mms_picture');
    Route::resource('invite_messages', 'InviteMessageAPIController');

    Route::get('/invites/pagination', 'InviteAPIController@pagination')->name('invite.pagination');
    Route::get('/invites/click_review_link/{id}', 'InviteAPIController@click_review_link')->name('invite.click_review_link');
    Route::post('/invites/import', 'InviteAPIController@import')->name('invite.import_customers');
    Route::post('/invites/send', 'InviteAPIController@send')->name('invite.send_list_invite');
    Route::post('/invites/import-from-app', 'InviteAPIController@importFromApp')->name('invite.import_from_ap');
    Route::post('/invites/send-all', 'InviteAPIController@sendAll')->name('invite.send_all_invite');
    Route::get('/invites/information/{startDate?}/{endDate?}', 'InviteAPIController@getCustomerInformation')->name('invite.get_customer_information');
    Route::resource('invites', 'InviteAPIController');

    Route::resource('notices', 'NoticeAPIController');

    Route::get('/users/stores', 'UserAPIController@stores');
    Route::post('/users/create-store', 'UserAPIController@createStore');
    Route::post('/users/update-store', 'UserAPIController@updateStore');
    Route::put('/users/update-password', 'UserAPIController@updatePassword');
    Route::resource('users', 'UserAPIController');

    Route::get('/statistic', 'StatisticAPIController@index');
    Route::get('/statistic/reviews-star/{star}/{endStar}', 'StatisticAPIController@reviewStar');
    Route::get('/statistic/customer/{startDate}/{endDate?}', 'StatisticAPIController@customer');
    Route::get('/statistic/message_status/{startDate}', 'StatisticAPIController@messageStatus');

    Route::get('/master_redirect_backgrounds/public', 'MasterRedirectBackgroundAPIController@index')->name('master_redirect_backgrounds.public');
    Route::get('/master_settings/key/{key}', 'MasterSettingAPIController@getByKey')->name('master_settings.get_key');
    Route::post('/common/support', 'CommonAPIController@supportAndRequest')->name('common.send_support');
    Route::get('/common/info-support','CommonAPIController@infoSupport')->name('common.info_support');
    Route::group(['middleware' => 'admin'], function () {
        Route::resource('activity_logs', 'ActivityLogAPIController');
        Route::get('/statistic/user', 'StatisticAPIController@user');
        Route::get('/agent/statistic/user', 'StatisticAPIController@userAgent');
        Route::post('/master_redirect_backgrounds/upload', 'MasterRedirectBackgroundAPIController@upload')->name('master_redirect_backgrounds.upload');
        Route::resource('master_redirect_backgrounds', 'MasterRedirectBackgroundAPIController');
        Route::resource('master_settings', 'MasterSettingAPIController');
    });
    Route::get('/statistic/reviews-link', 'StatisticAPIController@reviewLink');
    Route::get('/feedback/pagination', 'FeedbackAPIController@pagination')->name('feedback.pagination');

    Route::get('client_contacts/byUser', 'ClientContactAPIController@byUser')->name('client_contacts.by_user');
    Route::get('client_contacts/countUser/{id}', 'ClientContactAPIController@countUser')->name('client_contacts.countUser');
    Route::resource('client_contacts', 'ClientContactAPIController');
    Route::post('blast_templates/uploadPicture', 'BlastTemplateAPIController@uploadPicture')->name('blast_templates.upload_picture');
    Route::get('blast_templates/byUser', 'BlastTemplateAPIController@byUser')->name('blast_templates.by_user');
    Route::get('blast_templates/syncContact', 'BlastTemplateAPIController@syncContacts')->name('blast_templates.sync_contacts');
    Route::post('blast_templates/{id}/addContacts', 'BlastTemplateAPIController@addContacts')->name('blast_templates.add_contacts');
    Route::get('blast_templates/{id}/sendBlast', 'BlastTemplateAPIController@sendBlast')->name('blast_templates.send_blast');
    Route::get('blast_templates/{id}/sendAll', 'BlastTemplateAPIController@sendAll')->name('blast_templates.send_all');
    Route::resource('blast_templates', 'BlastTemplateAPIController');

    Route::resource('blast_contacts', 'BlastContactAPIController');
    Route::get('/members/pagination', 'MemberAPIController@pagination')->name('members.pagination');
    Route::get('/members/get-total-price/{id}', 'MemberAPIController@get_total_price')->name('members.get_total_price');
    Route::post('/members/delete/{id}', 'MemberAPIController@delete')->name('members.delete');
    Route::resource('members', 'MemberAPIController');

    Route::get('checkin/user/{id}', 'CheckinAPIController@CheckinByUser')->name('checkin.checkinByUser');
    Route::get('checkin/statistic', 'CheckinAPIController@checkinStatistic')->name('checkin.checkinStatistic');
    Route::get('checkin/checkin-current', 'CheckinAPIController@checkinCurrent')->name('checkin.checkinCurrent');
    Route::post('checkin/check-out/{id}', 'CheckinAPIController@checkOut')->name('checkin.checkOut');
    Route::resource('checkin', 'CheckinAPIController');

    Route::get('/promotions/pagination', 'PromotionAPIController@pagination')->name('promotions.pagination');
    Route::post('/promotions/delete/{id}', 'PromotionAPIController@delete')->name('promotions.delete');
    Route::post('/promotions/update/{id}', 'PromotionAPIController@update')->name('promotions.update');
    Route::resource('promotions', 'PromotionAPIController');
    
    Route::get('/birthdays/pagination', 'BirthdayAPIController@pagination')->name('birthdays.pagination');
    Route::post('/birthdays/delete/{id}', 'BirthdayAPIController@delete')->name('birthdays.delete');
    Route::resource('birthdays', 'BirthdayAPIController');

    Route::resource('promotionatrisk', 'PromotionAtRiskAPIController');

    Route::get('/setting-checkin/by-user', 'SettingCheckinAPIController@getSettingByUser')->name('setting_checkin.by_user');
    Route::resource('setting-checkin', 'SettingCheckinAPIController');
});
/** check-in router */
Route::get('/promotions/bannerpromotion/{slug}', 'PromotionAPIController@getBannerPromotion')->name('promotions.bannerpromotion');
Route::post('checkin/checkinstepone', 'CheckinAPIController@checkInStepOne')->name('check_in.step_one');
Route::post('checkin/checkinsteptwo', 'CheckinAPIController@checkInStepTwo')->name('check_in.step_two');
Route::post('checkin/checkinoffline', 'CheckinAPIController@CheckinOffline')->name('check_in.offline');
Route::get('/review-link', 'ReviewLinkAPIController@reviewLink')->name('reviewLink.reviewLink');
Route::resource('feedback', 'FeedbackAPIController');
Route::get('members_checkin/getmemberlist/{slug}', 'MemberAPIController@getMemberList');
Route::get('stores/get-store/{slug}', 'StoreAPIController@getBySlug');
Route::post('website/login', 'UserAPIController@getUserCheckin')->name('user.website');
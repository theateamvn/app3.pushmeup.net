<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', function () {
        return view('dashboard');
    })->name('root');
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::get('/settings', function () {
        return view('setting');
    });
    Route::get('/reviews', function () {
        return view('review');
    });
    Route::get('/bad-reviews', function () {
        return view('bad_review');
    });
    Route::get('profile', function () {
        return view('profile');
    })->name('profile');

    Route::get('customers', function () {
        return view('customer');
    })->name('customers');

    Route::get('templates', function () {
        return view('template');
    })->name('templates');

    Route::get('import_customers', function () {
        return view('import_customer');
    })->name('import_customers');

    Route::get('feedback',function(){
        return view('feedback');
    })->name('feedback');

    Route::get('send-sms', function () {
        return view('send-sms');
    })->name('send-sms');

    Route::get('members',function(){
        return view('members');
    })->name('members');

    Route::get('promotions',function(){
        return view('promotion');
    })->name('promotions');

    Route::get('promotion-at-risk',function(){
        return view('promotion_at_risk');
    })->name('promotion-at-risk');

    Route::get('/dashboard-checkin', function () {
        return view('dashboard-checkin');
    })->name('dashboard-checkin');

    Route::get('birthdays',function(){
        return view('birthdays');
    })->name('birthdays');

    Route::get('setting-checkin',function(){
        return view('setting-checkin');
    })->name('setting-checkin');
});

/*
 /--------------------------------------------------------------------
 / Route admin
 /--------------------------------------------------------------------
 */
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    Route::get('/', function () {
        return view('admin.dashboard');
    })->name('root');

    Route::get('dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');

    Route::get('dashboard-agent', function () {
        return view('admin.dashboard_agent');
    })->name('root_agent');

    Route::get('access-user/{userId}/{redirectTo?}', 'DashboardController@accessUser');

    Route::get('agents', function () {
        return view('admin.users');
    })->name('agents');

    Route::get('users', function () {
        return view('admin.users');
    })->name('users');

    Route::get('notices', function () {
        return view('admin.notices');
    })->name('notices');

    Route::get('manage-background', function () {
        return view('admin.manage_background');
    })->name('manage_background');

    Route::get('settings',function(){
        return view('admin.settings');
    })->name('settings');

});

Route::get('callback/facebook/{store}/{provider}', 'CallbackController@facebookCallback')->name('callback.facebook');
Route::post(
    'stripe/webhook',
    '\App\Http\Controllers\StripeWebhookController@handleWebhook'
);
Route::get('/review/{shortId}/{step?}', 'ReviewRedirectController@index')->name('review.redirect');
Route::post('/review/{shortId}/{step?}', 'ReviewRedirectController@submit')->name('review.redirect.submit');
Route::get('/register-thanks', function () {
    return view('auth.register_thanks');
});
Route::get('/get-reviews-api', 'ApiGetReviews@get')->name('data.reviews');
Route::post('/api/insert-phone-store', 'ApiGetReviews@insertPhoneByStoreId')->name('insertPhoneByStoreId');
Route::get('/storage-link', function() {
	echo Artisan::call("storage:link");
});
Route::get('/cronjob/cronjob-run', 'CronjobAPIController@cronjobRun')->name('cronjob.cronjob_run');
Route::get('/cronjob/cronjob-birthday', 'CronjobAPIController@cronjobBirthday')->name('cronjob.cronjob_run_birthday');
Route::get('/check-in/{slug}', 'CheckInController@checkInByStoreSlug')->name('checkin.slug');
Route::get('/{slug}/{id}', 'ReviewRedirectController@redirectByStoreSlug')->name('review.redirect.slug.id');
Route::get('/{slug}', 'ReviewRedirectController@redirectByStoreSlug')->name('review.redirect.slug');



<?php

namespace App\Notifications;

use App\Exceptions\ConfigurationRequiredException;
use App\Exceptions\ServiceNotSupportException;
use App\Models\BlastTemplate;
use App\Models\Setting;
use App\Notifications\Channels\MmsBandwidthChannel;
use App\Notifications\Channels\MmsTwilioChannel;
use App\Notifications\Channels\SmsBandwidthChannel;
use App\Notifications\Channels\SmsEsmsChannel;
use App\Notifications\Channels\TwilioChannel;
use App\Notifications\Messages\SendInviteMessage;
use App\Services\SettingService;
use App\Services\Utils\StringUtils;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class SendBlast extends Notification
{
    use Queueable;

    const MATCH_BRACKET_PATTERN = '/\{{2}(([^{}]|(?R))*)\}{2}/';

    /** @var $blast_template BlastTemplate */
    protected $blast_template;
    protected $channels;
    private $setting;

    /**
     * Create a new notification instance.
     *
     */
    public function __construct()
    {
        $settingService = resolve(SettingService::class);
        $this->setting = $settingService->getSettingByUser();
        if (!$this->setting) {
            throw new ConfigurationRequiredException('Setting');
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  BlastTemplate $notifiable
     * @return array
     * @throws ConfigurationRequiredException
     */
    public function via($notifiable)
    {
        $this->blast_template = $notifiable;
        if ($this->blast_template->contacts->count() == 0) {
            throw new ConfigurationRequiredException('Contacts');
        }
        $this->channels[] = $this->blast_template->type;
        $channels = $this->getChannels();
        return $channels;
    }

    /**
     * @return array
     */
    protected function getChannels()
    {
        $channels = [];
        if ($this->hasChannel('sms') && $this->blast_template->type == BlastTemplate::TYPE_SMS) {
            $channels[] = $this->getSmsChannelByGatewaySetting();
        }
        if ($this->hasChannel('mms') && $this->blast_template->type == BlastTemplate::TYPE_MMS) {
            $channels[] = $this->getMmsChannelByGatewaySetting();
            return $channels;
        }
        return $channels;
    }

    /**
     * @return mixed
     */
    protected function getSmsChannelByGatewaySetting()
    {
        switch ($this->setting->sms_gateway) {
            case Setting::SMS_GATEWAY_TWILIO:
                return TwilioChannel::class;
            case Setting::SMS_GATEWAY_BANDWIDTH:
                return SmsBandwidthChannel::class;
            case Setting::SMS_GATEWAY_ESMS:
                return SmsEsmsChannel::class;
            default:
                return TwilioChannel::class;
        }
    }

    /**
     * @return mixed
     * @throws ServiceNotSupportException
     */
    protected function getMmsChannelByGatewaySetting()
    {
        if ($this->setting->sms_gateway == Setting::SMS_GATEWAY_ESMS) {
            throw new ServiceNotSupportException("{$this->setting->sms_gateway} is not support for mms");
        }
        switch ($this->setting->sms_gateway) {
            case Setting::SMS_GATEWAY_TWILIO:
                return TwilioChannel::class;
            case Setting::SMS_GATEWAY_BANDWIDTH:
                return MmsBandwidthChannel::class;
            default:
                return MmsTwilioChannel::class;
        }
    }

    private function hasChannel($string)
    {
        return in_array(strtolower($string), $this->channels);
    }

    /**
     * @param BlastTemplate $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toTwilio($notifiable)
    {
        $this->blast_template = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_username)
            || empty($this->setting->sms_password
                || empty($this->setting->sms_user))
        ) {
            throw new ConfigurationRequiredException("SMS Provider");
        }

        return $this->getSendInviteMessageForSMS();
    }

    /**
     * @param BlastTemplate $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toMmsTwilio($notifiable)
    {
        $this->blast_template = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_username)
            || empty($this->setting->sms_password
                || empty($this->setting->sms_user))
        ) {
            throw new ConfigurationRequiredException("SMS Provider");
        }

        return $this->getSendInviteMessageForMMS();
    }

    /**
     * @param BlastTemplate $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toMmsBandwidth($notifiable)
    {
        $this->blast_template = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_bandwidth_phone)
            || empty($this->setting->sms_bandwidth_user_id)
            || empty($this->setting->sms_bandwidth_api_token)
            || empty($this->setting->sms_bandwidth_api_secret)
        ) {
            throw new ConfigurationRequiredException("Bandwidth Provider");
        }

        return $this->getSendInviteMessageForMMSBandwidth();
    }

    /**
     * @param BlastTemplate $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toSmsBandwidth($notifiable)
    {
        $this->blast_template = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_bandwidth_phone)
            || empty($this->setting->sms_bandwidth_user_id)
            || empty($this->setting->sms_bandwidth_api_token)
            || empty($this->setting->sms_bandwidth_api_secret)
        ) {
            throw new ConfigurationRequiredException("Bandwidth Provider");
        }

        return $this->getSendInviteMessageForSMSBandwidth();
    }

    /**
     * @param BlastTemplate $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toSmsEsms($notifiable)
    {
        $this->blast_template = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_esms_sms_type)
            || empty($this->setting->sms_esms_api_token)
            || empty($this->setting->sms_esms_api_secret)
        ) {
            throw new ConfigurationRequiredException("Esms Provider");
        }

        return $this->getSendInviteMessageForSMSEsms();
    }

    /**
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForSMS()
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sms_user;
        $sendInviteMessage->to = $this->getPhones();
        $sendInviteMessage->sid = $this->setting->sms_username;
        $sendInviteMessage->token = $this->setting->sms_password;
        $sendInviteMessage->body = $this->getBodyContent();
        return $sendInviteMessage;
    }

    /**
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForMMS()
    {
        $sendInviteMessage = $this->getSendInviteMessageForSMS();
        $sendInviteMessage->mediaUrl = url($this->blast_template->getMmsMediaPath());
        return $sendInviteMessage;
    }

    /**
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForMMSBandwidth()
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sms_bandwidth_phone;
        $sendInviteMessage->to = $this->getPhones();
        $sendInviteMessage->sid = $this->setting->sms_bandwidth_user_id;
        $sendInviteMessage->app_id = $this->setting->sms_bandwidth_application_id;
        $sendInviteMessage->token = $this->setting->sms_bandwidth_api_token;
        $sendInviteMessage->secret = $this->setting->sms_bandwidth_api_secret;
        $sendInviteMessage->body = $this->getBodyContent();
        $sendInviteMessage->mediaUrl = url($this->blast_template->getMmsMediaPath());
        return $sendInviteMessage;
    }

    /**
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForSMSBandwidth()
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sms_bandwidth_phone;
        $sendInviteMessage->to = $this->getPhones();
        $sendInviteMessage->sid = $this->setting->sms_bandwidth_user_id;
        $sendInviteMessage->app_id = $this->setting->sms_bandwidth_application_id;
        $sendInviteMessage->token = $this->setting->sms_bandwidth_api_token;
        $sendInviteMessage->secret = $this->setting->sms_bandwidth_api_secret;
        $sendInviteMessage->body = $this->getBodyContent();
        return $sendInviteMessage;
    }

    /**
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForSMSEsms()
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sms_esms_sms_type;
        $sendInviteMessage->to = $this->getPhones();
        $sendInviteMessage->token = $this->setting->sms_esms_api_token;
        $sendInviteMessage->secret = $this->setting->sms_esms_api_secret;
        $sendInviteMessage->brandName = $this->setting->sms_esms_brand_name;
        $sendInviteMessage->body = $this->getBodyContent();
        return $sendInviteMessage;
    }

    /**
     * @return string
     */
    private function getBodyContent()
    {
        if ($this->blast_template->content) {
            return $this->replacePlaceholder($this->blast_template->content);
        }
        return '';
    }

    private function replacePlaceholder($input)
    {
        return StringUtils::replacePlaceholder(static::MATCH_BRACKET_PATTERN, $input, [
            'business_name' => $this->blast_template->store->name,
            'business_address' => $this->blast_template->store->address
        ]);
    }

    private function getPhones()
    {
        return $this->blast_template->contacts->pluck('phone');
    }
}

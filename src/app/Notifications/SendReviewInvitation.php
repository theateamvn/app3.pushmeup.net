<?php

namespace App\Notifications;

use App\Exceptions\ConfigurationRequiredException;
use App\Models\Invite;
use App\Models\InviteMessage;
use App\Models\Setting;
use App\Notifications\Channels\MailChannel;
use App\Notifications\Channels\MmsTwilioChannel;
use App\Notifications\Channels\TwilioChannel;
use App\Notifications\Messages\SendInviteMessage;
use App\Services\InviteMessageService;
use App\Services\SettingService;
use App\Services\Utils\StringUtils;
use App\Services\Bijective;
use App\Exceptions\ServiceNotSupportException;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use App\Notifications\Channels\MmsBandwidthChannel;
use App\Notifications\Channels\SmsBandwidthChannel;
use App\Notifications\Channels\SmsEsmsChannel;


class SendReviewInvitation extends Notification
{
    use Queueable;

    const MATCH_BRACKET_PATTERN = '/\{{2}(([^{}]|(?R))*)\}{2}/';

    /**
     * @var Setting
     */
    private $setting;
    /**
     * @var InviteMessageService
     */
    private $inviteMessageService;
    /**
     * @var Invite
     */
    protected $invite;

    private $channels;

    private $bijective;

    /**
     * Create a new notification instance.
     * @param array $channels
     * @throws ConfigurationRequiredException
     */
    public function __construct(array $channels = ['sms', 'mms', 'email'])
    {
        $this->channels = $channels;
        $this->bijective = resolve(Bijective::class);
        $settingService = resolve(SettingService::class);
        $this->setting = $settingService->getSettingByUser();
        if (!$this->setting) {
            throw new ConfigurationRequiredException('Setting');
        }
        $this->inviteMessageService = resolve(InviteMessageService::class);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  Invite $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->invite = $notifiable;
        $channels = $this->getChannels();
        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  Invite $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toMail($notifiable)
    {
        $this->invite = $notifiable;
        if (!$this->setting
            || empty($this->setting->email_title)
            || empty($this->setting->sender_email
                || empty($this->setting->sender_name))
        ) {
            throw new ConfigurationRequiredException("Mail Provider");
        }

        $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_MAIL);
        if (!$inviteMessage) {
            throw new ConfigurationRequiredException("Mail Content");
        }

        return $this->getSendInviteMessageForMail($inviteMessage);
    }

    /**
     * @param Invite $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toTwilio($notifiable)
    {
        $this->invite = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_username)
            || empty($this->setting->sms_password
                || empty($this->setting->sms_user))
        ) {
            throw new ConfigurationRequiredException("SMS Provider");
        }

        $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_SMS);
        if (!$inviteMessage) {
            throw new ConfigurationRequiredException("SMS Content");
        }

        return $this->getSendInviteMessageForSMS($inviteMessage);
    }

    public function toMmsTwilio($notifiable)
    {
        $this->invite = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_username)
            || empty($this->setting->sms_password
                || empty($this->setting->sms_user))
        ) {
            throw new ConfigurationRequiredException("SMS Provider");
        }

        $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_MMS);
        if (!$inviteMessage) {
            throw new ConfigurationRequiredException("MMS Content");
        }

        return $this->getSendInviteMessageForMMS($inviteMessage);
    }

    /**
     * @param Invite $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toMmsBandwidth($notifiable)
    {
        $this->invite = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_bandwidth_phone)
            || empty($this->setting->sms_bandwidth_user_id)
            || empty($this->setting->sms_bandwidth_api_token)
            || empty($this->setting->sms_bandwidth_api_secret)
        ) {
            throw new ConfigurationRequiredException("Bandwidth Provider");
        }

        $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_MMS);
        if (!$inviteMessage) {
            throw new ConfigurationRequiredException("MMS Content");
        }

        return $this->getSendInviteMessageForMMSBandwidth($inviteMessage);
    }

    /**
     * @param Invite $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toSmsBandwidth($notifiable)
    {
        $this->invite = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_bandwidth_phone)
            || empty($this->setting->sms_bandwidth_user_id)
            || empty($this->setting->sms_bandwidth_api_token)
            || empty($this->setting->sms_bandwidth_api_secret)
        ) {
            throw new ConfigurationRequiredException("Bandwidth Provider");
        }

        $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_SMS);
        if (!$inviteMessage) {
            throw new ConfigurationRequiredException("Sms Content");
        }

        return $this->getSendInviteMessageForSMSBandwidth($inviteMessage);
    }

    /**
     * @param $notifiable
     * @return SendInviteMessage
     * @throws ConfigurationRequiredException
     */
    public function toSmsEsms($notifiable)
    {
        $this->invite = $notifiable;
        if (!$this->setting
            || empty($this->setting->sms_esms_sms_type)
            || empty($this->setting->sms_esms_api_token)
            || empty($this->setting->sms_esms_api_secret)
        ) {
            throw new ConfigurationRequiredException("Esms Provider");
        }

        $inviteMessage = $this->inviteMessageService->getMessageByUser(InviteMessage::TYPE_SMS);
        if (!$inviteMessage) {
            throw new ConfigurationRequiredException("SMS Content");
        }

        return $this->getSendInviteMessageForSMSEsms($inviteMessage);
    }

    /**
     * @return array
     */
    protected function getChannels()
    {
        $channels = [];
        if ($this->hasChannel('sms')) {
            $channels[] = $this->getSmsChannelByGatewaySetting();
        }
        if ($this->hasChannel('mms')) {
            $channels[] = $this->getMmsChannelByGatewaySetting();
            return $channels;
        }
        return $channels;
    }

    protected function getChannelsBk()
    {
        $channels = [];
        if (!$this->invite->email_sent && !empty($this->invite->email) && $this->hasChannel('email')) {
            $channels[] = MailChannel::class;
        }
        if (!$this->invite->phone_sent && !empty($this->invite->phone) && $this->hasChannel('sms')) {
            $channels[] = $this->getSmsChannelByGatewaySetting();
        }
        if (!$this->invite->mms_sent && !empty($this->invite->phone) && $this->hasChannel('mms')) {
            $channels[] = $this->getMmsChannelByGatewaySetting();
            return $channels;
        }
        return $channels;
    }
    /**
     * @return mixed
     */
    protected function getSmsChannelByGatewaySetting()
    {
        switch ($this->setting->sms_gateway) {
            case Setting::SMS_GATEWAY_TWILIO:
                return TwilioChannel::class;
            case Setting::SMS_GATEWAY_BANDWIDTH:
                return SmsBandwidthChannel::class;
            case Setting::SMS_GATEWAY_ESMS:
                return SmsEsmsChannel::class;
            default:
                return TwilioChannel::class;
        }
    }

    /**
     * @return mixed
     * @throws ServiceNotSupportException
     */
    protected function getMmsChannelByGatewaySetting()
    {
        /*if ($this->setting->sms_gateway == Setting::SMS_GATEWAY_ESMS) {
            throw new ServiceNotSupportException("{$this->setting->sms_gateway} is not support for mms");
        }*/
        switch ($this->setting->sms_gateway) {
            case Setting::SMS_GATEWAY_TWILIO:
                return MmsTwilioChannel::class;
            case Setting::SMS_GATEWAY_BANDWIDTH:
                return MmsBandwidthChannel::class;
            default:
                return MmsTwilioChannel::class;
        }
    }

    private function isMail($destination)
    {
        if (!$destination) return false;
        return preg_match('/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/', $destination);
    }

    /**
     * @param InviteMessage $inviteMessage
     * @return string
     */
    private function getBodyContent($inviteMessage)
    {
        if ($inviteMessage->content) {
            return $this->replacePlaceholder($inviteMessage->content);
        }
        return $inviteMessage->fallback_content;
    }

    /**
     * @param InviteMessage $inviteMessage
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForSMS(InviteMessage $inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sms_user;
        $sendInviteMessage->to = $this->invite->phone;
        $sendInviteMessage->sid = $this->setting->sms_username;
        $sendInviteMessage->token = $this->setting->sms_password;
        $sendInviteMessage->body = $this->getBodyContent($inviteMessage);
        return $sendInviteMessage;
    }

    private function getSendInviteMessageForMail(InviteMessage $inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sender_email;
        $sendInviteMessage->to = $this->invite->email;
        $sendInviteMessage->name = $this->setting->sender_name;
        $sendInviteMessage->title = $this->setting->email_title;
        $sendInviteMessage->body = $this->getBodyContent($inviteMessage);
        return $sendInviteMessage;
    }

    private function replacePlaceholder($input)
    {
        //var_dump($this->invite);die;
        $cusId = 0;
        if($this->invite)
        {
            $cusId = $this->bijective->decode($this->invite->id);
        }
        return StringUtils::replacePlaceholder(static::MATCH_BRACKET_PATTERN, $input, [
            'full_name' => $this->invite->name,
            'email' => $this->invite->email,
            'phone_number' => $this->invite->phone,
            'invite_link' => $this->invite->getInviteLink(),
            'review_link' => $this->invite->store->getReviewRedirectLink($cusId),
            'business_name' => $this->invite->store->name,
            'business_address' => $this->invite->store->address
        ]);
    }

    /**
     * @param InviteMessage $inviteMessage
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForMMS($inviteMessage)
    {
        echo 'link----'.url($inviteMessage->getMmsMediaPath());die;
        $sendInviteMessage = $this->getSendInviteMessageForSMS($inviteMessage);
        $sendInviteMessage->mediaUrl = url($inviteMessage->getMmsMediaPath());
        return $sendInviteMessage;
    }

    private function hasChannel($string)
    {
        return in_array(strtolower($string), $this->channels);
    }

    /**
     * @param InviteMessage $inviteMessage
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForMMSBandwidth($inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sms_bandwidth_phone;
        $sendInviteMessage->to = $this->invite->phone;
        $sendInviteMessage->sid = $this->setting->sms_bandwidth_user_id;
        $sendInviteMessage->app_id = $this->setting->sms_bandwidth_application_id;
        $sendInviteMessage->token = $this->setting->sms_bandwidth_api_token;
        $sendInviteMessage->secret = $this->setting->sms_bandwidth_api_secret;
        $sendInviteMessage->body = $this->getBodyContent($inviteMessage);
        $sendInviteMessage->mediaUrl = url($inviteMessage->getMmsMediaPath());
        return $sendInviteMessage;
    }

    /**
     * @param InviteMessage $inviteMessage
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForSMSBandwidth($inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sms_bandwidth_phone;
        $sendInviteMessage->to = $this->invite->phone;
        $sendInviteMessage->sid = $this->setting->sms_bandwidth_user_id;
        $sendInviteMessage->app_id = $this->setting->sms_bandwidth_application_id;
        $sendInviteMessage->token = $this->setting->sms_bandwidth_api_token;
        $sendInviteMessage->secret = $this->setting->sms_bandwidth_api_secret;
        $sendInviteMessage->body = $this->getBodyContent($inviteMessage);
        return $sendInviteMessage;
    }

    /**
     * @param InviteMessage $inviteMessage
     * @return SendInviteMessage
     */
    private function getSendInviteMessageForSMSEsms($inviteMessage)
    {
        $sendInviteMessage = new SendInviteMessage();
        $sendInviteMessage->from = $this->setting->sms_esms_sms_type;
        $sendInviteMessage->to = $this->invite->phone;
        $sendInviteMessage->token = $this->setting->sms_esms_api_token;
        $sendInviteMessage->secret = $this->setting->sms_esms_api_secret;
        $sendInviteMessage->brandName = $this->setting->sms_esms_brand_name;
        $sendInviteMessage->body = $this->getBodyContent($inviteMessage);
        return $sendInviteMessage;
    }
}

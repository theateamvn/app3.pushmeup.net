<?php
namespace App\Notifications\Messages;
/**
 * Created by PhpStorm.
 * User: Lai Vu
 * Date: 11/30/2016
 * Time: 10:16 AM
 */
class SendInviteMessage
{
    public $from;
    public $to;
    public $name;
    public $title;
    public $body;
    public $sid;
    public $token;
    public $mediaUrl;
    public $secret;
    public $brandName;

}
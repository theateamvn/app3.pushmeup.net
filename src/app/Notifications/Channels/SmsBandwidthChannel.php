<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 2/20/2017
 * Time: 9:50 AM
 */

namespace App\Notifications\Channels;
use App\Notifications\Messages\SendInviteMessage;
use App\Notifications\SendReviewInvitation;
//use Catapult\Client;
use GuzzleHttp\Client;
use Catapult\Credentials;
use Catapult\Message;
use Catapult\PhoneNumber;
use Catapult\TextMessage;
use BandwidthLib\Configuration;
use Illuminate\Notifications\Notification;

class SmsBandwidthChannel
{
    /**
     * @var Client
     */
    private $client;

    /**
     * TwilioChannel constructor.
     */
    public function __construct()
    {
    }


    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param Notification|SendReviewInvitation $notification
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toSmsBandwidth($notifiable);
        $client = new Client([
            'auth' => [$message->token, $message->secret],
        ]); 
        if(is_array($message->to)){
            foreach($message->to as $to){
                $client->request('POST', "https://messaging.bandwidth.com/api/v2/users/".$message->sid."/messages", [
                    'headers' => [
                        'Content-Type'  => 'application/json; charset=utf-8'
                    ],
                    'body' => json_encode([
                        "to"            => $to,
                        "from"          => $message->from,
                        "text"          => $message->body,
                        "applicationId"    => $message->app_id
                    ])
                ]);
            }
        } else {
            $client->request('POST', "https://messaging.bandwidth.com/api/v2/users/".$message->sid."/messages", [
                'headers' => [
                    'Content-Type'  => 'application/json; charset=utf-8'
                ],
                'body' => json_encode([
                    "to"            => $message->to,
                    "from"          => $message->from,
                    "text"          => $message->body,
                    "applicationId"    => $message->app_id
                ])
            ]);
        }
    }

    public function send_bk($notifiable, Notification $notification)
    {
        /** @var SendInviteMessage $message */
        $message = $notification->toSmsBandwidth($notifiable);
        $cred = new Credentials($message->sid, $message->token, $message->secret, "6df2ff10-0c7e-4974-bbed-5ed33e275a45", "9496621160");
        //var_dump($cred);die;
        $this->client = new Client($cred);
        if(is_array($message->to)){
            foreach($message->to as $to){
                $this->client = new Message([
                    "from" => new PhoneNumber($message->from), // Replace with a Bandwidth Number
                    "to" => new PhoneNumber($to),
                    "text" => new TextMessage($message->body)
                ]);
            }
        }else{
            $this->client = new Message([
                "from" => new PhoneNumber($message->from), // Replace with a Bandwidth Number
                "to" => new PhoneNumber($message->to),
                "text" => new TextMessage($message->body)
            ]);
        }

    }
    public function send_bk2($notifiable, Notification $notification)
    {
        $message = $notification->toSmsBandwidth($notifiable);
        
        $client = new Client([
            'auth' => ['3e3020183e048c1a9b5a579f248f9af72480fb8001773bb4', 'c3e16239c748aaaa477d4fb4dc06547d65422a92acf931a2'],
        ]); 
        $res = $client->request('POST', "https://messaging.bandwidth.com/api/v2/users/5005811/messages", [
            'headers' => [
                'Content-Type'  => 'application/json; charset=utf-8'
            ],
            'body' => json_encode([
                "to"            => "9497992012",
                "from"          => "9496621160",
                "text"          => "Hey, check this out mms!",
                "applicationId" => "6df2ff10-0c7e-4974-bbed-5ed33e275a45",
                "media" => "https://demo.pushmeup.net/assets/layouts/layout4/img/logo-light.png"
            ])
        ]);
        $result = json_decode((string) $res->getBody(), true);
        
    }
}
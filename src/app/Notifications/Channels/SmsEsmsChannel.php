<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 2/20/2017
 * Time: 10:35 AM
 */

namespace App\Notifications\Channels;


use App\Notifications\Messages\SendInviteMessage;
use App\Notifications\SendReviewInvitation;
use Illuminate\Notifications\Notification;
use App\Services\Sms\Esms\Client;

class SmsEsmsChannel
{
    /**
     * @var Client
     */
    private $client;

    /**
     * TwilioChannel constructor.
     */
    public function __construct()
    {
    }


    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param Notification|SendReviewInvitation $notification
     */
    public function send($notifiable, Notification $notification)
    {
        /** @var SendInviteMessage $message */
        $message = $notification->toSmsEsms($notifiable);
        $this->client = new Client($message->token, $message->secret);
        if (is_array($message->to)) {
            foreach ($message->to as $to) {
                $this->client->create($to, [
                    "body" => $message->body,
                    "smsType" => $message->from,
                    "brandName" => $message->brandName
                ]);
            }
        } else {
            $this->client->create($message->to, [
                "body" => $message->body,
                "smsType" => $message->from,
                "brandName" => $message->brandName
            ]);
        }
    }
}
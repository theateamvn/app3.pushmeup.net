<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 2/20/2017
 * Time: 10:02 AM
 */

namespace App\Notifications\Channels;


use App\Notifications\Messages\SendInviteMessage;
use App\Notifications\SendReviewInvitation;
//use Catapult\Client;
use GuzzleHttp\Client;
use Catapult\Credentials;
use Catapult\MediaURL;
use Catapult\Message;
use Catapult\PhoneNumber;
use Catapult\TextMessage;
use Illuminate\Notifications\Notification;

class MmsBandwidthChannel
{
    /**
     * @var Client
     */
    private $client;

    /**
     * TwilioChannel constructor.
     */
    public function __construct()
    {
    }

    public function send($notifiable, Notification $notification)
    {
        /** @var SendInviteMessage $message */
        $message = $notification->toMmsBandwidth($notifiable);
        $client = new Client([
            'auth' => [$message->token, $message->secret],
        ]); 
        if(is_array($message->to)){
            foreach($message->to as $to){
                $client->request('POST', "https://messaging.bandwidth.com/api/v2/users/".$message->sid."/messages", [
                    'headers' => [
                        'Content-Type'  => 'application/json; charset=utf-8'
                    ],
                    'body' => json_encode([
                        "to"            => $to,
                        "from"          => $message->from,
                        "text"          => $message->body,
                        "media"         => $message->mediaUrl,
                        "applicationId" => $message->app_id
                        //"applicationId" => "6df2ff10-0c7e-4974-bbed-5ed33e275a45"
                    ])
                ]);
            }
        } else {
            $client->request('POST', "https://messaging.bandwidth.com/api/v2/users/".$message->sid."/messages", [
                'headers' => [
                    'Content-Type'  => 'application/json; charset=utf-8'
                ],
                'body' => json_encode([
                    "to"            => $message->to,
                    "from"          => $message->from,
                    "text"          => $message->body,
                    "media"         => $message->mediaUrl,
                    "applicationId"              => $message->app_id
                ])
            ]);
        }

    }
    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param Notification|SendReviewInvitation $notification
     */
    public function send_bk($notifiable, Notification $notification)
    {
        /** @var SendInviteMessage $message */
        $message = $notification->toMmsBandwidth($notifiable);
        $cred = new Credentials($message->sid, $message->token, $message->secret);
        $this->client = new Client($cred);
        if(is_array($message->to)){
            foreach ($message->to as $to){
                $this->client = new Message([
                    "from" => new PhoneNumber($message->from),
                    "to" => new PhoneNumber($to),
                    "text" => new TextMessage($message->body),
                    "media" => new MediaURL($message->mediaUrl)
                ]);
            }
        }else{
            $this->client = new Message([
                "from" => new PhoneNumber($message->from),
                "to" => new PhoneNumber($message->to),
                "text" => new TextMessage($message->body),
                "media" => new MediaURL($message->mediaUrl)
            ]);
        }

    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 11/30/2016
 * Time: 11:22 AM
 */

namespace App\Notifications\Channels;


use Illuminate\Notifications\Notification;

class MailChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param Notification $notification
     */
    public function send($notifiable, Notification $notification)
    {
        /** var SendReviewInvitation $message */
        $message = $notification->toMail($notifiable);

        \Mail::send([],[],function ($mail) use ($message){
            $mail->to($message->to)
                ->from($message->from,$message->name)
                ->subject($message->title)
                ->setBody($message->body,'text/html');
        });
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 12/14/2016
 * Time: 2:43 PM
 */

namespace App\Notifications\Channels;


use App\Notifications\SendReviewInvitation;
use Illuminate\Notifications\Notification;
use Twilio\Rest\Client;

class MmsTwilioChannel
{
    /**
     * @var Client
     */
    private $client;

    /**
     * TwilioChannel constructor.
     */
    public function __construct()
    {
    }


    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param Notification|SendReviewInvitation $notification
     */
    public function send($notifiable, Notification $notification)
    {
        /** var SendReviewInvitation $message */
        $message = $notification->toMmsTwilio($notifiable);

        // Send notification to the $notifiable instance...
        $this->client = new Client($message->sid,$message->token);

        if(is_array($message->to)){
            foreach ($message->to as $to){
                $this->client->messages->create($to,[
                    'from'=>$message->from,
                    'body'=>$message->body,
                    'mediaUrl'=>$message->mediaUrl
                ]);
            }
        }else{
            $this->client->messages->create($message->to,[
                'from'=>$message->from,
                'body'=>$message->body,
                'mediaUrl'=>$message->mediaUrl
            ]);
        }

    }
}
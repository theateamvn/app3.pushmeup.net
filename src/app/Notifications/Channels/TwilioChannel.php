<?php
namespace App\Notifications\Channels;

use App\Notifications\SendReviewInvitation;
use Illuminate\Notifications\Notification;
use Twilio\Rest\Client;

/**
 * Created by PhpStorm.
 * User: Lai Vu
 * Date: 11/30/2016
 * Time: 10:09 AM
 */
class TwilioChannel
{

    /**
     * @var Client
     */
    private $client;

    /**
     * TwilioChannel constructor.
     */
    public function __construct()
    {
    }


    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param Notification|SendReviewInvitation $notification
     */
    public function send($notifiable, Notification $notification)
    {
        /** var SendReviewInvitation $message */
        $message = $notification->toTwilio($notifiable);
        // Send notification to the $notifiable instance...
        $this->client = new Client($message->sid, $message->token);
        if (is_array($message->to)) {
            foreach ($message->to as $to) {
                $this->client->messages->create($to, [
                    'from' => $message->from,
                    'body' => $message->body
                ]);
            }
        } else {
            $this->client->messages->create($message->to, [
                'from' => $message->from,
                'body' => $message->body
            ]);
        }

    }
}
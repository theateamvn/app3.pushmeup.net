<?php

namespace App\Console\Commands;

use App\Services\ReviewService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class FetchReview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pushmeup:fetch-all-review';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch all reviews from all store and provider';

  /**
   * @var ReviewService
   */
    private $reviewService;

    /**
     * Create a new command instance.
     *
     * @param ReviewService $reviewService
     */
    public function __construct(ReviewService $reviewService)
    {
        parent::__construct();
        $this->reviewService = $reviewService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("================= Start fetch all review from all store =================");
        $result = $this->reviewService->fetchAndSaveReviewInformationFromService();
        $this->line("RESULT:");
        $this->line("Execution Time: {$result->executionTime}ms");
        $this->line("Fetch all: {$result->totalReview} reviews");
        $this->line("Save new success: {$result->reviewSaveSuccess} reviews");
        $this->line("Save reject: {$result->reviewSaveFail} reviews");
        $this->info("================================= FINISH ================================");
    }
}

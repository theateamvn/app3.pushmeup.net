<?php

namespace App\Console;

use App\Console\Commands\FetchReview;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;
use Illuminate\Routing\UrlGenerator;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        FetchReview::class
    ];

    /**
     * Define the application's command schedule.
     * time: https://laravel.com/docs/5.8/scheduling
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*$schedule->command('pushmeup:fetch-all-review')
            ->everyTenMinutes()
            ->sendOutputTo(storage_path() . "/logs/mail.log")
            ->emailOutputTo(env('ADMIN_MAIL'));*/
        $url=url()->full();
        $schedule->exec('curl '.$url.'/cronjob/cronjob-run')->everyFiveMinutes()->sendOutputTo(storage_path() .'/logs/cronjob.log');
        $schedule->exec('curl '.$url.'/cronjob/cronjob-birthday')->dailyAt('07:59')->sendOutputTo(storage_path() .'/logs/cronbirthday.log');
        //$schedule->command('pushmeup:fetch-all-review')->everyMinute();
            
        // Setting queue task on shared hosting
        /*$path = base_path();
        $schedule->call(function() use($path) {
            if (file_exists($path . '/queue.pid')) {
                $pid = file_get_contents($path . '/queue.pid');
                $result = exec("ps -p $pid --no-heading | awk '{print $1}'");
                $run = $result == '' ? true : false;
            } else {
                $run = true;
            }
            if($run) {
                $command = '/usr/bin/php -c ' . $path .'/php.ini ' . $path . '/artisan queue:listen --tries=3 > /dev/null & echo $!';
                
                Log::info($command);
                
                $number = exec($command);
                file_put_contents($path . '/queue.pid', $number);
            }
        })->name('monitor_queue_listener')->everyFiveMinutes();*/
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 3/13/2017
 * Time: 9:44 AM
 */

namespace App\Facades;


use App\Services\MasterSettingService;
use Illuminate\Support\Facades\Facade;

class MasterSettings extends Facade
{
    protected static function getFacadeAccessor()
    {
        return MasterSettingService::class;
    }
}
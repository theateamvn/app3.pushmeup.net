<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 12/9/2016
 * Time: 10:14 AM
 */

namespace App\DTOs;


class FetchAllReviewResult
{
    public $executionTime = 0;
    public $totalReview = 0;
    public $reviewSaveSuccess = 0;
    public $reviewSaveFail = 0;


}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 21/11/2016
 * Time: 11:17 PM
 */

namespace App\Policies;


use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BaseAdminPolicy
{
  use HandlesAuthorization;

  /**
   * @param User $user
   * @param $ability
   * @return bool
   */
  public function before($user, $ability)
  {
    if ($user->isAdmin()) {
      return true;
    }
  }

}
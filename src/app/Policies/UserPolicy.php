<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy extends BaseAdminPolicy
{

  /**
   * Determine whether the user can view the user.
   *
   * @param  User $user
   * @param User $userModel
   * @return mixed
   */
  public function view(User $user, User $userModel)
  {
    return true;
  }

  /**
   * Determine whether the user can create users.
   *
   * @param  User $user
   * @return mixed
   */
  public function create(User $user)
  {
    return true;
  }

  /**
   * Determine whether the user can update the user.
   *
   * @param  User $user
   * @param User $userModel
   * @return mixed
   */
  public function update(User $user, User $userModel)
  {
    return $user->id == $userModel->id;
  }

  /**
   * Determine whether the user can delete the user.
   *
   * @param  User $user
   * @param User $userModel
   * @return mixed
   */
  public function delete(User $user, User $userModel)
  {
    return false;
  }
}

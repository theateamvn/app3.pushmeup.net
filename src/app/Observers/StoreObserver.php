<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 12/21/2016
 * Time: 3:38 PM
 */

namespace App\Observers;


use App\Models\InviteMessage;
use App\Models\Setting;
use App\Models\Store;

class StoreObserver
{
    public function deleting(Store $store)
    {
        foreach ($store->reviewProviders as $provider) {
            $provider->delete();
        }
        foreach ($store->reviews as $review) {
            $review->delete();
        }
        foreach ($store->inviteMessages as $inviteMessage){
            $inviteMessage->delete();
        }
        foreach ($store->settings as $setting){
            $setting->delete();
        }
    }

    public function creating(Store $store){
        $store->invitation_introducing = trans('messages.we_appreciate_you_so_much_for_leaving_your_review_and_rating_us');
        $store->survey_message_1 = trans('messages.survey_message_1');
        $store->survey_message_2 = trans('messages.survey_message_2');
    }

    public function created(Store $store){
        InviteMessage::createDefaultContent($store->id);
        Setting::createDefaultSetting($store->id);
    }
}
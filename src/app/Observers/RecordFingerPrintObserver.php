<?php
namespace App\Observers;

use Auth;

/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 13/11/2016
 * Time: 10:27 AM
 */
class RecordFingerPrintObserver
{

    public function saving($model)
    {
        if (Auth::check())
            $model->updated_by = Auth::user()->id;
    }

    public function saved($model)
    {
        if (Auth::check())
            $model->updated_by = Auth::user()->id;
    }


    public function updating($model)
    {
        if (Auth::check())
            $model->updated_by = Auth::user()->id;
    }

    public function updated($model)
    {
        if (Auth::check())
            $model->updated_by = Auth::user()->id;
    }


    public function creating($model)
    {
        if (Auth::check())
            $model->created_by = Auth::user()->id;
    }

    public function created($model)
    {
        if (Auth::check())
            $model->created_by = Auth::user()->id;
    }
}
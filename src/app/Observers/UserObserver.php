<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 12/21/2016
 * Time: 3:47 PM
 */

namespace App\Observers;


use App\Models\User;

class UserObserver
{
    public function deleting(User $user)
    {
        foreach ($user->storeUsers as $store){
            $user->storeUsers()->detach($store->id);
            $store->delete();
        }
    }
}
<?php

namespace App\Listeners;

use App\Models\BlastTemplate;
use App\Models\Invite;
use App\Models\SmsSendLog;
use App\Notifications\Channels\MailChannel;
use App\Notifications\Channels\MmsTwilioChannel;
use App\Notifications\Channels\TwilioChannel;
use App\Notifications\SendBlast;
use App\Notifications\SendReviewInvitation;
use App\Notifications\SendCheckin;
use App\Notifications\SendPromotion;
use App\Notifications\SendBirthday;
use App\Notifications\SendAtRisk;
use Carbon\Carbon;
use Illuminate\Notifications\Events\NotificationSent;
use App\Notifications\Channels\MmsBandwidthChannel;
use App\Notifications\Channels\SmsBandwidthChannel;
use App\Notifications\Channels\SmsEsmsChannel;

class InviteSent
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationSent $event
     * @return void
     */
    public function handle(NotificationSent $event)
    {
        $this->handleSendInvitationSuccess($event);
        $this->handleSendSmsBlast($event);
        $this->handleSendCheckinSuccess($event);
        $this->handleSendPromotionSuccess($event);
        $this->handleSendBirthdaySuccess($event);
        $this->handleSendAtRiskSuccess($event);
    }

    /**
     * @param NotificationSent $event
     */
    protected function handleSendInvitationSuccess(NotificationSent $event)
    {
        if ($event->notification instanceof SendReviewInvitation
            && ($event->channel == MailChannel::class
                || $event->channel == TwilioChannel::class
                || $event->channel == MmsTwilioChannel::class
                || $event->channel == SmsBandwidthChannel::class
                || $event->channel == MmsBandwidthChannel::class
                || $event->channel == SmsEsmsChannel::class)
        ) {
            /** @var Invite $invite */
            $invite = $event->notifiable;
            if ($event->channel == MailChannel::class) {
                $invite->email_sent = Carbon::now();
            }
            if ($event->channel == TwilioChannel::class
                || $event->channel == SmsEsmsChannel::class
                || $event->channel == SmsBandwidthChannel::class
            ) {
                $invite->phone_sent = Carbon::now();
            }
            if ($event->channel == MmsTwilioChannel::class
                || $event->channel == MmsBandwidthChannel::class
            ) {
                $invite->mms_sent = Carbon::now();
            }
            $invite->save();
        }
    }

    /**
     * @param NotificationSent $event
     */
    private function handleSendSmsBlast($event)
    {
        if ($event->notification instanceof SendBlast
            && ($event->channel == MailChannel::class
                || $event->channel == TwilioChannel::class
                || $event->channel == MmsTwilioChannel::class
                || $event->channel == SmsBandwidthChannel::class
                || $event->channel == MmsBandwidthChannel::class
                || $event->channel == SmsEsmsChannel::class)
        ){
            /** @var BlastTemplate $blastTemplate */
            $blastTemplate = $event->notifiable;
            $blastTemplate->last_sent_at = Carbon::now();
            $blastTemplate->save();
        }
    }

    protected function handleSendCheckinSuccess(NotificationSent $event)
    {
        if ($event->notification instanceof SendCheckin
            && ($event->channel == MailChannel::class
                || $event->channel == TwilioChannel::class
                || $event->channel == MmsTwilioChannel::class
                || $event->channel == SmsBandwidthChannel::class
                || $event->channel == MmsBandwidthChannel::class
                || $event->channel == SmsEsmsChannel::class)
        ) {
            /** @var Invite $invite */
            $type = 'sms';
            $invite = $event->notifiable;
            if ($event->channel == MailChannel::class) {
                $type = 'email';
            }
            if ($event->channel == TwilioChannel::class
                || $event->channel == SmsEsmsChannel::class
                || $event->channel == SmsBandwidthChannel::class
            ) {
                $type = 'sms';
            }
            if ($event->channel == MmsTwilioChannel::class
                || $event->channel == MmsBandwidthChannel::class
            ) {
                $type = 'mms';
            }
            $input = array(
                'store_id'  => $invite->member_store_id,
                'member_id' => $invite->id,
                'type_send' => $type,
                'type_checkin' => 'checkin'
            );
            $smsSendLog = SmsSendLog::create($input);
            $smsSendLog->save();
        }
    }
    protected function handleSendPromotionSuccess(NotificationSent $event)
    {
        if ($event->notification instanceof SendPromotion
            && ($event->channel == MailChannel::class
                || $event->channel == TwilioChannel::class
                || $event->channel == MmsTwilioChannel::class
                || $event->channel == SmsBandwidthChannel::class
                || $event->channel == MmsBandwidthChannel::class
                || $event->channel == SmsEsmsChannel::class)
        ) {
            /** @var Invite $invite */
            $type = 'sms';
            $invite = $event->notifiable;
            if ($event->channel == MailChannel::class) {
                $type = 'email';
            }
            if ($event->channel == TwilioChannel::class
                || $event->channel == SmsEsmsChannel::class
                || $event->channel == SmsBandwidthChannel::class
            ) {
                $type = 'sms';
            }
            if ($event->channel == MmsTwilioChannel::class
                || $event->channel == MmsBandwidthChannel::class
            ) {
                $type = 'mms';
            }
            $input = array(
                'store_id'  => $invite->member_store_id,
                'member_id' => $invite->id,
                'type_send' => $type,
                'type_checkin' => 'promotion'
            );
            $smsSendLog = SmsSendLog::create($input);
            $smsSendLog->save();
        }
    }
    protected function handleSendBirthdaySuccess(NotificationSent $event)
    {
        if ($event->notification instanceof SendBirthday
            && ($event->channel == MailChannel::class
                || $event->channel == TwilioChannel::class
                || $event->channel == MmsTwilioChannel::class
                || $event->channel == SmsBandwidthChannel::class
                || $event->channel == MmsBandwidthChannel::class
                || $event->channel == SmsEsmsChannel::class)
        ) {
            /** @var Invite $invite */
            $type = 'sms';
            $invite = $event->notifiable;
            if ($event->channel == MailChannel::class) {
                $type = 'email';
            }
            if ($event->channel == TwilioChannel::class
                || $event->channel == SmsEsmsChannel::class
                || $event->channel == SmsBandwidthChannel::class
            ) {
                $type = 'sms';
            }
            if ($event->channel == MmsTwilioChannel::class
                || $event->channel == MmsBandwidthChannel::class
            ) {
                $type = 'mms';
            }
            $input = array(
                'store_id'  => $invite->member_store_id,
                'member_id' => $invite->id,
                'type_send' => $type,
                'type_checkin' => 'birthday'
            );
            $smsSendLog = SmsSendLog::create($input);
            $smsSendLog->save();
        }
    }
    protected function handleSendAtRiskSuccess(NotificationSent $event)
    {
        if ($event->notification instanceof SendAtRisk
            && ($event->channel == MailChannel::class
                || $event->channel == TwilioChannel::class
                || $event->channel == MmsTwilioChannel::class
                || $event->channel == SmsBandwidthChannel::class
                || $event->channel == MmsBandwidthChannel::class
                || $event->channel == SmsEsmsChannel::class)
        ) {
            /** @var Invite $invite */
            $type = 'sms';
            $invite = $event->notifiable;
            if ($event->channel == MailChannel::class) {
                $type = 'email';
            }
            if ($event->channel == TwilioChannel::class
                || $event->channel == SmsEsmsChannel::class
                || $event->channel == SmsBandwidthChannel::class
            ) {
                $type = 'sms';
            }
            if ($event->channel == MmsTwilioChannel::class
                || $event->channel == MmsBandwidthChannel::class
            ) {
                $type = 'mms';
            }
            $input = array(
                'store_id'  => $invite->member_store_id,
                'member_id' => $invite->id,
                'type_send' => $type,
                'type_checkin' => 'sendAtRick'
            );
            $smsSendLog = SmsSendLog::create($input);
            $smsSendLog->save();
        }
    }
}

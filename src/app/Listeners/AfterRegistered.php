<?php

namespace App\Listeners;

use App\Mail\AdminAfterRegistered;
use App\Mail\WelcomeAfterRegistered;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class AfterRegistered
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        Mail::to($event->user)->send(new WelcomeAfterRegistered($event->user));
        Mail::to(config('pushmeup.setting.admin_mail'))->send(new AdminAfterRegistered($event->user));
    }
}

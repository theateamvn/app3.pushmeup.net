<?php

namespace App\Criteria;

use Auth;
use App\Models\User;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class UserListByRoleCriteria implements CriteriaInterface
{

    private $request;

    /**
     * ReviewProviderCriteria constructor.
     * @param array $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $modelSS
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $input = $this->request->all();
        $role = substr($input['search'],10)==='user'? 0:2;
        $userId = Auth::user()->id;
        $user_role = Auth::user()->role;
        //agent get users
        if(isset($userId) && $user_role == 2 && $role == 0){
            $model = $model->where('created_by', $userId)->where('role','=', 0)->orderBy('id', 'DESC');
        }
        //admin get all users
        if(isset($userId) && $user_role == 1 && $role == 0){
            $model = $model->where('role','=', 0)->orderBy('id', 'DESC');
        }
        //admin get admin and agents
        if(isset($userId) && $user_role == 1 && $role == 2 ){
            $model = $model->where('created_by', $userId)->where('role','<>', 0)->orderBy('id', 'DESC');
        }
        return $model;
    }
}

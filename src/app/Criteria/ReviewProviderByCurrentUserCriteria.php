<?php

namespace App\Criteria;

use App\Models\ReviewProvider;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ReviewProviderByCurrentUserCriteria
 * @package namespace App\Criteria;
 */
class ReviewProviderByCurrentUserCriteria implements CriteriaInterface
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * ReviewProviderByUserCriteria constructor.
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
    }


    /**
     * Apply criteria in query repository
     *
     * @param ReviewProvider      $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $store = Auth::user()->store();
        if(isset($store)){
            $model = $model->where('store_id',$store->id);
        }
        return $model;
    }
}

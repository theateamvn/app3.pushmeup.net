<?php

namespace App\Criteria;

use Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class ByStoreIdCriteria implements CriteriaInterface
{
    private $storeId;
    public function __construct($storeId)
    {
        $this->storeId = $storeId;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        if(isset($this->storeId)){
            $model = $model->where('store_id',$this->storeId);
        }
        return $model;
    }
}

<?php

namespace App\Criteria;

use App\Models\Setting;
use Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SettingByUserCriteria
 * @package namespace App\Criteria;
 */
class SettingByUserCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param Setting             $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $store = Auth::user()->store();
        if(isset($store)){
            $model = $model->where('store_id',$store->id)->orderBy('id', 'DESC');
        }
        return $model;
    }
}

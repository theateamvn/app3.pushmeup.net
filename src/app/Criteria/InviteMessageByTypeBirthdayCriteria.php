<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class InviteMessageByTypeCriteria
 * @package namespace App\Criteria;
 */
class InviteMessageByTypeBirthdayCriteria implements CriteriaInterface
{
    private $type;

    /**
     * InviteMessageByTypeCriteria constructor.
     * @param int $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }


    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('type',4);
        return $model;
    }
}

<?php

namespace App\Criteria;

use App\Models\Setting;
use Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SettingByUserCriteria
 * @package namespace App\Criteria;
 */
class SettingByUserCriteria implements CriteriaInterface
{
    private $storeId;
    public function __construct($storeId)
    {
        $this->storeId = $storeId;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        if(isset($this->storeId)){
            $model = $model->where('store_id',$this->storeId)->orderBy('id', 'DESC');
        }
        return $model;
    }
}

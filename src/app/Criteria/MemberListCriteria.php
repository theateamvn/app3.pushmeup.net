<?php

namespace App\Criteria;

use Auth;
use DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class MemberListCriteria implements CriteriaInterface
{

    private $request;

    /**
     * ReviewProviderByStoreCriteria constructor.
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        $store = Auth::user()->store();
        if(isset($this->request['from'], $this->request['to'])){
            $model = $model->whereBetween('member_created_at',[$this->request['from'], $this->request['to']]);
        }
        if(isset($store)){
            $model = $model->where('member_store_id',$store->id)
                            ->where('member_status', '<>', '-1');
            //$model   =  json_decode(json_encode($model), True);
        }
        return $model;
    }
}

<?php

namespace App\Criteria;

use Auth;
use App\Models\User;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class BirthdayListByStoreIdCriteria implements CriteriaInterface
{

    /**
     * Apply criteria in query repository
     *
     * @param                     $modelSS
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $store = Auth::user()->store();
        if(isset($store)){
            $model = $model->where('birthday_store_id',$store->id)->orderBy('id', 'DESC');
        }
        return $model;
    }
}

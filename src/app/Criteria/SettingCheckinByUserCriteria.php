<?php

namespace App\Criteria;

use App\Models\ConfigCheckin;
use Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SettingByUserCriteria
 * @package namespace App\Criteria;
 */
class SettingCheckinByUserCriteria implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        $store = Auth::user()->store();
        if(isset($store)){
            $model = $model->where('store_id',$store->id)->orderBy('id', 'DESC');
        }
        return $model;
    }
}

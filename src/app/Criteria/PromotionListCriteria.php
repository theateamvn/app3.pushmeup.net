<?php

namespace App\Criteria;

use Auth;
use DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ByUserStoreCriteria
 * @package namespace App\Criteria;
 */
class PromotionListCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $store = Auth::user()->store();
        if(isset($store)){
            $model = $model->where('promotion_store_id',$store->id)
                            ->where('promotion_status', '<>', '-1');
            //$model   =  json_decode(json_encode($model), True);
        }
        return $model;
    }
}

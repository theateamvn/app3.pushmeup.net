<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ReviewCriteria
 * @package namespace App\Criteria;
 */
class ReviewCriteria implements CriteriaInterface
{

    private $request;

    /**
     * ReviewProviderByStoreCriteria constructor.
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if(isset($this->request['min_rating'], $this->request['max_rating'])){
            $model = $model->whereBetween('rating',[$this->request['min_rating'], $this->request['max_rating']]);
        }
        if(isset($this->request['provider'])){
            if ($this->request['provider'] != 'all') {
              $model = $model->where('provider_name', $this->request['provider']);
            }
        }
        if(isset($this->request['time'])){
            if ($this->request['time'] != 'all') {
              if ($this->request['time'] == 'this_week') {
                $time = date('Y-m-d H:i:s',strtotime('Monday -1 week'));
                $model = $model->where('published_at', '>=', $time);
              }
              elseif ($this->request['time'] == 'last_week') {
                $time_begin = date('Y-m-d H:i:s',strtotime('Monday -2 week'));
                $time_end = date('Y-m-d H:i:s',strtotime('Monday -1 week'));
                $model = $model->whereBetween('published_at', [$time_begin, $time_end]);
              }
              elseif ($this->request['time'] == 'last_month') {
                $time = date('Y-m-d H:i:s',strtotime('Last Month'));
                $model = $model->where('published_at', '>=', $time);
              } else {
                $model = $model->where('published_at', $this->request['time']);
              }
            }
        }
        return $model;
    }
}

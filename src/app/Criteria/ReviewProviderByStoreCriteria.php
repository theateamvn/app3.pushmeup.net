<?php

namespace App\Criteria;

use App\Models\ReviewProvider;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ReviewProviderByStoreCriteria
 * @package namespace App\Criteria;
 */
class ReviewProviderByStoreCriteria implements CriteriaInterface
{
    const REVIEW_REQUEST_FILTER_KEY_NAME = 'store_id';

    private $request;

    /**
     * ReviewProviderByStoreCriteria constructor.
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }


    /**
     * Apply criteria in query repository
     *
     * @param ReviewProvider      $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if(isset($this->request[static::REVIEW_REQUEST_FILTER_KEY_NAME])){
            $model = $model->where('store_id',$this->request[static::REVIEW_REQUEST_FILTER_KEY_NAME]);
        }
        return $model;
    }
}

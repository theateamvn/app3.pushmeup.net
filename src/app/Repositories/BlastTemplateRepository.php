<?php

namespace App\Repositories;

use App\Models\BlastTemplate;
use InfyOm\Generator\Common\BaseRepository;

class BlastTemplateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language',
        'content' => 'like',
        'extra_data',
        'type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BlastTemplate::class;
    }
}

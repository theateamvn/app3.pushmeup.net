<?php

namespace App\Repositories;

use App\Models\MemberCheckout;
use InfyOm\Generator\Common\BaseRepository;

class MemberCheckoutRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'store_id',
        'checkin_id',
        'member_id',
        'price_services',
        'price_point',
        'member_checkout_status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MemberCheckout::class;
    }
}

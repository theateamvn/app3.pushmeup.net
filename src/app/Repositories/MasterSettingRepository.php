<?php

namespace App\Repositories;

use App\Models\MasterSetting;
use InfyOm\Generator\Common\BaseRepository;

class MasterSettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'key',
        'value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MasterSetting::class;
    }
}

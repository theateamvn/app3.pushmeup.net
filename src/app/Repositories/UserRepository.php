<?php

namespace App\Repositories;

use App\Models\User;
use InfyOm\Generator\Common\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'username',
        'full_name',
        'email',
        'phone',
        'role',
        'time_zone',
        'status',
        'note',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}

<?php

namespace App\Repositories;

use App\Models\Birthday;
use InfyOm\Generator\Common\BaseRepository;

class BirthdayRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'birthday_send_date',
        'birthday_value',
        'birthday_type',
        'birthday_allow',
        'birthday_message',
        'birthday_status',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Birthday::class;
    }
}

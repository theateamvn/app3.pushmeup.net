<?php

namespace App\Repositories;

use App\Models\PromotionAtRisk;
use InfyOm\Generator\Common\BaseRepository;

class PromotionAtRiskRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'promotion_store_id',
        'promotion_value',
        'promotion_type',
        'promotion_message'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PromotionAtRisk::class;
    }
}

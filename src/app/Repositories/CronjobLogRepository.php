<?php

namespace App\Repositories;

use App\Models\CronjobLog;
use InfyOm\Generator\Common\BaseRepository;

class CronjobLogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'cronjob_id',
        'cronjob_log',
        'cronjob_create_at',
        'cronjob_log_status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CronjobLog::class;
    }
}

<?php

namespace App\Repositories;

use App\Models\MasterRedirectBackground;
use InfyOm\Generator\Common\BaseRepository;

class MasterRedirectBackgroundRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'path',
        'url',
        'enabled'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MasterRedirectBackground::class;
    }
}

<?php

namespace App\Repositories;

use App\Models\ConfigCheckin;
use InfyOm\Generator\Common\BaseRepository;

class SettingCheckinRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'config_checkin_name',
        'config_checkin_value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ConfigCheckin::class;
    }
}

<?php

namespace App\Repositories;

use App\Models\BlastContact;
use InfyOm\Generator\Common\BaseRepository;

class BlastContactRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'blast_template_id',
        'client_contact_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BlastContact::class;
    }
}

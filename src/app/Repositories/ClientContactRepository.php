<?php

namespace App\Repositories;

use App\Models\ClientContact;
use InfyOm\Generator\Common\BaseRepository;

class ClientContactRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'phone',
        'email',
        'name',
        'store_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ClientContact::class;
    }
}

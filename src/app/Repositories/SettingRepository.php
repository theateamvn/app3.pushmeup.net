<?php

namespace App\Repositories;

use App\Models\Setting;
use InfyOm\Generator\Common\BaseRepository;

class SettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'image_message_enabled',
        'image_message_id',
        'review_goal',
        'sender_email',
        'sender_name',
        'email_title',
        'email_title_promo',
        'email_title_birthday',
        'email_title_atrisk',
        'sms_username',
        'sms_password',
        'sms_user',
        'link_review_google',
        'link_review_facebook',
        'link_review_yelp'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Setting::class;
    }
}

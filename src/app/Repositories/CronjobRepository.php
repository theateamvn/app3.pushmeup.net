<?php

namespace App\Repositories;

use App\Models\Cronjob;
use InfyOm\Generator\Common\BaseRepository;

class CronjobRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cronjob_store_id',
        'cronjob_type',
        'cronjob_type_id',
        'cronjob_send_at',
        'cronjob_status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cronjob::class;
    }
}

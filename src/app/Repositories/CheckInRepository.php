<?php

namespace App\Repositories;

use App\Models\CheckIn;
use InfyOm\Generator\Common\BaseRepository;

class CheckInRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'store_id',
        'member_id',
        'checkin_status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CheckIn::class;
    }
}

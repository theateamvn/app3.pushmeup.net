<?php

namespace App\Repositories;

use App\Models\ReviewLink;
use InfyOm\Generator\Common\BaseRepository;

class ReviewLinkRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'storeId',
        'providerName',
        'inviteId',
        'agent'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReviewLink::class;
    }
}
<?php

namespace App\Repositories;

use App\Models\StoreUser;
use InfyOm\Generator\Common\BaseRepository;

class StoreUserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreUser::class;
    }
}

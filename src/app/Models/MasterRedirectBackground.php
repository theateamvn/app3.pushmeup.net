<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class MasterRedirectBackground
 *
 * @package App\Models
 * @version January 5, 2017, 4:20 am UTC
 * @property int $id
 * @property string $path
 * @property string $url
 * @property bool $enabled
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MasterRedirectBackground whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MasterRedirectBackground wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MasterRedirectBackground whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MasterRedirectBackground whereEnabled($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MasterRedirectBackground whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MasterRedirectBackground whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MasterRedirectBackground extends Model
{

    public $table = 'master_redirect_background';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'path',
        'url',
        'enabled'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'path' => 'string',
        'url' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}

<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Review
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Member extends Model
{

    public $table = 'members';

    const CREATED_AT = 'member_created_at';
    const UPDATED_AT = 'member_updated_at';


    public $fillable = [
        'id',
        'member_store_id',
        'member_fullname',
        'member_phone',
        'member_date_birth',
        'member_month_birth',
        'member_email',
        'member_level',
        'member_note',
        'member_status',
        'joined_at',
        'member_created_by',
        'member_updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'member_store_id' => 'integer',
        'store_id' => 'integer',
        'member_fullname' => 'string',
        'member_phone' => 'string',
        'member_date_birth' => 'integer',
        'member_month_birth' => 'integer',
        'member_email' => 'string',
        'joined_at' => 'string',
        'member_level' => 'integer',
        'member_note' => 'string',
        'member_status' => 'integer',
        'member_created_by' => 'integer',
        'member_updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'member_fullname' => 'required|max:255',
        'member_email' => 'max:255|unique:members',
        'member_phone' => 'required|max:20'
    ];

    public static $updateRules = [
        'member_fullname' => 'required|max:255',
        'member_email' => 'max:255|unique:members',
        'member_phone' => 'required|max:20|unique:members'
    ];

    public static function rulesUpdate($id, $store_id){
        return array(
            'member_fullname' => 'required|max:255',
            'member_email' => 'max:255',
            'member_phone' => 'required|max:20'
        );
    }

}

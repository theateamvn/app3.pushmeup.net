<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Review
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Promotion extends Model
{

    public $table = 'promotions';

    const CREATED_AT = 'promotion_created_at';
    const UPDATED_AT = 'promotion_updated_at';


    public $fillable = [
        'promotion_store_id',
        'promotion_offer',
        'promotion_value',
        'promotion_type',
        'promotion_start',
        'promotion_send_date',
        'promotion_end',
        'promotion_email',
        'promotion_message',
        'promotion_image',
        'promotion_send_members',
        'promotion_status',
        'promotion_created_by',
        'promotion_updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                   => 'integer',
        'promotion_store_id'   => 'integer',
        'promotion_offer'      => 'string',
        'promotion_value'      => 'string',
        'promotion_type'       => 'string',
        'promotion_start'      => 'string',
        'promotion_end'        => 'string',
        'promotion_send_date'  => 'string',
        'promotion_email'      => 'string',
        'promotion_message'    => 'string',
        'promotion_image'      => 'string',
        'promotion_send_members'=> 'string',
        'promotion_status'     => 'integer',
        'promotion_created_by' => 'integer',
        'promotion_updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'promotion_email'   => 'required|max:160',
        'promotion_message' => 'max:160',
        'promotion_value'   => 'required',
        'promotion_type'    => 'required',
        'promotion_offer'   => 'required',
        'promotion_start'   => 'required',
        'promotion_end'     => 'required',
        'promotion_send_date' => 'required',
    ];

    public static $updateRules = [
        'promotion_email' => 'max:160',
        'promotion_message' => 'max:160',
        'promotion_value'   => 'required',
        'promotion_type'    => 'required',
        'promotion_offer'   => 'required',
        'promotion_start'   => 'required',
        'promotion_end'     => 'required',
        'promotion_send_date' => 'required',
    ];

    public static function rulesUpdate($id){
        return array(
        );
    }

}

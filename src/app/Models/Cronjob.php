<?php

namespace App\Models;

use Eloquent as Model;

class Cronjob extends Model
{

    public $table = 'cronjob_send';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'cronjob_store_id',
        'cronjob_type',
        'cronjob_type_id',
        'cronjob_send_at',
        'cronjob_status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'        => 'integer',
        'cronjob_store_id'  => 'integer',
        'cronjob_type'      => 'string',
        'cronjob_type_id'    => 'integer',
        'cronjob_send_at'    => 'string',
        'cronjob_status'    => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }
}

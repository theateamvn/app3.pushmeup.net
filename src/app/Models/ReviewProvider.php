<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\Activitylog\LogsActivity;
use Spatie\Activitylog\LogsActivityInterface;

/**
 * Class ReviewProvider
 *
 * @package App\Models
 * @version November 15, 2016, 2:19 pm UTC
 * @property integer $id
 * @property integer $store_id
 * @property string $name
 * @property string $reviewed_key
 * @property string $reviewed_url
 * @property string $extra_data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Store $store
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReviewProvider whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReviewProvider whereStoreId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReviewProvider whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReviewProvider whereReviewedKey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReviewProvider whereReviewedUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReviewProvider whereExtraData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReviewProvider whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReviewProvider whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReviewProvider extends Model implements LogsActivityInterface
{
    use LogsActivity;

    public $table = 'review_providers';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const PROVIDER_GOOGLE = 'GOOGLE';
    const PROVIDER_FACEBOOK = 'FACEBOOK';
    const PROVIDER_YELP = 'YELP';
    const PROVIDER_TRIPADVISOR = "TRIPADVISOR";
    const PROVIDER_AGODA = "AGODA";
    const PROVIDER_BOOKING = "BOOKING";

    public $fillable = [
        'store_id',
        'name',
        'reviewed_key',
        'reviewed_url',
        'extra_data'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'name' => 'string',
        'reviewed_key' => 'string',
        'reviewed_url' => 'string',
        'extra_data' => 'array'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id'=>'required',
        'name'=>'required'
    ];

    public static $createRules = [
        'store_id'=>'required',
        'name'=>'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }

    /**
     * Get the message that needs to be logged for the given event.
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return 'Review provider "' . $this->name . '" was created';
        }

        if ($eventName == 'updated')
        {
            return 'Review provider "' . $this->name . '" was updated';
        }

        if ($eventName == 'deleted')
        {
            return 'Review provider "' . $this->name . '" was deleted';
        }

        return '';
    }
}

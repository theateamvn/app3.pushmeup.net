<?php

namespace App\Models;

use Eloquent as Model;

class ConfigCheckin extends Model
{

    public $table = 'config_checkin';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'id',
        'store_id',
        'config_checkin_name',
        'config_checkin_value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                    => 'integer',
        'store_id'              => 'integer',
        'config_checkin_name'   => 'string',
        'config_checkin_value'  => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }
}

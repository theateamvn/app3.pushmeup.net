<?php

namespace App\Models;

use Eloquent as Model;

class SmsSendLog extends Model
{

    public $table = 'checkin_smssendlog';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'id',
        'store_id',
        'member_id',
        'type_send',
        'type_checkin',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => 'integer',
        'store_id'          => 'integer',
        'member_id'         => 'integer',
        'type_send'         => 'string',
        'type_checkin'      => 'string',
        'status'         => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }
}

<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Feedback
 * @package App\Models
 * @version February 23, 2017, 2:42 pm UTC
 */
class ReviewLink extends Model
{

    public $table = 'review_links';
    
    const CREATED_AT = 'created_at';



    public $fillable = [
        'storeId',
        'providerName',
        'inviteId',
        'agent'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'providerName' => 'string',
        'inviteId' => 'integer',
        'agent' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }
}

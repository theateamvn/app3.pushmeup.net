<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class InviteMessage
 *
 * @package App\Models
 * @version December 14, 2016, 2:54 am UTC
 * @property integer $id
 * @property integer $store_id
 * @property string $language
 * @property string $content
 * @property array $extra_data
 * @property integer $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Store $store
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InviteMessage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InviteMessage whereStoreId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InviteMessage whereLanguage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InviteMessage whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InviteMessage whereExtraData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InviteMessage whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InviteMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InviteMessage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class InviteMessage extends Model
{

    public $table = 'invite_messages';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const TYPE_SMS = 0;
    const TYPE_MAIL = 1;
    const TYPE_MMS = 2;
    const TYPE_PROMO = 3;
    const TYPE_EMAIL_PROMO = 4;

    public $fillable = [
        'store_id',
        'language',
        'content',
        'extra_data',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'language' => 'string',
        'content' => 'string',
        'extra_data' => 'array',
        'type' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id' => 'required',
        'type' => 'required',
        'language' => 'required',
        'content' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }

    public function getMmsMediaPath()
    {
        return array_get($this->extra_data, 'mediaUrl');
    }

    public static function createDefaultContent($storeId)
    {
        static::createDefaultSMS($storeId);
        static::createDefaultMMS($storeId);
        static::createDefaultEmail($storeId);
        static::createDefaultEmailPromo($storeId);
        static::createDefaultPROMO($storeId);
    }

    private static function createDefaultSMS($storeId)
    {
        return static::create([
            'store_id' => $storeId,
            'type' => static::TYPE_SMS,
            'language' => 'en',
            'content' => config('pushmeup.templates.sms')
        ]);
    }

    private static function createDefaultEmail($storeId)
    {
        return static::create([
            'store_id' => $storeId,
            'type' => static::TYPE_MAIL,
            'language' => 'en',
            'content' => config('pushmeup.templates.email')
        ]);
    }
    private static function createDefaultEmailPromo($storeId)
    {
        return static::create([
            'store_id' => $storeId,
            'type' => static::TYPE_EMAIL_PROMO,
            'language' => 'en',
            'content' => config('pushmeup.templates.email_promo')
        ]);
    }

    private static function createDefaultMMS($storeId)
    {
        return static::create([
            'store_id' => $storeId,
            'type' => static::TYPE_MMS,
            'language' => 'en',
            'content' => config('pushmeup.templates.mms.content'),
            'extra_data' => [
                'mediaUrl' => config('pushmeup.templates.mms.mediaUrl')
            ]
        ]);
    }

    private static function createDefaultPROMO($storeId)
    {
        return static::create([
            'store_id' => $storeId,
            'type' => static::TYPE_PROMO,
            'language' => 'en',
            'content' => config('pushmeup.templates.promo')
        ]);
    }
}

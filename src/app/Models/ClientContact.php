<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ClientContact
 *
 * @package App\Models
 * @version February 27, 2017, 3:44 pm UTC
 * @property int $id
 * @property string $phone
 * @property string $email
 * @property string $name
 * @property int $store_id
 * @property string $last_sent_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Store $store
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BlastTemplate[] $templates
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ClientContact whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ClientContact wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ClientContact whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ClientContact whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ClientContact whereStoreId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ClientContact whereLastSentAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ClientContact whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ClientContact whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClientContact extends Model
{

    public $table = 'client_contacts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'phone',
        'email',
        'name',
        'store_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'phone' => 'string',
        'email' => 'string',
        'name' => 'string',
        'store_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function templates()
    {
        return $this->belongsToMany(\App\Models\BlastTemplate::class,'blast_contact');
    }
}

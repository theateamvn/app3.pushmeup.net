<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 18/12/2016
 * Time: 12:00 PM
 */

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Model;

trait UniqueSluggable
{
    /**
     * Generate a unique slug.
     * If it already exists, a number suffix will be appended.
     * It probably works only with MySQL.
     *
     * @link http://chrishayes.ca/blog/code/laravel-4-generating-unique-slugs-elegantly
     *
     * @param Model $model
     * @param string $value
     * @return string
     */
    static function getUniqueSlug(Model $model, $value)
    {
        $slug = str_slug($value);
        $slugCount = count($model->whereRaw("slug REGEXP '^{$slug}(-[0-9]+)?$' and id != '{$model->id}'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

    /**
     * @return array
     */
    public function sluggable()
    {
        return [
            'source' => '',
            'target' => 'slug'
        ];
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $sluggable = $model->sluggable();
            $source = array_get($sluggable, 'source');
            $target = array_get($sluggable, 'target');
            $model->{$target} = static::getUniqueSlug($model, $model->{$source});
        });

        static::updating(function ($model) {
            $sluggable = $model->sluggable();
            $source = array_get($sluggable, 'source');
            $target = array_get($sluggable, 'target');
            $model->{$target} = static::getUniqueSlug($model, $model->{$source});
        });
    }
}
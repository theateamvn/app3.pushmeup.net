<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Review
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class UserCheckin extends Model
{

    public $table = 'user_checkin';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'id',
        'user_id',
        'slug',
        'secret_key',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'            => 'integer',
        'user_id'       => 'integer',
        'slug'          => 'string',
        'secret_key'    => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'slug' => 'required',
        'secret_key' => 'required',
    ];

    public static $updateRules = [
    ];

}

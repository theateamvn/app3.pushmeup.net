<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class MasterSetting
 *
 * @package App\Models
 * @version February 8, 2017, 2:41 pm UTC
 * @property int $id
 * @property string $key
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MasterSetting whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MasterSetting whereKey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MasterSetting whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MasterSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MasterSetting whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MasterSetting extends Model
{

    public $table = 'master_settings';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'key',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'key' => 'string',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    
}

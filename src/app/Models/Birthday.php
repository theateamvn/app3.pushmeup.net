<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Review
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Birthday extends Model
{

    public $table = 'birthdays';

    const CREATED_AT = 'birthday_created_at';
    const UPDATED_AT = 'birthday_updated_at';


    public $fillable = [
        'id',
        'birthday_store_id',
        'birthday_send_date',
        'birthday_value',
        'birthday_type',
        'birthday_allow',
        'birthday_message',
        'birthday_status',
        'birthday_created_by',
        'birthday_updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'birthday_store_id' => 'integer',
        'birthday_send_date' => 'integer',
        'birthday_value' => 'integer',
        'birthday_type' => 'string',
        'birthday_allow' => 'integer',
        'birthday_message' => 'string',
        'birthday_status' => 'integer',
        'birthday_created_by' => 'integer',
        'birthday_updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'birthday_message' => 'required|max:160',
        'birthday_value' => 'required',
    ];

    public static $updateRules = [
        'birthday_message' => 'required|max:160',
        'birthday_value' => 'required',
    ];

}

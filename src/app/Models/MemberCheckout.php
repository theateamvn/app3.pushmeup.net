<?php

namespace App\Models;

use Eloquent as Model;

class MemberCheckout extends Model
{

    public $table = 'members_checkout';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'id',
        'store_id',
        'checkin_id',
        'member_id',
        'price_services',
        'price_point',
        'member_checkout_status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => 'integer',
        'store_id'          => 'integer',
        'member_id'         => 'integer',
        'checkin_id'         => 'integer',
        'price_services'    => 'string',
        'price_point'       => 'string',
        'member_checkout_status'    => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id' => 'required',
        'store_id' => 'integer',
        'price_services' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }
}

<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Review
 *
 * @package App\Models
 * @version November 15, 2016, 2:20 pm UTC
 * @property integer $id
 * @property integer $store_id
 * @property string $reviewer_id
 * @property string $content
 * @property integer $rating
 * @property integer $real_rating
 * @property string $author
 * @property \Carbon\Carbon $published_at
 * @property string $provider_name
 * @property string $url
 * @property string $review_unique_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Store $store
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereStoreId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereReviewerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereRating($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereRealRating($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereAuthor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review wherePublishedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereProviderName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereReviewUniqueId($value)
 */
class Review extends Model
{

    public $table = 'reviews';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'store_id',
        'reviewer_id',
        'content',
        'rating',
        'real_rating',
        'author',
        'published_at',
        'provider_name',
        'url'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'reviewer_id' => 'string',
        'content' => 'string',
        'rating' => 'float',
        'real_rating' => 'float',
        'author' => 'string',
        'provider_name' => 'string',
        'url' => 'string',
        'published_at' => 'datetime',
        'review_unique_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }

    protected static function boot()
    {
        static::saving(function (Review $review) {
            $key = implode('-', [
                $review->store_id,
                $review->content,
                $review->author,
                $review->rating,
                $review->real_rating,
                $review->published_at
            ]);
            $review->review_unique_id = hash('md5', $key);
        });
        parent::boot();
    }

}

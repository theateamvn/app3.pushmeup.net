<?php

namespace App\Models;

use Eloquent as Model;

class CronjobLog extends Model
{

    public $table = 'cronjob_log';

    const CREATED_AT = 'cronjob_created_at';
    const UPDATED_AT = 'cronjob_created_at';

    public $fillable = [
        'id',
        'cronjob_id',
        'cronjob_log',
        'cronjob_log_status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                    => 'integer',
        'cronjob_id'            => 'integer',
        'cronjob_log'           => 'string',
        'cronjob_log_status'    => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }
}

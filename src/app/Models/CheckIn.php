<?php

namespace App\Models;

use Eloquent as Model;

class CheckIn extends Model
{

    public $table = 'members_checkin';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'id',
        'store_id',
        'member_id',
        'checkin_at',
        'checkout_at',
        'checkin_status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => 'integer',
        'store_id'          => 'integer',
        'member_id'         => 'integer',
        'checkin_at'         => 'string',
        'checkout_at'         => 'string',
        'checkin_status'    => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id' => 'required',
        'store_id' => 'integer',
        'member_id' => 'required',
        'member_id' => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class);
    }
}

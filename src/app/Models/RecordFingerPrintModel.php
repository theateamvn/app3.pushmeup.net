<?php

namespace App\Models;

use App\Observers\RecordFingerPrintObserver;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RecordFingerPrintModel
 *
 * @mixin \Eloquent
 * @property-read \App\Models\User $owner
 */
class RecordFingerPrintModel extends Model
{
  public static function boot()
  {
    $class = get_called_class();
    $class::observe(new RecordFingerPrintObserver());
    parent::boot();
  }

  public function owner(){
    return $this->belongsTo(User::class,'created_by');
  }
}

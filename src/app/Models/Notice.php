<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Notice
 *
 * @package App\Models
 * @version November 15, 2016, 2:22 pm UTC
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property boolean $enabled
 * @property string $started_at
 * @property string $ended_at
 * @property integer $type
 * @property string $created_by
 * @property string $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Notice whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Notice whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Notice whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Notice whereEnabled($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Notice whereStartedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Notice whereEndedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Notice whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Notice whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Notice whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Notice whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Notice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Notice extends Model
{

    public $table = 'notices';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'title',
        'content',
        'enabled',
        'started_at',
        'ended_at',
        'type',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'content' => 'string',
        'type' => 'integer',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'content' => 'required',
        'enabled' => 'required'
    ];


}

<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Review
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class PromotionAtRisk extends Model
{

    public $table = 'promotion_at_risk';

    const CREATED_AT = 'promotion_created_at';
    const UPDATED_AT = 'promotion_updated_at';


    public $fillable = [
        'id',
        'promotion_store_id',
        'promotion_value',
        'promotion_type',
        'promotion_message',
        'promotion_send_status',
        'promotion_status',
        'promotion_created_by',
        'promotion_updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                   => 'integer',
        'promotion_store_id'   => 'integer',
        'promotion_value'      => 'string',
        'promotion_type'       => 'string',
        'promotion_message'    => 'string',
        'promotion_send_status'=> 'integer',
        'promotion_status'     => 'integer',
        'promotion_created_by' => 'integer',
        'promotion_updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'promotion_message' => 'required|max:160',
        'promotion_value'   => 'required',
        'promotion_type'    => 'required',
    ];

    public static $updateRules = [
        'promotion_message' => 'required|max:160',
        'promotion_value'   => 'required',
        'promotion_type'    => 'required',
    ];

    public static function rulesUpdate($id){
        return array(
        );
    }

}

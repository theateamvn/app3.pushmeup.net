<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\Birthday;
use App\Repositories\BirthdayRepository;
use Faker\Factory;
use Request;
use DB;
class BirthdayService
{
    /**
     * @var BirthdayRepository
     */
    private $birthdayRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct(BirthdayRepository $Repository)
    {
        $this->birthdayRepository = $Repository;
        $this->faker = Factory::create();
    }

    public function create($input){
        $birthday =Birthday::create($input);
        $birthday->save();
        return $birthday;
    }

    public function update($input){
        $birthday=Birthday::update($input);
        $birthday->save();
        return $birthday;
    }

    public function getBirthdayList()
    {
        $birthday= DB::table('birthdays')->get()->toArray();
        $birthday=json_decode(json_encode($birthday),TRUE);
        return $birthday;
    }

}
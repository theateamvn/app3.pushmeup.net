<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\Member;
use App\Models\Cronjob;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Auth;
use Faker\Factory;
use Carbon\Carbon;
use Request;
use DB;
use App\Services\DataRequiredException;
class MemberService
{
    /**
     * @var MemberRepository
     */
    private $memberRepository;
    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct(MemberRepository $memberRepository)
    {
        $this->memberRepository = $memberRepository;
        //$this->faker = Factory::create();
    }

    public function create($input){
        $member =Member::create($input);
        $member->save();
        return $member;
    }

    public function update($input){
        $member=Member::update($input);
        $member->save();
        return $member;
    }

    public function getMemberListCheckin($slug)
    {
        // $store_id = $this->getCurrentUser()->store()->id;
        $store=DB::table('stores')->where('slug','=',$slug)->get()->toArray();
        $store=json_decode(json_encode($store),TRUE);
        $member= DB::table('members')->where('member_store_id', $store[0]['id'])->get()->toArray();
        $member=json_decode(json_encode($member),TRUE);
        return $member;
    }

    public function getMemberList($store_id)
    {
        $member= DB::table('members')->where([
            ['member_store_id', '=', $store_id],
            ['member_status', '<>', '-1']
        ])->get()->toArray();
        $member=json_decode(json_encode($member),TRUE);
        return $member;
    }

    public function updateLevel($member_id, $totlalCheckIn = 0, $store_id = 0){
        if($totlalCheckIn) {
            $level = 1;
            $regular    = $this->getConfigCheckin('member_regular', $store_id);
            $vip        = $this->getConfigCheckin('member_vip', $store_id);
            
            if($totlalCheckIn >= $regular ) {
                $level = 2;
            }
            if($vip <= $totlalCheckIn) {
                $level = 3;
            }
            $input = array( 'member_level' => $level);
            $this->memberRepository->update($input, $member_id);
        }
        return true;
    }

    public function updateMemberAtRick(){
        $memberAtRick = DB::table('members')
                            ->select('members.*', 'members_checkin.checkin_at')
                            ->join("store_users","store_users.store_id","=","members.member_store_id")
                            ->join("users","users.id","=","store_users.user_id")
                            ->leftJoin("promotion_at_risk","promotion_at_risk.promotion_store_id","=","store_users.store_id")
                            ->leftJoin("members_checkin","members_checkin.member_id","=","members.id")
                            ->where('members.member_status', 1)
                            ->where('promotion_at_risk.promotion_status', 1)
                            ->where('members.member_level', 1)
                            ->where('users.status', 1)
                            //->where('member_created_at','<=',$from)
                            ->get()->toArray();
        $memberAtRick = json_decode(json_encode($memberAtRick),TRUE);
       // var_dump($memberAtRick);
        if(count($memberAtRick)){
            foreach ($memberAtRick as $key => $member){
                $time_zone      = $this->getTimeZone($member['member_store_id']);
                $current_time   = strtotime(Carbon::now($time_zone)->format('Y-m-d H:i:s'));
                $next_time      = $current_time - (1*86400);
                $from           = date('Y-m-d H:i:s', $next_time);
                //echo $from.'--'.$member['member_created_at'];
                if($member['checkin_at'] != NULL) {
                    $from           = strtotime($from);
                    $member_time    = strtotime($member['checkin_at']);
                    //echo $member_time.'--'.$from;die;
                    if($member_time <= $from){
                        // update level
                        $input = array( 
                            'member_level' => 4
                        );
                        $this->memberRepository->update($input, $member['id']);
                        // update cron job
                        $croinjob = [
                            'cronjob_type'      => 'sendAtRick',
                            'cronjob_type_id'   => $member['id'],
                            'cronjob_store_id'  => $member['member_store_id'],
                            'cronjob_send_at'   => date('Y-m-d 08:00:00', ($current_time+86400))
                        ];
                        Cronjob::create($croinjob);
                        return true;
                    }
                } else {
                    $from           = strtotime($from);
                    $member_time    = strtotime($member['member_created_at']);
                    //echo $member_time.'--'.$from;die;
                    if($member_time <= $from){
                        // update level
                        $input = array( 
                            'member_level' => 4
                        );
                        $this->memberRepository->update($input, $member['id']);
                        // update cron job
                        $croinjob = [
                            'cronjob_type'      => 'sendAtRick',
                            'cronjob_type_id'   => $member['id'],
                            'cronjob_store_id'  => $member['member_store_id'],
                            'cronjob_send_at'   => date('Y-m-d 08:00:00', ($current_time+86400))
                        ];
                        Cronjob::create($croinjob);
                        return true;
                    }
                }
            }
        }
        return true;
    }

    public function updateMemberAtRick_bk(){
        $memberAtRick = DB::table('members')
                            ->select('members.*')
                            ->join("store_users","store_users.store_id","=","members.member_store_id")
                            ->join("users","users.id","=","store_users.user_id")
                            ->where('members.member_status', 1)
                            ->where('members.member_level', 1)
                            ->where('users.status', 1)
                            //->where('member_created_at','<=',$from)
                            ->get()->toArray();
        $memberAtRick = json_decode(json_encode($memberAtRick),TRUE);
        foreach ($memberAtRick as $key => $member){
            $time_zone      = $this->getTimeZone($member['member_store_id']);
            $current_time   = strtotime(Carbon::now($time_zone)->format('Y-m-d H:i:s'));
            $next_time      = $current_time - (1*86400);
            $from           = date('Y-m-d H:i:s', $next_time);
            //echo $from.'--'.$member['member_created_at'];
            $from           = strtotime($from);
            $member_time    = strtotime($member['member_created_at']);
            //echo $member_time.'--'.$from;die;
            if($member_time <= $from){
                // update level
                $input = array( 
                    'member_level' => 4
                );
                $this->memberRepository->update($input, $member['id']);
                // update cron job
                $croinjob = [
                    'cronjob_type'      => 'sendAtRick',
                    'cronjob_type_id'   => $member['id'],
                    'cronjob_store_id'  => $member['member_store_id'],
                    'cronjob_send_at'   => date('Y-m-d 08:00:00', ($current_time+86400))
                ];
                Cronjob::create($croinjob);
                return true;
            }
        }
        return true;
    }

    public function getMemberTotalPrice($id) {
        $total_price = 0;
        $total_price = DB::table('members_checkout')
                        ->where('member_id', $id)->sum("price_services");
        return $total_price;
    }

    public function getConfigCheckin($name, $store_id = 0) {
        //echo  $store_id;die;
        $result =  DB::table('config_checkin')->where(['status' => 1, 'config_checkin_name' => $name, 'store_id' => $store_id])->get()->toArray();
        return $result[0]->config_checkin_value;
    }
    public function getTimeZone($storeId) {
        $result = DB::table('users')
                    ->select('users.*')    
                    ->join("store_users","store_users.user_id","=","users.id")        
                    ->where('store_users.store_id', $storeId)
                    ->where('users.status', 1)
                    ->get();
        if($result[0]->time_zone == 0){
            return '+7:00';
        }
        return $result[0]->time_zone;
    }
    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 07/03/2017
 * Time: 9:57 PM
 */

namespace App\Services;


use App\Criteria\ByUserStoreCriteria;
use App\Facades\Stores;
use App\Models\BlastTemplate;
use App\Models\ClientContact;
use App\Models\Invite;
use App\Notifications\SendBlast;
use App\Repositories\BlastTemplateRepository;
use App\Repositories\ClientContactRepository;
use App\Repositories\InviteRepository;
use Carbon\Carbon;

class BlastSmsService
{
    /** @var ClientContactRepository */
    private $clientContactRepository;
    /** @var InviteRepository */
    private $inviteRepository;

    /** @var  BlastTemplateRepository */
    private $blastTemplateRepository;

    /**
     * BlastSmsService constructor.
     * @param ClientContactRepository $clientContactRepository
     * @param InviteRepository $inviteRepository
     * @param BlastTemplateRepository $blastTemplateRepository
     */
    public function __construct(ClientContactRepository $clientContactRepository, InviteRepository $inviteRepository, BlastTemplateRepository $blastTemplateRepository)
    {
        $this->clientContactRepository = $clientContactRepository;
        $this->inviteRepository = $inviteRepository;
        $this->blastTemplateRepository = $blastTemplateRepository;
    }

    /**
     * @param $storeId
     * @return int
     */
    public function synchronizeClientContact($storeId)
    {
        $count = 0;
        ClientContact::where('store_id', $storeId)->delete();

        $invites = Invite::where('store_id', $storeId)->groupby(['phone', 'name'])->get(['phone', 'name']);
        /** @var Invite $invite */
        foreach ($invites as $invite) {
            $clientContact = ClientContact::where('store_id', $storeId)->where('phone', $invite->phone)->first();
            if (!$clientContact && $invite->phone) {
                $this->clientContactRepository->create([
                    'phone' => $invite->phone,
                    'name' => $invite->name,
                    'store_id' => $storeId
                ]);
                $count++;
            }
        }
        return $count;
    }

    /**
     * @return int
     */
    public function synchronizeClientContactByUser()
    {
        $storeId = Stores::getCurrentStore()->id;
        return $this->synchronizeClientContact($storeId);
    }

    /**
     * @param BlastTemplate $blastTemplate
     */
    public function sendAll($blastTemplate)
    {
        $this->clientContactRepository->pushCriteria(new ByUserStoreCriteria());
        $contacts = $this->clientContactRepository->all(['id']);

        $blastTemplate->contacts()->sync($contacts);
        \Notification::send($blastTemplate, new SendBlast());
    }

    private function countBlastSmsSentByType($storeId, $type, $startDate = "1900-01-01", $endDate = 'now')
    {
        return BlastTemplate::whereBetween('last_sent_at', [new Carbon($startDate), new Carbon($endDate)])
            ->where('store_id', $storeId)
            ->where('type', $type)
            ->count();
    }

    private function countBlastSms($storeId, $startDate = "1900-01-01", $endDate = 'now')
    {
        return BlastTemplate::whereBetween('created_at', [new Carbon($startDate), new Carbon($endDate)])
            ->where('store_id', $storeId)
            ->count();
    }

    public function statisticMessageByDateRange($storeId, $startDate, $endDate = 'now')
    {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        $blastSmsSentCount = $this->countBlastSmsSentByType($storeId, BlastTemplate::TYPE_SMS, $startDate, $endDate);
        $blastMmsSentCount = $this->countBlastSmsSentByType($storeId, BlastTemplate::TYPE_MMS, $startDate, $endDate);
        $total = $this->countBlastSms($storeId, $startDate, $endDate);
        $result['count_sms'] = $blastSmsSentCount;
        $result['count_mms'] = $blastMmsSentCount;
        $result['total'] = $total;
        return $result;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;

use Activity;
use App\Http\Requests\API\CreateCronjobAPIRequest;
use App\Models\Store;
use App\Models\CheckIn;
use App\Models\Cronjob;
use App\Models\Member;
use App\Criteria\InviteMessageByTypeCriteria;
use App\Criteria\InviteMessageByTypePromoCriteria;
use App\Criteria\InviteMessageByTypeBirthdayCriteria;
use App\Criteria\ByStoreIdCriteria;
use App\Services\MemberService;
use App\Repositories\CronjobRepository;
use App\Repositories\MemberRepository;
use App\Repositories\CheckInRepository;
use App\Repositories\InviteMessageRepository;
use App\Notifications\SendCheckin;
use App\Notifications\SendPromotion;
use App\Notifications\SendBirthday;
use App\Notifications\SendAtRisk;
use App\Services\Bijective;
use Illuminate\Support\Facades\Auth;
use Faker\Factory;
use Notification;
use DB;
use Request;

class CronjobService
{
    private $checkInRepository;
    private $cronjobRepository;
    private $faker;
    private $membersService;
    private $inviteMessageRepo;
    /**
     * UserService constructor.
     * @param $checkInRepository
     */
    public function __construct(CheckInRepository $checkInRepository,CronjobRepository $cronjobRepository, MemberService $membersService, MemberRepository $memberRepository, InviteMessageRepository $inviteRepo)
    {
        $this->cronjobRepository = $cronjobRepository;
        $this->checkInRepository = $checkInRepository;
        $this->memberRepository = $memberRepository;
        $this->inviteMessageRepo = $inviteRepo;
        $this->membersService = $membersService;
        $this->faker = Factory::create();
    }

    public function cronjobRunCheck($input = null){
        $result = array();
        $current_time   = time();
        $time = date('Y-m-d H:i:s', $current_time);
        // check cronjob: 1: running, 2:error
        $checkCronjob = \DB::table('cronjob_send')->where([
            ['cronjob_status', '=', 0]])
            ->count();
        // $checkCronjob = \DB::table('cronjob_send')->count();
        if($checkCronjob) {
            $getlistCronjob = DB::table('cronjob_send')
                        ->selectRaw('*')
                        ->where('cronjob_status',  0)
                        ->where('cronjob_send_at','<=', $time)
                        ->orderBy('cronjob_send_at', 'ASC')
                        ->get()
                        ->toArray();
            if(count($getlistCronjob)) {
                $firstCronjob = $getlistCronjob[0];
                $type = $firstCronjob->cronjob_type;
                if($type == 'promotion') {
                    $result = $this->promotionCronjob($firstCronjob);
                } else if($type == 'checkin') {
                    $result = $this->checkInCronjob($firstCronjob);
                } else if($type == 'birthday') {
                    $result = $this->birthdayCronjob($firstCronjob);
                } else if($type == 'sendAtRick') {
                    $result = $this->atRickCronjob($firstCronjob);
                }
            } else {
                $result = array(
                    'success'   => true,
                    'messenger' => 'Cronjob no has request!'
                );
            }
        } else {
            $result = array(
                'success'   => false,
                'messenger' => 'Cronjob is runnning!'
            );
        }
        return $result;
    }
    public function checkInCronjob($firstCronjob = null, $checkOut = false){
        set_time_limit(0);
        $checkInId  = $firstCronjob->cronjob_type_id;
        $storeId    = $firstCronjob->cronjob_store_id;
        if(!$checkOut){
            $checkInDetail = DB::table('members_checkin')
                            ->select('members.*', 'stores.name', 'stores.address', 'stores.slug')
                            ->leftJoin("members","members_checkin.member_id","=","members.id")
                            ->leftJoin("stores","stores.id","=","members.member_store_id")
                            ->where('members_checkin.checkin_status', 1)
                            ->where('members_checkin.id', $checkInId)
                            ->get()
                            ->toArray();
        } else {
            $checkInDetail = DB::table('members_checkin')
                            ->select('members.*', 'stores.name', 'stores.address', 'stores.slug')
                            ->leftJoin("members","members_checkin.member_id","=","members.id")
                            ->leftJoin("stores","stores.id","=","members.member_store_id")
                            ->where('members_checkin.id', $checkInId)
                            ->get()
                            ->toArray();
        }
        if(count($checkInDetail)){
            $phoneInfo = $checkInDetail[0];
            $bijective = resolve(Bijective::class);
            $invite_link = route('review.redirect', ['shortId' => $bijective->encode($phoneInfo->id)]);
            $review_link = route('review.redirect.slug.id', ['slug' => $phoneInfo->slug, 'id' => $phoneInfo->id]);
            $phoneInfo->invite_link = $invite_link;
            $phoneInfo->review_link = $review_link;
            $via = ['sms'];
            // change status 
            DB::table('cronjob_send')->where('id',  $firstCronjob->id)->update(array(
                'cronjob_status'=>1
            ));
            Notification::send($phoneInfo, new SendCheckin($via, $storeId));
            Activity::log('Send checkin sms/mms');
            DB::table('cronjob_send')->where('id',  $firstCronjob->id)->update(array(
                'cronjob_status'=>2
            ));
            DB::table('members_checkin')->where('id',  $checkInId)->update(array(
                'checkin_status'=>2
            ));
            $result = array(
                'success'   => true,
                'messenger' => 'Send success!'
            );
        } else {
            $result = array(
                'success'   => false,
                'messenger' => 'Checkin can not get record!'
            );
        }
        return $result;
    }
    public function promotionCronjob($firstCronjob = null, $sendNow = false) {
        set_time_limit(0);
        $result = array();
        $typeId     = $firstCronjob->cronjob_type_id;
        $storeId    = $firstCronjob->cronjob_store_id;
        if(!$sendNow){
            $promotionDetail = DB::table('promotions')
            ->select('promotions.*', 'stores.name', 'stores.address', 'stores.slug')
            ->leftJoin("stores","stores.id","=","promotions.promotion_store_id")
            ->where('promotions.promotion_status', 1)
            ->where('promotions.id', $typeId)
            ->get()
            ->toArray();
        }
        if(count($promotionDetail)){
            $promotionInfo  = $promotionDetail[0];
            $proSendMembers = $promotionInfo->promotion_send_members;
            $phoneInfo      = $this->getMemberPromotion($proSendMembers, $storeId);
            $via = ['sms','mms'];
            $via_email = ['email'];
            /*DB::table('cronjob_send')->where('id',  $firstCronjob->id)->update(array(
                'cronjob_status'=>1
            ));*/
            Notification::send($phoneInfo, new SendPromotion($via, $storeId, $promotionInfo));
            Notification::send($phoneInfo, new SendPromotion($via_email, $storeId, $promotionInfo));
            Activity::log('Send checkin sms/mms');
            DB::table('cronjob_send')->where('id',  $firstCronjob->id)->update(array(
                'cronjob_status'=>2
            ));
            $result = array(
                'success'   => true,
                'messenger' => 'Send success!'
            );
        } else {
            $result = array(
                'success'   => true,
                'messenger' => 'Cannot find promotion!'
            );
        }
        return $result;
    }
    public function birthdayCronjob($firstCronjob = null) {
        set_time_limit(0);
        $result = array();
        $typeId     = $firstCronjob->cronjob_type_id;
        $storeId    = $firstCronjob->cronjob_store_id;
        $birthdayInfo = DB::table('birthdays')
        ->select('birthdays.*', 'stores.name', 'stores.address', 'stores.slug')
        ->leftJoin("stores","stores.id","=","birthdays.birthday_store_id")
        ->where('birthdays.birthday_status', 1)
        ->where('birthdays.birthday_store_id', $storeId)
        ->get()
        ->toArray();
        if(count($birthdayInfo)){
            $memberDetail = DB::table('members')
                ->select('members.*', 'stores.name', 'stores.address', 'stores.slug')
                ->leftJoin("stores","stores.id","=","members.member_store_id")
                ->where('members.member_status', 1)
                ->where('members.id', $typeId)
                ->get()
                ->toArray();
            $phoneInfo = $memberDetail[0];
            $via = ['sms'];
            $via_email = ['email'];
            // change status 
            DB::table('cronjob_send')->where('id',  $firstCronjob->id)->update(array(
                'cronjob_status'=>1
            ));
            Notification::send($phoneInfo, new SendBirthday($via, $storeId, $birthdayInfo[0]));
            Notification::send($phoneInfo, new SendBirthday($via_email, $storeId, $birthdayInfo[0]));
            Activity::log('Send checkin sms/email');
            DB::table('cronjob_send')->where('id',  $firstCronjob->id)->update(array(
                'cronjob_status'=>2
            ));
            $result = array(
                'success'   => true,
                'messenger' => 'Send success!'
            );
        } else {
            $result = array(
                'success'   => true,
                'messenger' => 'Cannot find promotion!'
            );
        }
        return $result;
    }
    public function atRickCronjob($firstCronjob = null) {
        set_time_limit(0);
        $result = array();
        $typeId     = $firstCronjob->cronjob_type_id;
        $storeId    = $firstCronjob->cronjob_store_id;
        $proAtRiskInfo = DB::table('promotion_at_risk')
        ->select('promotion_at_risk.*', 'stores.name', 'stores.address', 'stores.slug')
        ->leftJoin("stores","stores.id","=","promotion_at_risk.promotion_store_id")
        ->where('promotion_at_risk.promotion_status', 1)
        ->where('promotion_at_risk.promotion_store_id', $storeId)
        ->get()
        ->toArray();
        if(count($proAtRiskInfo)){
            $memberDetail = DB::table('members')
                ->select('members.*', 'stores.name', 'stores.address', 'stores.slug')
                ->leftJoin("stores","stores.id","=","members.member_store_id")
                ->where('members.member_status', 1)
                ->where('members.id', $typeId)
                ->get()
                ->toArray();
            $phoneInfo = $memberDetail[0];
            $via = ['sms'];
            // change status 
            DB::table('cronjob_send')->where('id',  $firstCronjob->id)->update(array(
                'cronjob_status'=>1
            ));
            Notification::send($phoneInfo, new SendAtRisk($via, $storeId, $proAtRiskInfo[0]));
            Activity::log('Send checkin sms/email');
            DB::table('cronjob_send')->where('id',  $firstCronjob->id)->update(array(
                'cronjob_status'=>2
            ));
            $result = array(
                'success'   => true,
                'messenger' => 'Send success!'
            );
        } else {
            $result = array(
                'success'   => true,
                'messenger' => 'Cannot find promotion!'
            );
        }
        return $result;
    }
    public function getMemberPromotion($proSendMembers, $storeId){
        $listMembers = explode(",",rtrim($proSendMembers, ", "));
        if(count($listMembers) == 1){
            $listMembers = $listMembers[0];
            $memberList = DB::table('members')
                ->select('members.*', 'stores.name', 'stores.address', 'stores.slug')
                ->leftJoin("stores","stores.id","=","members.member_store_id")
                ->where('members.member_status', 1)
                ->where('members.member_store_id', $storeId)
                ->where('members.member_level', $listMembers)
                ->get()
                ->toArray();
        } else {
            // get list members
            $memberList = DB::table('members')
                ->select('members.*', 'stores.name', 'stores.address', 'stores.slug')
                ->leftJoin("stores","stores.id","=","members.member_store_id")
                ->where('members.member_status', 1)
                ->where('members.member_store_id', $storeId)
                ->whereIn('members.member_level', $listMembers)
                ->get()
                ->toArray();
        }
        return $memberList;
    }
    public function getMessageByUser($type, $storeId, $promo = false){
        $this->inviteMessageRepo->pushCriteria(new ByStoreIdCriteria($storeId));
        if($promo) {
            $this->inviteMessageRepo->pushCriteria(new InviteMessageByTypePromoCriteria(3));
        } else {
            $this->inviteMessageRepo->pushCriteria(new InviteMessageByTypeCriteria($type));
        }
        return $this->inviteMessageRepo->first();
    }
    public function getMessageSMSByUser($type, $storeId, $promo = false){
        $this->inviteMessageRepo->pushCriteria(new ByStoreIdCriteria($storeId));
        $this->inviteMessageRepo->pushCriteria(new InviteMessageByTypePromoCriteria(3));
        return $this->inviteMessageRepo->first();
    }
    public function getMessageMMSByUser($type, $storeId, $promo = false){
        $this->inviteMessageRepo->pushCriteria(new ByStoreIdCriteria($storeId));
        $this->inviteMessageRepo->pushCriteria(new InviteMessageByTypePromoCriteria(3));
        return $this->inviteMessageRepo->first();
    }
    public function getMessageEmailByUser($type, $storeId, $promo = false){
        $this->inviteMessageRepo->pushCriteria(new ByStoreIdCriteria($storeId));
        $this->inviteMessageRepo->pushCriteria(new InviteMessageByTypeBirthdayCriteria(4));
        return $this->inviteMessageRepo->first();
    }
    public function createCronjob($input = null){
        $result = array();
        $cronjob = Cronjob::create($input);
        return true;
    }
    public function updateCronjob($input = null){
        $result = array();
        $cronjob_type_id    = $input['cronjob_type_id'];
        $cronjob_store_id   = $input['cronjob_store_id'];
        $cronjob_send_at    = $input['cronjob_send_at'];
        DB::table('cronjob_send')->where('cronjob_type_id',  $cronjob_type_id)
                                ->where('cronjob_store_id',  $cronjob_store_id)
                                ->where('cronjob_status',  0)
                                ->update(array(
                                    'cronjob_send_at'=>$cronjob_send_at
                                ));
        return true;
    }
    public function deleteCronjob($input = null){
        $result = array();
        $cronjob_type_id    = $input['cronjob_type_id'];
        $cronjob_store_id   = $input['cronjob_store_id'];
        DB::table('cronjob_send')->where('cronjob_type_id',  $cronjob_type_id)
                                ->where('cronjob_store_id',  $cronjob_store_id)
                                ->where('cronjob_status',  0)
                                ->update(array(
                                    'cronjob_status'=>-1
                                ));
        return true;
    }
    public function getConfigCheckin($storeId, $config_checkin_name) {
        $result = DB::table('config_checkin')->where('status', 1)
                                            ->where('store_id', $storeId)
                                            ->where('config_checkin_name', $config_checkin_name)
                                            ->get();
        return $result[0]->config_checkin_value;
    }

    /** Birthday cron job */
    // public function cronjobBirthdayCheck($input = null){
    //     $result = array();
    //     $current_time   = time();
    //     $time = date('Y-m-d', $current_time);
    //     $listMembers = DB::table('members')
    //                     ->select('members.*','birthdays.birthday_message', 'birthdays.birthday_allow')
    //                     ->join("store_users","members.member_store_id","=","store_users.store_id")
    //                     ->join("users","store_users.user_id","=","users.id")
    //                     ->leftJoin("birthdays","birthdays.birthday_store_id","=","store_users.store_id")
    //                     ->where('users.status',  1)
    //                     ->where('members.member_status',  1)
    //                     ->where('birthdays.birthday_status',  1)
    //                     ->where('members.member_birthday','<>','""')
    //                     ->whereNotNull('members.member_birthday')
    //                     ->orderBy('members.member_birthday', 'ASC')
    //                     ->get()
    //                     ->toArray();
    //     $listMembers   =  json_decode(json_encode($listMembers), True);
    //     $i = 0;
    //     foreach ($listMembers as $key => $member){
    //         $storeId    = $member['member_store_id'];
    //         $memberId   = $member['id'];
    //         $cronjob_type = 'birthday';
    //         $current_t   = time();
    //         if(!$this->checkExistCronjob($storeId, $memberId, $cronjob_type)){
    //             if($member['member_birthday']){
    //                 $time_birthday = strtotime($member['member_birthday']);
    //                 $date   = date('d', $time_birthday); // extracts day of month
    //                 $month  = date('m', $time_birthday);
    //                 $currentYear = date("Y");
    //                 $date = $currentYear.'-'.$month.'-'.$date.' 00:00:00';
    //                 $current_time = strtotime($date);
    //                 if($current_time >= $current_t){
    //                     $timeToSend = $current_time - ($member['birthday_allow']*86400);
    //                     $dateToSend = date('Y-m-d h:i:s', $timeToSend);
    //                     // add cron job
    //                     $input = array(
    //                         'cronjob_send_at'   => $dateToSend,
    //                         'cronjob_store_id'  => $storeId,
    //                         'cronjob_type_id'   => $memberId,
    //                         'cronjob_type'      => $cronjob_type
    //                     );
    //                     $this->createCronjob($input);
    //                     $i++;
    //                 }
    //             }
    //         }
    //     }
    //     $result = array(
    //         'success'   => true,
    //         'messenger' => 'Cronjob birthday created ('.$i.')',
    //     );
    //     return $result;
    // }

    //edit cronjob
    public function cronjobBirthdayCheck($input = null){
        $result = array();
        $current_time   = time();
        $listMembers = DB::table('members')
                        ->select('members.*','birthdays.birthday_message', 'birthdays.birthday_allow')
                        ->join("store_users","members.member_store_id","=","store_users.store_id")
                        ->join("users","store_users.user_id","=","users.id")
                        ->leftJoin("birthdays","birthdays.birthday_store_id","=","store_users.store_id")
                        ->where('users.status',  1)
                        ->where('members.member_status',  1)
                        ->where('birthdays.birthday_status',  1)
                        ->where('members.member_month_birth','<>','0')
                        ->whereNotNull('members.member_month_birth')
                        ->orderBy('members.member_date_birth', 'ASC')
                        ->get()
                        ->toArray();
        $listMembers   =  json_decode(json_encode($listMembers), True);
        $i = 0;
        foreach ($listMembers as $key => $member){
            $storeId    = $member['member_store_id'];
            $memberId   = $member['id'];
            $cronjob_type = 'birthday';
            $current_t   = time();
            if(!$this->checkExistCronjob($storeId, $memberId, $cronjob_type)){
                if($member['member_month_birth']&&$member['member_date_birth']){
                    $date   = $member['member_date_birth'];
                    $month  = $member['member_month_birth'];
                    $currentYear = date("Y");
                    $date = $currentYear.'-'.$month.'-'.$date.' 00:00:00';
                    $current_time = strtotime($date);
                    if($current_time >= $current_t){
                        $timeToSend = $current_time - ($member['birthday_allow']*86400);
                        $dateToSend = date('Y-m-d H:i:s', $timeToSend);
                        // add cron job
                        $input = array(
                            'cronjob_send_at'   => $dateToSend,
                            'cronjob_store_id'  => $storeId,
                            'cronjob_type_id'   => $memberId,
                            'cronjob_type'      => $cronjob_type
                        );
                        $this->createCronjob($input);
                        $i++;
                    }
                }
            }
        }
        $result = array(
            'success'   => true,
            'messenger' => 'Cronjob birthday created ('.$i.')',
        );
        return $result;
    }

    public function getConfigBirthday($storeId) {
        $result = DB::table('birthdays')->where('birthday_store_id', $storeId)
                                        ->where('birthday_status', 1)
                                        ->get();
        return $result[0];
    }
    public function checkExistCronjob($storeId, $cronjob_type_id, $cronjob_type) {
        $currentYear = date("Y");
        $result = DB::table('cronjob_send')->where([
            ['cronjob_store_id',    '=', $storeId],
            ['cronjob_type_id',     '=', $cronjob_type_id],
            ['cronjob_type',        '=', $cronjob_type]
            ])
            ->get();
        if(count($result)){
            $sendDate = strtotime($result[0]->cronjob_send_at);
            $sendYear   = date('Y', $sendDate); 
            if($currentYear != $sendYear){
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
        
    }
    /** END Birthday cron job */
    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }

}
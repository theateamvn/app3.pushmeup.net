<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;

use App\Models\Store;
use App\Models\MemberCheckout;
use App\Models\Member;
use App\Repositories\MemberCheckoutRepository;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Auth;
use App\Events\MessagePosted;
use Faker\Factory;
use Carbon\Carbon;
use DB;
use Request;

class MemberCheckoutService
{
    private $memberCheckoutRepository;
    private $memberCheckoutService;
    /**
     * UserService constructor.
     * @param $checkInRepository
     */
    public function __construct(MemberCheckoutRepository $memberCheckoutRepository, MemberCheckoutService $memberCheckoutService)
    {
        $this->MemberCheckoutRepository = $memberCheckoutRepository;
        $this->memberCheckoutService = $memberCheckoutService;
    }
    
}
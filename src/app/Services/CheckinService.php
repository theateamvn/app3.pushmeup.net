<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Http\Requests\API\CreateCheckInAPIRequest;
use App\Models\Store;
use App\Models\CheckIn;
use App\Models\Member;
use App\Services\MemberService;
use App\Repositories\CheckInRepository;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Auth;
use App\Events\MessagePosted;
use Faker\Factory;
use Carbon\Carbon;
use DB;
use Request;

class CheckInService
{
    private $checkInRepository;
    private $faker;
    private $membersService;
    /**
     * UserService constructor.
     * @param $checkInRepository
     */
    public function __construct(CheckInRepository $checkInRepository, MemberService $membersService, MemberRepository $memberRepository)
    {
        $this->checkInRepository = $checkInRepository;
        $this->memberRepository = $memberRepository;
        $this->membersService = $membersService;
        $this->faker = Factory::create();
    }
    public function memberCheck($input = null){
        $result = array();
        $register = true;
        $checkinMemberId = 0;
        $time_zone      = $input['time_zone'];
        $store_id       = $input['member_store_id'];
        $member_phone   = $input['member_phone'];
        $time_after_checkin = $this->getConfigCheckin($store_id, 'time_after_checkin');
        $member = DB::table('members')
                        ->selectRaw('*')
                        ->where('member_store_id',  $store_id)
                        ->where('member_phone',     $member_phone)
                        ->where('member_status', '<>', -1)
                        ->get()
                        ->toArray();
        if(count($member)) {
            // lây dữ liệu member 
            $date_birth     = $member[0]->member_date_birth;
            $month_birth    = $member[0]->member_month_birth;
            $email          = $member[0]->member_email;
            $member_id      = $member[0]->id;
            $register       = false;
            if($email == null){
                $register       = true;
            }
            // tạo lịch sử checkin
            // kiểm tra checkin 
            //$current_time   = time();
           // echo Carbon::now($time_zone)->format('Y-m-d H:i:s');die;
            if($time_zone) {
                $current_time   = strtotime(Carbon::now($time_zone)->format('Y-m-d H:i:s'));
            } else {
                $current_time   = strtotime(Carbon::now()->format('Y-m-d H:i:s'));
            }
            //echo date('Y-m-d H:i:s',   $current_time);
           // echo Carbon::now($time_zone)->format('Y-m-d H:i:s');die;
            $next_time      = $current_time - ($time_after_checkin*60);
            //$next_time      = $current_time;
            $from   = date('Y-m-d H:i:s', $next_time);
            $to     = date('Y-m-d H:i:s',   $current_time);
            //update user status
            DB::table('members')
            ->where('id', $member_id)
            ->update(['member_status' => 1]);
            $checkinCheck = \DB::table('members_checkin')->where([
                                ['checkin_status', '=', 1],
                                ['member_id', '=', $member_id]])
                                ->whereBetween('checkin_at', array($from, $to))
                                ->count();
            if($checkinCheck == 0) {
                $checkin = CheckIn::create([
                    'store_id'          => $store_id,
                    'member_id'         => $member_id,
                    'checkin_at'        => date('Y-m-d H:i:s',   $current_time)
                ]);
                $checkinMemberId = $checkin->id;
            }
            $totalCheckIn = \DB::table('members_checkin')->where([['member_id', '=', $member_id]])->count();
            $totalCheckInPrice = \DB::table('members_checkout')->where('member_id', $member_id)->sum("price_services");
            $total_point = $totalCheckIn + $totalCheckInPrice;
            // update level members
            $this->membersService->updateLevel($member_id, $total_point, $store_id);
            $result = array(
                'register'              => $register,
                'member'                => $member[0],
                'totalVisit'            => $total_point,
                'checkinMemberId'       => $checkinMemberId
            );
        } else {
            // tạo mới member
            $member = Member::create([
                'member_store_id'       => $store_id,
                'member_phone'          => $member_phone,
                'member_date_birth'      => 0,
                'member_month_birth'     => 0
            ]);
            $member_id = $member['id'];
            // tạo lịch sử checkin
            // kiểm tra checkin 
            //$current_time   = time();
            if($time_zone) {
                $current_time   = strtotime(Carbon::now($time_zone)->format('Y-m-d H:i:s'));
            } else {
                $current_time   = strtotime(Carbon::now()->format('Y-m-d H:i:s'));
            }
            $next_time      = $current_time - ($time_after_checkin*60);
            $from   = date('Y-m-d H:i:s', $next_time);
            $to     = date('Y-m-d H:i:s', $current_time);
            $checkinCheck = \DB::table('members_checkin')->where([
                                ['checkin_status', '=', 1],
                                ['member_id', '=', $member_id]])
                                ->whereBetween('checkin_at', array($from, $to))
                                ->count();
            if($checkinCheck == 0) {
                $checkin = CheckIn::create([
                    'store_id'          => $store_id,
                    'member_id'         => $member_id,
                    'checkin_at'        => date('Y-m-d H:i:s',   $current_time)
                ]);
                $checkinMemberId = $checkin->id;
            }
            $totalCheckIn = \DB::table('members_checkin')->where([['member_id', '=', $member_id]])->count();
            $result = array(
                'register'              => true,
                'member'                => $member,
                'totalVisit'            => $totalCheckIn,
                'checkinMemberId'       => $checkinMemberId
            );
        }
        //broadcast(new MessagePosted($member['id']));//->toOthers();

        return $result;
    }
    public function memberCheckInStep2($input = null){
        $result = array();
        $store_id       = $input['member_store_id'];
        $member_id      = $input['id'];
        $email          = isset($input['member_email']) ? $input['member_email'] : '';
        $checkExistEmail = $this->checkExistEmailMember($email, $store_id, $member_id);
        if(!$checkExistEmail) {
            $input_ = array( 
                        'member_fullname'   => $input['member_fullname'],
                        'member_email'      => $email,
                        'member_date_birth' => isset($input['member_date_birth']) ?  $input['member_date_birth'] : 0,
                        'member_month_birth'=> isset($input['member_month_birth']) ?  $input['member_month_birth'] : 0
                            );
            $result_u = $this->memberRepository->update($input_, $input['id']);
            $result = array(
                'success'      => true,
                'message'      => 'save successfully',
                'member'       => $result_u
            );
        } else {
            $result = array(
                'success'      => false,
                'message'      => 'Email is exist!',
                'member'       => $input
            );
        }
        return $result;
    }

    //checkin Offline
    public function memberCheckInOffline($input){
        $store_id = $input['member_store_id'];
        $member_fullname = $input['member_fullname'];
        $member_created_at = $input['member_created_at'];
        $member_phone = $input['member_phone'];
        $member_email = $input['member_email'];
        $member_date_birth  = isset($input['member_date_birth']) ? $input['member_date_birth']: 0;
        $member_month_birth = isset($input['member_month_birth']) ? $input['member_month_birth']:0;
        $members = DB::table('members')
                        ->selectRaw('*')
                        ->where('member_store_id',  $store_id)
                        ->where('member_phone',     $member_phone)
                        ->where('member_status', 1)
                        ->get()
                        ->toArray();
        $member= count($members) ? $members[0] : '';
        if( count($members) < 1 ){
            $member = Member::create([
                'member_store_id'   => $store_id,
                'member_phone'      => $member_phone,
                'member_fullname'   => $member_fullname,
                'member_email'      => $member_email,
                'member_date_birth' => $member_date_birth,
                'member_month_birth'=> $member_month_birth,
                'member_status'     => 1,
                'member_created_at' => $member_created_at
            ]);
            
        }
        $checkin = CheckIn::create([
            'store_id'          => $store_id,
            'member_id'         => $member->id,
            'checkin_at'        => $member_created_at,
            'checkin_status'    => 2
        ]);
        return $member;
    }

    //All checkin by userid
    public function checkinbyUser($id)
    {
        //SELECT members_checkin.*,members_checkout.price_point FROM `members_checkin` LEFT JOIN members_checkout ON members_checkin.id = members_checkout.checkin_id 
        //$checkins=DB::table('members_checkin')->where('member_id','=',$id)->get()->toArray();
        $checkins=DB::table('members_checkin')
        ->leftJoin('members_checkout','members_checkin.id', '=','members_checkout.checkin_id')
        ->where('members_checkin.member_id','=',$id)
        ->get()->toArray();
        $checkins   =  json_decode(json_encode($checkins), True);
        return $checkins;
    }

    public function getCheckinCurrent($input)
    {
        $store_id = $this->getCurrentUser()->store()->id;
        //$from = date('Y-m-d' . ' 00:00:00', time());
        //$to = date('Y-m-d' . ' 23:59:59', time());
        $result = array();
            $checkinList = DB::table('members_checkin')
                    ->select('members_checkin.id as checkin_id', 'members_checkin.checkin_at','members_checkin.checkin_status', 'members.*')
                    ->leftJoin("members","members.id","=","members_checkin.member_id")
                    ->where('members_checkin.store_id', $store_id)
                    ->whereBetween('checkin_at', array($input['from'], $input['to']))
                    ->orderBy('members_checkin.id', 'DESC')
                    ->paginate()
                    ->toArray();
        $checkinList   =  json_decode(json_encode($checkinList), True);
        foreach ($checkinList['data'] as $key => $checkin){
            $total_checkin = DB::table('members_checkin')->where([['member_id', '=', $checkin['id']]])
            ->count();
            $checkinList['data'][$key]['total_checkin'] = $total_checkin;
        }            
        return $checkinList;
    }
    public function getCheckinStatistic()
    {
        $store_id   = $this->getCurrentUser()->store()->id;
        $time_zone  = Auth::user()->time_zone;
        if($time_zone) {
            $current_time   = strtotime(Carbon::now($time_zone)->format('Y-m-d H:i:s'));
        } else {
            $current_time   = strtotime(Carbon::now()->format('Y-m-d H:i:s'));
        }
        $from = date('Y-m-d' . ' 00:00:00', $current_time);
        $to = date('Y-m-d' . ' 23:59:59', $current_time);
        $first_date = date('Y-m-d'. ' 00:00:00',strtotime('first day of this month'));
        $last_date = date('Y-m-d'. ' 23:59:59',strtotime('last day of this month'));
        $totalMember = DB::table('members')->where([
                            ['member_status', '=', 1],
                            ['member_store_id', '=', $store_id]
                        ])->count();
        $totalCheckin = DB::table('members_checkin')->where([
                            ['store_id', '=', $store_id]
                        ])->count();
        $totalCheckinDate = DB::table('members_checkin')->where([
                            ['store_id', '=', $store_id]
                        ])
                        ->whereBetween('checkin_at', array($from, $to))
                        ->count();
        $totalCheckinMonth = DB::table('members_checkin')->where([
                            ['store_id', '=', $store_id]
                            ])
                            ->whereBetween('checkin_at', array($first_date, $last_date))
                            ->count();
        $result = array(
            'totalMember'       => $totalMember,
            'totalCheckin'      => $totalCheckin,
            'totalCheckinMonth' => $totalCheckinMonth,
            'totalCheckinDate'  => $totalCheckinDate
        );
        return $result;
    }

    public function checkExistUser($username = ""){
        if(!$username){
            return 1;
        }
        $username = DB::table('users')->where([
                                            ['username', '=', $username]
                                            ])->count();
        if ($username != 0) {
            return 1;
        } else {
            return 0;
        }
    }
    public function checkExistEmailMember($email = "", $store_id, $id){
        if(!$email){
            return 0;
        }
        $email = DB::table('members')->where([
                                            ['member_email', '=', $email],
                                            ['member_store_id', '=', $store_id],
                                            ['id', '<>', $id],
                                            ['member_status', 1]
                                            ])->count();
        if ($email != 0) {
            return 1;
        } else {
            return 0;
        }
    }
    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
    protected function getConfigCheckin($storeId, $config_checkin_name) {
        $result = DB::table('config_checkin')->where('status', 1)
                                            ->where('store_id', $storeId)
                                            ->where('config_checkin_name', $config_checkin_name)
                                            ->get();
        return $result[0]->config_checkin_value;
    }

}
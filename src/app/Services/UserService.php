<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Http\Requests\API\CreateUserAPIRequest;
use App\Models\Store;
use App\Models\ConfigCheckin;
use App\Models\User;
use App\Repositories\UserRepository;
use Faker\Factory;
use Request;
use DB;

class UserService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * UserService constructor.
     * @param $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->faker = Factory::create();
    }

    public function createNewUserWithoutPayment($input){

        $user = User::create($input);
        $user->password = bcrypt($input['password']);
        $user->save();

        $store = $this->createStoreBasic($input);
        $this->createCheckinBasic($store['id']);
        $user->storeUsers()->attach($store);

        return $user;
    }

    public function createStore($input = null){
        if($input){
            $store = Store::create($input);
        }else{
            $store = Store::create([
                'name' => $this->faker->name,
                'address' => $this->faker->address,
                'url' => $this->faker->url
            ]);
        }
        $store->save();
        return $store;
    }
    public function createStoreBasic($input = null){

        $store = Store::create([
            'phone' => $input['phone'],
            'name' => $this->faker->name,
            'address' => $this->faker->address,
            'url' => $this->faker->url
        ]);
        $store->save();
        return $store;
    }
    public function createCheckinBasic($storeId){

        $configCheckin = ConfigCheckin::create([
            'store_id' => $storeId,
            'config_checkin_value' => config('pushmeup.setting_checkin.member_regular'),
            'config_checkin_name' => 'member_regular'
        ]);
        $configCheckin->save();
        $configCheckin_2 = ConfigCheckin::create([
            'store_id' => $storeId,
            'config_checkin_value' => config('pushmeup.setting_checkin.member_vip'),
            'config_checkin_name' => 'member_vip'
        ]);
        $configCheckin_2->save();
        $configCheckin_3 = ConfigCheckin::create([
            'store_id' => $storeId,
            'config_checkin_value' => config('pushmeup.setting_checkin.time_after_checkin'),
            'config_checkin_name' => 'time_after_checkin'
        ]);
        $configCheckin_3->save();
        return $configCheckin;
    }

    public function checkExistUser($username = ""){
        if(!$username){
            return 1;
        }
        $username = DB::table('users')->where([
                                            ['username', '=', $username]
                                            ])->count();
        if ($username != 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getUserCheckin($userCheckin)
    {
       $user = DB::table('user_checkin')
       ->select('user_checkin.id','stores.slug','users.username','users.phone','user_checkin.secret_key')
       ->leftJoin('users','users.id','=','user_checkin.user_id')
       ->leftJoin('store_users','store_users.user_id','=','user_checkin.user_id')
       ->leftJoin('stores','stores.id','=','store_users.store_id')
       ->where('users.username','=',$userCheckin['username'])
       ->where('users.phone','=',$userCheckin['phone'])
       ->where('user_checkin.secret_key','=',$userCheckin['secret_key'])
       ->where('user_checkin.status','=',1)->get()->toArray();
       return $user;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 12/2/2016
 * Time: 8:59 AM
 */

namespace App\Services;


use App\Exceptions\DataRequiredException;
use Auth;

class StoreService
{
    /**
     * @return \App\Models\Store|null
     * @throws DataRequiredException
     */
    public function getCurrentStore(){
        $user = $this->getCurrentUser();
        $store = $user->store();
        if (!isset($store)) {
            throw new DataRequiredException('Store');
        }
        return $store;
    }

    /**
     * @return \App\Models\User|null
     * @throws DataRequiredException
     */
    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
}
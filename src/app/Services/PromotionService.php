<?php
/**
 * Created by TAT.
 * User: Khanh
 * Date: 25/10/2019
 * Time: 17:17 PM
 */

namespace App\Services;


use App\Models\Promotion;
use App\Repositories\PromotionRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Faker\Factory;
use Request;
use DB;

class PromotionService
{
    /**
     * @var PromotionRepository
     */
    private $promotionRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct(PromotionRepository $proRepo)
    {
        $this->promotionRepository = $proRepo;
        $this->faker = Factory::create();
    }

    public function create($input){
        $promotion =Promotion::create($input);
        $promotion->save();
        return $promotion;
    }

    public function getpromotionList()
    {
        $result=array();
        $promotion= DB::table('promotions')->get()->toArray();
        $promotion=json_decode(json_encode($promotion),TRUE);
        foreach ($promotion as $value) {
            array_push($result['list'],explode(',',$value->promotion_send_members));
            array_push($result['promotion'],$promotion);
        }
        return $result;
    }

    public function promotionList($store_id,$input)
    {
        $result=array();
        $promotion_list=array();
        $whereClause="promotions.promotion_store_id = ".$store_id." and promotions.promotion_status <> -1";
        $whereClause = $input['search']!='' ? $whereClause." and promotions.promotion_status = ".explode(':',$input['search'],2)[1] : $whereClause;
        $promotions=DB::table('promotions')
        ->select('promotions.*', 'cronjob_send.cronjob_store_id', 'cronjob_send.cronjob_store_id', 'cronjob_send.cronjob_type', 'cronjob_send.cronjob_type_id', 'cronjob_send.cronjob_status')
        ->leftJoin('cronjob_send', function ($join)
        {
            //$join->on('cronjob_send.cronjob_store_id', '=','promotions.promotion_store_id');
            $join->on('cronjob_send.cronjob_type_id', '=','promotions.id');
            $join->where('cronjob_send.cronjob_type', '=','promotion');
        })
        ->whereRaw($whereClause)
        ->orderBy('promotions.id', 'DESC')
        ->paginate();
        $result['total'] = $promotions->total();
        $result['last_page']=$promotions->lastPage();
        $result['current_page']=$promotions->currentPage();
        $result['per_page']=$promotions->perPage();
        $result['data']=array();
        $promotions=$promotions->toArray();
        $promotions=json_decode(json_encode($promotions),TRUE);
        foreach ($promotions['data'] as $key => $value) {
            $result['data'][$key] = $value;
           // var_dump($value);die;
            $sendList = $value['promotion_send_members'];
            $memberList = explode(",", $sendList);
            $result['data'][$key]['memberList'] = $memberList;
        }
        
        return $result;
    }

    public function getBannerPromotion($slug){
        $today=date("Y-m-d");
        $store=DB::table('stores')->where('slug','=',$slug)->get()->toArray();
        $store=json_decode(json_encode($store),TRUE);
        $result=array();
        $promotion=DB::table('promotions')
        ->where('promotion_start','<=',$today)
        ->where('promotion_end','>=',$today)
        ->where('promotion_store_id','=',$store[0]['id'])
        ->where('promotion_status','=',1)
        ->orderBy('promotions.id', 'DESC')
        ->get()->toArray();
        $promotion=json_decode(json_encode($promotion),TRUE);
        return $promotion;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 2/20/2017
 * Time: 10:28 AM
 */

namespace App\Services\Sms\Esms\Exceptions;


class EsmsException extends \Exception
{
    public $codeResult;
    public $errorMessage;

    /**
     * EsmsException constructor.
     * @param $codeResult
     * @param $errorMessage
     */
    public function __construct($codeResult, $errorMessage)
    {
        $this->codeResult = $codeResult;
        $this->errorMessage = $errorMessage;

        $this->message = $errorMessage;
    }


}
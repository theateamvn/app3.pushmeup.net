<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 12/2/2016
 * Time: 8:45 AM
 */

namespace App\Services;


use App\DTOs\OverviewInformation;
use App\Models\ReviewProvider;
use Stores;
use Carbon\Carbon;
use Auth;
class StatisticService
{

    /**
     * @var ReviewService
     */
    private $reviewService;
    /**
     * @var InviteService
     */
    private $inviteService;
    /** @var  BlastSmsService */
    private $blastSmsService;

    /**
     * StatisticService constructor.
     * @param ReviewService $reviewService
     * @param InviteService $inviteService
     * @param BlastSmsService $blastSmsService
     */
    public function __construct(ReviewService $reviewService, InviteService $inviteService, BlastSmsService $blastSmsService)
    {
        $this->reviewService = $reviewService;
        $this->inviteService = $inviteService;
        $this->blastSmsService = $blastSmsService;
    }

    /**
     * @param $storeId
     * @param string $startDate
     * @param string $endDate
     * @return OverviewInformation
     */
    public function getOverviewInfo($storeId, $startDate = "1900-01-01", $endDate = 'now')
    {
        $overviewInformation = new OverviewInformation();
        $overviewInformation->overallRating = $this->reviewService->getOverallRating($storeId);
        $overviewInformation->totalReview = $this->reviewService->getReviewCount($storeId);
        $overviewInformation->reviewByGoogle = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_GOOGLE);
        $overviewInformation->reviewByFacebook = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_FACEBOOK);
        $overviewInformation->reviewByYelp = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_YELP);
        $overviewInformation->reviewByTripadvisor = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_TRIPADVISOR);
        $overviewInformation->reviewByAgoda = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_AGODA);
        $overviewInformation->reviewByBooking = $this->reviewService->getReviewCountByProvider($storeId, ReviewProvider::PROVIDER_BOOKING);
        $overviewInformation->ratingByGoogle = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_GOOGLE);
        $overviewInformation->ratingByFacebook = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_FACEBOOK);
        $overviewInformation->ratingByYelp = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_YELP);
        $overviewInformation->ratingByTripadvisor = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_TRIPADVISOR);
        $overviewInformation->ratingByAgoda = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_AGODA);
        $overviewInformation->ratingByBooking = $this->reviewService->getRatingByProvider($storeId, ReviewProvider::PROVIDER_BOOKING);
        $overviewInformation->inviteClickedThroughRate = $this->inviteService->getInviteClickedThroughRate($storeId);
        $overviewInformation->inviteSentThisMonth = $this->inviteService->getCustomerCreatedCountInThisMonth($storeId);
        $overviewInformation->inviteSentLastMonth = $this->inviteService->getCustomerCreatedCountInLastMonth($storeId);
        $overviewInformation->customerInformation = $this->inviteService->getCustomerInformation($startDate, $endDate);
        return $overviewInformation;
    }

    public function getOverviewInfoByCurrentUser()
    {
        $store = Stores::getCurrentStore();
        return $this->getOverviewInfo($store->id);
    }

    public function getUsersInformation()
    {
        $total = \DB::table('users')->count();
        $activatedUser = \DB::table('users')->where('status', 1)->count();
        $managers = \DB::table('users')->where('role', 1)->count();
        $agents = \DB::table('users')->where('role', 2)->count();

        return [
            'total' => $total,
            'activated' => $activatedUser,
            'pending' => $total - $activatedUser,
            'managers' => $managers,
            'agents' => $agents,
            'clients' => $total - $managers - $agents
        ];
    }

    public function getCustomerStatistic($startDate, $endDate = 'now')
    {
        $store = Stores::getCurrentStore();
        return $this->inviteService->statisticInviteByDateRange($store->id, $startDate, $endDate);
    }

    public function getMessageStatistic($startDate)
    {
        $store = Stores::getCurrentStore();
        $endDate = Carbon::parse($startDate)->lastOfMonth()->toDateString();
        $startDate = Carbon::parse($startDate)->startOfMonth()->toDateString();
        $customerInformation = $this->inviteService->getCustomerInformation($startDate,$endDate);
        $blastSmsStatistic = $this->blastSmsService->statisticMessageByDateRange($store->id, $startDate, $endDate);
        return [
            'count_sms' => $customerInformation->smsSentCount + $blastSmsStatistic['count_sms'],
            'count_mms' => $customerInformation->mmsSentCount + $blastSmsStatistic['count_mms'],
            'total' => $customerInformation->newCreatedCount + $blastSmsStatistic['total']
        ];
    }

    public function getReviewsByStar($star, $endStar)
    {
        $store = Stores::getCurrentStore();
        return $this->getOverviewInfoByStar($store->id, $star, $endStar);
    }
    public function getOverviewInfoByStar($storeId, $star, $endStar)
    {
        $overviewInformation = new OverviewInformation();
        $overviewInformation->totalReview = $this->reviewService->getReviewCountByStar($storeId, $star, $endStar);
        $overviewInformation->reviewByGoogle = $this->reviewService->getReviewCountByProviderAndStar($storeId, $star, $endStar, ReviewProvider::PROVIDER_GOOGLE);
        $overviewInformation->reviewByFacebook = $this->reviewService->getReviewCountByProviderAndStar($storeId, $star, $endStar, ReviewProvider::PROVIDER_FACEBOOK);
        $overviewInformation->reviewByYelp = $this->reviewService->getReviewCountByProviderAndStar($storeId, $star, $endStar, ReviewProvider::PROVIDER_YELP);
        $overviewInformation->reviewByTripadvisor = $this->reviewService->getReviewCountByProviderAndStar($storeId, $endStar, $star, ReviewProvider::PROVIDER_TRIPADVISOR);
        $overviewInformation->reviewByAgoda = $this->reviewService->getReviewCountByProviderAndStar($storeId, $star, $endStar, ReviewProvider::PROVIDER_AGODA);
        $overviewInformation->reviewByBooking = $this->reviewService->getReviewCountByProviderAndStar($storeId, $star, $endStar, ReviewProvider::PROVIDER_BOOKING);
        return $overviewInformation;
    }

    public function getReviewsInformation()
    {
        $total = \DB::table('reviews')->count();
        $reviewByGoogle = $this->reviewService->getReviewTotalCountByProvider(ReviewProvider::PROVIDER_GOOGLE);
        $reviewByFacebook = $this->reviewService->getReviewTotalCountByProvider(ReviewProvider::PROVIDER_FACEBOOK);
        $reviewByYelp = $this->reviewService->getReviewTotalCountByProvider(ReviewProvider::PROVIDER_YELP);
        $reviewByTripadvisor = $this->reviewService->getReviewTotalCountByProvider(ReviewProvider::PROVIDER_TRIPADVISOR);
        $reviewByAgoda = $this->reviewService->getReviewTotalCountByProvider(ReviewProvider::PROVIDER_AGODA);
        $reviewByBooking = $this->reviewService->getReviewTotalCountByProvider(ReviewProvider::PROVIDER_BOOKING);

        return [
            'total' => $total,
            'google' => $reviewByGoogle,
            'facebook' => $reviewByFacebook,
            'tripadvisor' => $reviewByTripadvisor,
            'yepl' => $reviewByYelp,
            'agoda' => $reviewByAgoda,
            'booking' => $reviewByBooking
        ];
    }
    public function getReviewLinkStatistic()
    {
        $store = Stores::getCurrentStore();
        $total = \DB::table('review_links')->where('storeId', $store->id)->count();
        $reviewByGoogle = $this->reviewService->getReviewLinkTotalCountByProvider(ReviewProvider::PROVIDER_GOOGLE);
        $reviewByFacebook = $this->reviewService->getReviewLinkTotalCountByProvider(ReviewProvider::PROVIDER_FACEBOOK);
        $reviewByYelp = $this->reviewService->getReviewLinkTotalCountByProvider(ReviewProvider::PROVIDER_YELP);
        $reviewByTripadvisor = $this->reviewService->getReviewLinkTotalCountByProvider(ReviewProvider::PROVIDER_TRIPADVISOR);
        $reviewByAgoda = $this->reviewService->getReviewLinkTotalCountByProvider(ReviewProvider::PROVIDER_AGODA);
        $reviewByBooking = $this->reviewService->getReviewLinkTotalCountByProvider(ReviewProvider::PROVIDER_BOOKING);

        return [
            'total' => $total,
            'google' => $reviewByGoogle,
            'facebook' => $reviewByFacebook,
            'tripadvisor' => $reviewByTripadvisor,
            'yepl' => $reviewByYelp,
            'agoda' => $reviewByAgoda,
            'booking' => $reviewByBooking
        ];
    }
    /* by agent role*/
    public function getAgentUsersInformation()
    {
        $userId  = Auth::user()->id;
        $total = \DB::table('users')->where('created_by', $userId)->count();
        $activatedUser = \DB::table('users')->where([['status', '=', 1],
                                                    ['created_by', '=', $userId]])->count();
        return [
            'total' => $total,
            'activated' => $activatedUser,
            'pending' => $total - $activatedUser
        ];
    }
}


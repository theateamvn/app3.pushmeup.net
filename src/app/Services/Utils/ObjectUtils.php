<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 3/13/2017
 * Time: 9:51 AM
 */

namespace App\Services\Utils;


class ObjectUtils
{
    public static function convertValueToPersist($value)
    {
        if (is_object($value) || is_array($value) || is_bool($value)) {
            return serialize($value);
        }

        return $value;
    }

    public static function restorePersistedValue($data)
    {
        $val = @unserialize($data);
        if ($val === false) {
            return $data;
        }

        return $val;
    }
}
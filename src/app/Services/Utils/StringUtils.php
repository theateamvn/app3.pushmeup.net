<?php
namespace App\Services\Utils;
/**
 * Created by PhpStorm.
 * User: Lai Vu
 * Date: 12/6/2016
 * Time: 1:15 PM
 */
class StringUtils
{
    /**
     * @param $pattern
     * @param $input
     * @param array $variable
     * @return mixed
     */
    public static function replacePlaceholder($pattern, $input, array $variable){
        return preg_replace_callback($pattern,function ($matches) use($variable){
            $key = strtolower( $matches[ 1 ] );
            return array_key_exists( $key, $variable ) ? $variable[ $key ] : $matches[ 0 ];
        },$input);
    }

    /**
     * @param $input
     * @return mixed
     */
    public static function convertToUTF8($input){
        try{
            $output = iconv("UTF-8", "ISO-8859-1//TRANSLIT ", $input);
        }catch (\Exception $ex){
            $output = utf8_decode($input);
        }
        return $output;
    }
}
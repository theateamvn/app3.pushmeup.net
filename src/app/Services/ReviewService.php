<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 24/11/2016
 * Time: 4:35 PM
 */

namespace App\Services;


use App\DTOs\FetchAllReviewResult;
use App\Exceptions\DataRequiredException;
use App\Models\ReviewProvider;
use App\Repositories\ReviewRepository;
use App\Repositories\StoreRepository;
use App\Services\Review\Drivers\Factory;
use DB;
use Illuminate\Database\QueryException;
use Log;
use Stores;

class ReviewService
{
    /**
     * @var ReviewProviderService
     */
    private $reviewProviderService;

    /**
     * @var ReviewRepository
     */
    private $reviewRepository;

    /**
     * @var StoreRepository
     */
    private $storeRepository;

    /**
     * @var Factory
     */
    private $reviewFactory;

    /**
     * ReviewService constructor.
     * @param ReviewProviderService $reviewProviderService
     * @param ReviewRepository $reviewRepository
     * @param StoreRepository $storeRepository
     * @param Factory $reviewFactory
     */
    public function __construct(ReviewProviderService $reviewProviderService,
                                ReviewRepository $reviewRepository,
                                StoreRepository $storeRepository,
                                Factory $reviewFactory)
    {
        $this->reviewProviderService = $reviewProviderService;
        $this->reviewRepository = $reviewRepository;
        $this->storeRepository = $storeRepository;
        $this->reviewFactory = $reviewFactory;
    }

    /**
     * @param $provider
     * @return \Illuminate\Support\Collection
     */
    public function getReviewInformationByUserAndProvider($provider)
    {
        $reviewProvider = $this->reviewProviderService->findProviderByCurrentUser($provider);
        $reviews = $this->fetchReviewInformationByProvider($reviewProvider);
        return $reviews;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getReviewInformationsByUser()
    {
        $store = Stores::getCurrentStore();
        return $this->getReviewInformationsByStore($store->id);
    }

    /**
     * @param $storeId
     * @return \Illuminate\Support\Collection
     * @throws DataRequiredException
     */
    public function getReviewInformationsByStore($storeId)
    {
        $reviewProviders = $this->reviewProviderService->findProvidersByStore($storeId);
        $result = collect([]);
        foreach ($reviewProviders as $reviewProvider) {
            $reviews = $this->fetchReviewInformationByProvider($reviewProvider);
            $result = $result->merge($reviews);
        }
        return $result;
    }

    /**
     * @param $reviewProvider ReviewProvider
     * @return \Illuminate\Support\Collection
     * @throws DataRequiredException
     * @internal param $provider
     */
    public function fetchReviewInformationByProvider($reviewProvider)
    {
        if (!isset($reviewProvider)
            && !isset($reviewProvider->reviewed_key)
            && !isset($reviewProvider->name)
            && !isset($reviewProvider->store_id)
        ) {
            $dataRequiredException = new DataRequiredException('Review provider', $reviewProvider);
            Log::error($dataRequiredException, $reviewProvider->toArray());
            throw $dataRequiredException;
        }
        set_time_limit(0);
        //return $this->reviewFactory->make($reviewProvider->name)->getReviews($reviewProvider->reviewed_key, $reviewProvider->store_id);
        return $this->reviewFactory->make($reviewProvider->name)->getAllReviews($reviewProvider->reviewed_key, $reviewProvider->store_id);
    }

    /**
     * @param ReviewProvider $reviewProvider
     */
    public function fetchAndSaveReviewInformationByProvider($reviewProvider)
    {
        $reviewInformation = $this->fetchReviewInformationByProvider($reviewProvider);
        foreach ($reviewInformation as $review) {
            try {
                $review->save();
            } catch (QueryException $ex) {
                Log::error("Save review Error: " . $ex->getMessage());
            }
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function fetchReviewInformationFromService()
    {
        //$stores = $this->storeRepository->all();
        $stores = DB::table('stores')
                        ->select('stores.*')
                        ->leftJoin("store_users","stores.id","=","store_users.store_id")
                        ->leftJoin("users","users.id","=","store_users.user_id")
                        ->where('users.status', 1)
                        ->where('users.role',   0)
                        ->get();
        $result = collect([]);
        foreach ($stores as $store) {
            $reviews = $this->getReviewInformationsByStore($store->id);
            $result = $result->merge($reviews);
        }
        
        return $result;
    }

    /**
     *
     */
    public function fetchAndSaveReviewInformationFromService()
    {
        $time_start = microtime(true);
        $successCount = 0;
        $reviews = $this->fetchReviewInformationFromService();
        foreach ($reviews as $review) {
            try {
                $review->save();
                $successCount++;
            } catch (QueryException $ex) {
                Log::error("Save review Error: " . $ex->getMessage());
            }
        }
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start) / 60;

        $fetchAllReviewResult = new FetchAllReviewResult();
        $fetchAllReviewResult->executionTime = $execution_time;
        $fetchAllReviewResult->totalReview = $reviews->count();
        $fetchAllReviewResult->reviewSaveSuccess = $successCount;
        $fetchAllReviewResult->reviewSaveFail = $fetchAllReviewResult->totalReview - $successCount;

        return $fetchAllReviewResult;
    }

    /**
     * @param $reviewProvider ReviewProvider
     * @return \Illuminate\Support\Collection
     * @throws DataRequiredException
     */
    public function fetchAllReviewInformationByProvider($reviewProvider)
    {
        if (!isset($reviewProvider)
            && !isset($reviewProvider->reviewed_key)
            && !isset($reviewProvider->name)
            && !isset($reviewProvider->store_id)
        ) {
            $dataRequiredException = new DataRequiredException('Review provider', $reviewProvider);
            Log::error($dataRequiredException, $reviewProvider->toArray());
            throw $dataRequiredException;
        }

        set_time_limit(0);
        Log::info($reviewProvider->name.'-'.$reviewProvider->reviewed_key.'-'.$reviewProvider->store_id);
        return $this->reviewFactory->make($reviewProvider->name)->getAllReviews($reviewProvider->reviewed_key, $reviewProvider->store_id);
    }

    /**
     * @param ReviewProvider $reviewProvider
     */
    public function fetchAndSaveAllReviewInformationByProvider($reviewProvider)
    {
        $reviews = $this->fetchAllReviewInformationByProvider($reviewProvider);
        foreach ($reviews as $review) {
            try {
                $review->save();
            } catch (QueryException $ex) {
                Log::error("Save review Error: " . $ex->getMessage());
            }
        }
    }

    public function fetchAndSaveAllReviewByCurrentUser(){
        $store = Stores::getCurrentStore();
        $reviewProviders = $store->reviewProviders;
        foreach ($reviewProviders as $provider){
            $this->fetchAndSaveAllReviewInformationByProvider($provider);
        }
    }

    /**
     * @param $storeId
     * @return double
     */
    public function getOverallRating($storeId)
    {
        $avgRating = DB::table('reviews')->where('store_id', $storeId)->avg('rating');
        return round($avgRating, 1);
    }

    /**
     * @return double
     */
    public function getOverallRatingByCurrentUser()
    {
        $store = Stores::getCurrentStore();
        return $this->getOverallRating($store->id);
    }

    /**
     * @param $storeId
     * @return int
     */
    public function getReviewCount($storeId)
    {
        return DB::table('reviews')->where('store_id', $storeId)->count();
    }

    public function getReviewCountByStar($storeId, $star, $endStar)
    {
        return DB::table('reviews')->where('store_id', $storeId)
                                    ->whereBetween('rating',[$star, $endStar])->count();
    }

    /**
     * @return int
     */
    public function getReviewCountByCurrentUser()
    {
        $store = Stores::getCurrentStore();
        return $this->getReviewCount($store->id);
    }

    /**
     * @param $storeId
     * @param $providerName
     * @return int
     */
    public function getReviewCountByProvider($storeId, $providerName)
    {
        return DB::table('reviews')->where([
            ['store_id', '=', $storeId],
            ['provider_name', '=', $providerName]
        ])->count();
    }
    public function getReviewCountByProviderAndStar($storeId, $star, $endStar, $providerName)
    {
        return DB::table('reviews')->where([
            ['store_id', '=', $storeId],
            ['provider_name', '=', $providerName]
        ])->whereBetween('rating',[$star, $endStar])->count();
    }

    /**
     * @param $providerName
     * @return int
     */
    public function getReviewCountByProviderAndCurrentUser($providerName)
    {
        $store = Stores::getCurrentStore();
        return $this->getReviewCountByProvider($store->id, $providerName);
    }

    /**
     * @param $storeId
     * @param $providerName
     * @return double
     */
    public function getRatingByProvider($storeId, $providerName)
    {
        $avgRating = DB::table('reviews')->where([
            ['store_id', '=', $storeId],
            ['provider_name', '=', $providerName]
        ])->avg('rating');
        return round($avgRating, 1);
    }

    /**
     * @param $providerName
     * @return double
     */
    public function getRatingByProviderAndCurrentUser($providerName)
    {
        $store = Stores::getCurrentStore();
        return $this->getRatingByProvider($store->id, $providerName);
    }

    public function deleteAllReviewByProvider($storeId, $providerName)
    {
        return DB::table('reviews')->where('store_id', $storeId)->where('provider_name', $providerName)->delete();
    }

    public function deleteAllReviewByProviderAndCurrentUser($providerName)
    {
        $store = Stores::getCurrentStore();
        return $this->deleteAllReviewByProvider($store->id, $providerName);
    }
    public function getReviewTotalCountByProvider($providerName)
    {
        return DB::table('reviews')->where([
            ['provider_name', '=', $providerName]
        ])->count();
    }
    public function getReviewLinkTotalCountByProvider($providerName)
    {
        $store = Stores::getCurrentStore();
        return DB::table('review_links')->where([
            ['providerName', '=', $providerName],
            ['storeId', '=', $store->id]
        ])->count();
    }
}

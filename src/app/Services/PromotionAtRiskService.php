<?php
/**
 * Created by TAT.
 * User: Khanh
 * Date: 25/10/2019
 * Time: 17:17 PM
 */

namespace App\Services;


use App\Models\PromotionAtRisk;
use App\Repositories\PromotionAtRiskRepository;
use Faker\Factory;
use Request;
use DB;

class PromotionAtRiskService
{
    /**
     * @var PromotionAtRiskRepository
     */
    private $promotionAtRiskRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct(PromotionAtRiskRepository $proRepo)
    {
        $this->promotionAtRiskRepository = $proRepo;
        $this->faker = Factory::create();
    }

    public function create($input){
        $promotion =PromotionAtRisk::create($input);
        $promotion->save();
        return $promotion;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/11/2017
 * Time: 4:07 PM
 */

namespace App\Services\Review\Drivers;


use App\Models\Review;
use App\Models\ReviewProvider;
use App\Services\Review\Interfaces\ReviewServiceInterface;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class Facebook implements ReviewServiceInterface
{

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            //'base_uri' => 'https://graph.facebook.com/v2.8/'
            'base_uri' => 'https://www.facebook.com'
        ]);
    }

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getReviews($reviewId, $storeId)
    {
        return $this->getAllReviews($reviewId,$storeId,10);
    }

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getAllReviews($reviewId, $storeId)
    {
        return $this->fetchReview($reviewId,$storeId,500);
    }

    private function fetchReview($reviewId, $storeId,$limit){
        $reviews = [];
        $failedCount = 0;
        // $placeid = '1444711412459962';
        $facebook_uri_format = 'https://www.facebook.com%s';
        $base_uri_format = 'https://www.facebook.com/pg/'.$reviewId.'/reviews/?ref=page_internal';
        //$base_uri_format = 'https://www.facebook.com/social_rex/page_feed/?page_id='.$reviewId.'&sort_order=most_recent&feed_container_id=feed_container0';
        $contents = $this->client->post($base_uri_format, $this->buildRequestHeader())->getBody()->getContents();
        //var_dump($contents);die;
        $crawler = new Crawler($contents);
        $filters = $crawler->filter('._3ccb');
        //var_dump($filters);die;
        //$filters = $crawler->filter('._1dwg._1w_m');
        
        foreach ($filters as $filter) {
            $review_html = $filter->ownerDocument->saveHTML($filter);
            $review_crawler = new Crawler($review_html);
            $result_name = $review_crawler->filter('.fwb .profileLink');
            //$star_content = $review_crawler->filter('._51mq.img u');
            $rating_num = 5;
            /*if($star_content->text()){
                preg_match('/\d+/', $star_content->text(), $rating_match);
                $rating_num = $rating_match[0];
            }   */
            $review = new Review();
            $review->store_id = $storeId;
            $real_rating = 0;
            // $review->content = utf8_decode($result_content->text());
            //$review->content = iconv("UTF-8", "ISO-8859-1", utf8_decode($result_content->text()));
            $result_content = $review_crawler->filter('.userContent');
            if(!empty($result_content)){
                $review->content = utf8_decode($result_content->text());
            } else {
                $review->content = "";
            }
            if($review_crawler->filter('._51mq.img u')->count() > 0 ){
                $star_content = $review_crawler->filter('._51mq.img u');
                preg_match('/\d+/', $star_content->text(), $rating_match);
                $rating_num = $rating_match[0];
            } else {
                $rating_num = 5;
                $real_rating = 6;
            }
            $review->rating         = $rating_num;
            $review->real_rating    = $real_rating;
            $review->author =  utf8_decode($result_name->text());
            $published_at = $review_crawler->filter('abbr._5ptz');
            $review->published_at = date('Y-m-d H:i:00', $published_at->attr('data-utime'));
            $review->provider_name = ReviewProvider::PROVIDER_FACEBOOK;
            $result_url = $review_crawler->filter('._5pcq');
            $review_url = explode('/', $result_url->attr('href'));
            $review->url = sprintf($facebook_uri_format, $result_url->attr('href'));
            $review->reviewer_id = sprintf($facebook_uri_format, '/' . $review_url[1]);
            $reviews[] = $review;
        }
        return collect($reviews);
    }

    private function fetchReview_bk($reviewId, $storeId,$limit){
        $reviews = [];
        $failedCount = 0;
        // $placeid = '1444711412459962';
        $facebook_uri_format = 'https://www.facebook.com%s';
        //$base_uri_format = 'https://www.facebook.com/ajax/pages/review/spotlight_reviews_tab_pager/?max_fetch_count=%d&page_id=%s&sort_order=most_recent&__user=0&__a=1';
        $base_uri_format = 'https://www.facebook.com/social_rex/page_feed/?max_fetch_count=%d&page_id=%s&sort_order=most_helpful&feed_container_id=feed_container0';
        $client = new Client(['base_uri' => sprintf($base_uri_format, $limit, $reviewId)]);
        $contents = $client->post('', $this->buildRequestHeader())->getBody()->getContents();
        var_dump($contents);die;
        $fuck_string = 'for (;;);';
        $contents = str_replace($fuck_string, '', $contents);
        $contents = utf8_decode($contents);
        $response = \GuzzleHttp\json_decode($contents, true);
        
        $html = $response['domops'][0][3]['__html'];
        
        $crawler = new Crawler($html);
        $filters = $crawler->filter('._1dwg._1w_m');

        foreach ($filters as $filter) {
            $review_html = $filter->ownerDocument->saveHTML($filter);
            var_dump($review_html);
            $review_crawler = new Crawler($review_html);
            $result_name = $review_crawler->filter('span .profileLink');
            $result_content = $review_crawler->filter('.userContent');
            $published_at = $review_crawler->filter('abbr._5ptz');
            $star_content = $review_crawler->filter('._51mq.img u');
            preg_match('/\d+/', $star_content->text(), $rating_match);
            $rating_num = $rating_match[0];
            $result_url = $review_crawler->filter('._5pcq');
            $review_url = explode('/', $result_url->attr('href'));

            $review = new Review();
            $review->store_id = $storeId;
            $review->reviewer_id = sprintf($facebook_uri_format, '/' . $review_url[1]);
            // $review->content = utf8_decode($result_content->text());
            $review->content = iconv("UTF-8", "ISO-8859-1", utf8_decode($result_content->text()));
            $review->rating = $rating_num;
            $review->real_rating = $rating_num;
            $review->author =  utf8_decode($result_name->text());
            $review->published_at = date('Y-m-d H:i:00', $published_at->attr('data-utime'));
            $review->provider_name = ReviewProvider::PROVIDER_FACEBOOK;
            $review->url = sprintf($facebook_uri_format, $result_url->attr('href'));
            $reviews[] = $review;
        }

        return collect($reviews);
    }
    public function getAllReviewsWithOtherId($reviewId, $storeId, $hotelId)
    {
        return;
    }
    /**
     * @param $reviewId
     * @return string
     */
    public function getReviewLink($reviewId)
    {
        return "https://www.facebook.com/$reviewId/reviews/";
    }

    protected function buildRequestHeader()
    {
        return [
            'delay' => config('pushmeup.setting.delay_time_each_request')
        ];
    }

    private function buildRequestHeader_bk()
    {
        return [
            'delay' => config('pushmeup.setting.delay_time_each_request')
        ];
    }
}

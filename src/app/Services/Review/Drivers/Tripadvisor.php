<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/11/2017
 * Time: 4:49 PM
 */

namespace App\Services\Review\Drivers;


use App\Models\Review;
use App\Models\ReviewProvider;
use App\Services\Review\Interfaces\ReviewServiceInterface;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class Tripadvisor implements ReviewServiceInterface
{
    const TRIPVISOR_DETECT_REVIEW_PATTERN = '/(.*)(Reviews-s1-|Reviews-)(.*)/';
    const TRIPADVISOR_BASE_URI = 'https://www.tripadvisor.com/';

    private $client;

    /**
     * Tripadvisor constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => self::TRIPADVISOR_BASE_URI
        ]);
    }

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getReviews($reviewId, $storeId)
    {
        return $this->getReviewsPerPage($reviewId, $storeId, 'en', 0); // theateam edited lang All to en
    }
    public function getAllReviewsWithOtherId($reviewId, $storeId, $hotelId)
    {
        return;
    }

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getAllReviews($reviewId, $storeId)
    {
        return $this->getReviewsByLang($reviewId, $storeId, 'ALL');
    }

    /**
     * @param $reviewId
     * @return string
     */
    public function getReviewLink($reviewId)
    {
        return static::TRIPADVISOR_BASE_URI . $reviewId;
    }

    private function getReviewsByLang($reviewLink, $storeId, $filterLang)
    {
        $count = intval($this->getReviewPageCount($reviewLink, $filterLang));
        $result = collect([]);
        for ($page = 0; $page < $count; $page++) {
            $reviewInfor = $this->getReviewsPerPage($reviewLink, $storeId, $filterLang, $page);
            $result = $result->merge($reviewInfor);
        };
        return $result;
    }

    private function isReviewLink($reviewLink)
    {
        return preg_match(static::TRIPVISOR_DETECT_REVIEW_PATTERN, $reviewLink, $matches);
    }

    private function createReviewLink($reviewLink, $page)
    {
        $pageString = "";
        if ($page > 0) {
            $pageString = "Reviews-or{$page}0-";
        }
        $replace = '$1' . $pageString . '$3#REVIEWS';
        return preg_replace(static::TRIPVISOR_DETECT_REVIEW_PATTERN, $replace, $reviewLink);
    }

    private function getReviewPageCount($reviewLink, $lang)
    {
        $reviewLinkPage = $this->createReviewLink($reviewLink, 0);
        $response = $this->client->post($reviewLinkPage, $this->buildRequestHeader($lang))->getBody()->getContents();
        $crawler = new Crawler($response);
        $node = $crawler->filter('.pageNumbers > a');
        if ($node->count()) {
            return $node->last()->attr('data-page-number');
        }
        return 1;
    }

    /**
     * @param $reviewLink
     * @param $storeId
     * @param $lang
     * @param $page
     * @return \Illuminate\Support\Collection
     */
     // theateam edited
    private function getReviewsPerPage($reviewLink, $storeId, $lang, $page)
    {
        $lang = 'en';
        if($page < 1) {
            $reviewLinkPage = $this->createReviewLink($reviewLink, $page);
            $response = $this->client->post($reviewLinkPage, $this->buildRequestHeader($lang))->getBody()->getContents();
            $crawler = new Crawler($response);

            //$reviews = $crawler->filter('.basic_review')->each(function (Crawler $node, $i) use ($reviewLink, $storeId) {
                // theateam edited
            $reviews = $crawler->filter('div.review-container')->each(function (Crawler $node, $i) use ($reviewLink, $storeId) {
                    $username = utf8_encode($node->filter('.username')->text());
                    $quote = utf8_encode($node->filter('.quote')->text());
                    $published_at = $node->filterXPath('//span[contains(@class, "ratingDate")]')->text();
                    $published_at = preg_replace('/Reviewed (.*)/', '$1', $published_at);
                    $published_at = date('Y-m-d', strtotime($published_at));
                    $content = utf8_encode($node->filter('.entry')->text());
                    $rating = $this->getRating($node);

                    $review = new Review();
                    $review->store_id = $storeId;
                    $review->reviewer_id = '';
                    $review->content = trim($content);
                    $review->rating = $rating;
                    $review->real_rating = $rating;
                    $review->author =  trim($username);
                    $review->published_at = trim($published_at);
                    $review->provider_name = ReviewProvider::PROVIDER_TRIPADVISOR;
                    $review->url =  $this->getReviewLink($reviewLink);
                    return $review ;
            });
            return collect($reviews);
        } else {
            return 0;
        }
    }

    private function getRating(Crawler $node)
    {

        $field = '';
        $className = $node->filterXPath('//img[contains(@class, "sprite-rating_s_fill rating_s_fill s")]')->evaluate('substring-after(@class, "rating_s_fill s")');
        if (count($className) > 0) {
            $field = $className[0];
        }
        $className = $node->filterXPath('//span[contains(@class, "ui_bubble_rating bubble_")]')->evaluate('substring-after(@class, " bubble_")');
        if (count($className) > 0) {
            $field = $className[0];
        }

        preg_match('/[1-5]/', $field, $matches);
        if (count($matches) > 0) {
            return $matches[0];
        }
        return 0;
    }

    private function getFilterLangs($reviewLink)
    {
        $reviewLinkPage = $this->createReviewLink($reviewLink, 0);
        $response = $this->client->get($reviewLinkPage)->getBody()->getContents();
        $crawler = new Crawler($response);
        return $crawler->filterXPath("//form[@class='moreLanguagesOverlay'][1]//input[@name='filterLang_more']")->extract(['value']);
    }

    /**
     * @param $lang
     * @return array
     */
    protected function buildRequestHeader($lang)
    {
        return [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
                'X-Requested-With' => 'XMLHttpRequest'
            ],
            'form_params' => [
                'filterRating' => '',
                'filterSegment' => '',
                'filterSeasons' => '',
                'filterLang' => $lang
            ],
            'delay' => config('pushmeup.setting.delay_time_each_request')
        ];
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/11/2017
 * Time: 3:00 PM
 */

namespace App\Services\Review\Drivers;


use App\Services\Review\Exceptions\ServiceNotImplementException;
use App\Services\Review\Interfaces\ReviewServiceInterface;

class Factory
{

    /**
     * @param $providerName
     * @return ReviewServiceInterface
     * @throws ServiceNotImplementException
     */
    public function make($providerName)
    {
        $providerName = $this->normalize($providerName);
        $serviceName = 'App\Services\Review\Drivers\\' . $providerName;

        /** @var $service ReviewServiceInterface */
        try {
            $service = resolve($serviceName);
        } catch (\ReflectionException $ex) {
            throw new ServiceNotImplementException($serviceName);
        }

        return $service;
    }

    /**
     * @param $providerName
     * @return string
     */
    private function normalize($providerName)
    {
        return studly_case(strtolower($providerName));
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 18/01/2017
 * Time: 7:52 PM
 */

namespace App\Services\Review\Drivers;


use App\Models\MasterSetting;
use App\Models\Review;
use App\Models\ReviewProvider;
use App\Services\Review\Interfaces\ReviewServiceInterface;
use App\Services\Utils\StringUtils;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class Agoda implements ReviewServiceInterface
{
    const AGODA_BASE_URL = "https://www.agoda.com";
    const AGODA_GET_REVIEW_URL = "https://www.agoda.com/NewSite/en-us/Review/ReviewComments";

    private $proxy;

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getReviews($reviewId, $storeId)
    {
        $hotelId = $this->getHotelId($reviewId);
        $reviews = $this->getReview($hotelId, $storeId);
        return collect($reviews);
    }

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getAllReviews($reviewId, $storeId)
    {
        $setting = MasterSetting::where('key', 'proxy_url')->first();
        if ($setting) {
            $this->proxy = $setting->value;
        }
        $reviews = [];
        $page = 1;
        $hotelId = $this->getHotelId($reviewId);
        do {
            $review = $this->getReview($hotelId, $storeId, $page);
            $reviews = array_merge($reviews, $review);
            $page++;
        } while (count($review) > 0);

        return collect($reviews);
    }
    public function getAllReviewsWithOtherId($reviewId, $storeId, $hotelId)
    {
        $setting = MasterSetting::where('key', 'proxy_url')->first();
        if ($setting) {
            $this->proxy = $setting->value;
        }
        $reviews = [];
        $page = 1;
        do {
            $review = $this->getReview($hotelId, $storeId, $page);
            $reviews = array_merge($reviews, $review);
            $page++;
        } while (count($review) > 0);

        return collect($reviews);
    }

    /**
     * @param $reviewId
     * @return string
     */
    public function getReviewLink($reviewId)
    {
        // happy-day-hotel-da-nang/hotel/da-nang-vn.html
        // return "https://www.agoda.com/happy-day-hotel-da-nang/reviews/da-nang-vn.html";
        return sprintf("https://www.agoda.com/%s", preg_replace("/\/hotel\//", "/reviews/", $reviewId));
    }

    private function buildRequestHeader($options = [])
    {
        return array_merge([
            'delay' => config('pushmeup.setting.delay_time_each_request'),
            'proxy' => $this->proxy
        ], $options);
    }

    public function getHotelId($reviewId)
    {
        $client = new Client(['base_uri' => static::AGODA_BASE_URL . '/' . $reviewId]);
        
        $contents = $client->get('', $this->buildRequestHeader())->getBody()->getContents();
        $crawler = new Crawler($contents);

        $hotelIdNode = $crawler->filter('[data-signin-favorite-hotel-id]');
        return $hotelIdNode->attr('data-signin-favorite-hotel-id');//1723531
    }

    public function getReview($hotelId, $storeId, $page = 1, $pageSize = 10)
    {
        $client = new Client(['base_uri' => static::AGODA_GET_REVIEW_URL]);
        $contents = $client->post('', $this->buildRequestHeader([
            'json' => [
                'hotelId' => $hotelId,
                'page' => $page,
                'pageSize' => $pageSize,
                'sorting' => 1
            ]
        ]))->getBody()->getContents();

        $crawler = new Crawler($contents);  
        $reviews = $crawler->filter('div.individual-review-item')->each(function (Crawler $node, $i) use ($storeId) {
//            $result_content_divs = $node->filter('.review-comment-bubble span');

            $result_name = $node->filterXPath("//*[contains(@class, 'reviewer-name')]/strong");
            $result_content_title = $node->filterXPath("//*[contains(@class, 'comment-title')]");
            $result_content_body = $node->filterXPath("//*[contains(@class, 'comment-text')]");
            $result_rating = $node->filterXPath("//*[contains(@class, 'comment-score')]");
            $result_date = $node->filterXPath("//*[@*[contains(., 'review-date')]]");
            $published_at = $result_date->text();
            $published_at = preg_replace('/Reviewed (.*)/', '$1', $published_at);
            $published_at = date('Y-m-d', strtotime($published_at));
            $author_name = trim(StringUtils::convertToUTF8($result_name->text()));
            $review = new Review();
            $review->store_id = $storeId;
            $review->reviewer_id = $author_name;
            $result_content = "";
            if ($result_content_title->count() > 0) {
                $result_content = $result_content . $result_content_title->text() . "\n";
            }
            if ($result_content_body->count() > 0) {
                $result_content = $result_content . $result_content_body->text();
            }
            $review->content = trim(StringUtils::convertToUTF8($result_content));
            $rating_value = floatval($result_rating->text());
            $review->rating = $rating_value / 2;
            $review->real_rating = $rating_value;
            $review->author = $author_name;
            $review->published_at = $published_at;
            $review->provider_name = ReviewProvider::PROVIDER_AGODA;
//            $review->url = $base_uri;
            return $review;
        });
        return $reviews;
    }
}

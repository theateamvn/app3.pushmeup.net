<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 18/01/2017
 * Time: 7:52 PM
 */

namespace App\Services\Review\Drivers;


use App\Services\Review\Interfaces\ReviewServiceInterface;
use App\Exceptions\DataRequiredException;
use App\Exceptions\ExternalAPIException;
use App\Models\Review;
use App\Models\ReviewProvider;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\TransferStats;

class Booking implements ReviewServiceInterface
{

    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getReviews($reviewId, $storeId)
    {
        return collect([]);
    }
    public function getAllReviewsWithOtherId($reviewId, $storeId, $hotelId)
    {
        return;
    }
    /**
     * @param $reviewId
     * @param $storeId
     * @return \Illuminate\Support\Collection
     */
    public function getAllReviews($reviewId, $storeId)
    {
        $reviews = [];
        // $reviewId = "hotel/vn/ocean-haven.html";
        $review_url = $this->getReviewLink($reviewId);
        $strs = $this->getURLArray($reviewId);
        $languages = ["0" => "en", "1" => 'vi'];//$this->getLanguages($review_url);
       //  var_dump($languages);die;
         //$lang = 'en';
        foreach ($languages as $lang) {
            $strs[2] = $this->getPlaceID($strs[2], $lang);
            // echo $strs[2] . PHP_EOL;
            $place_url = 'http://www.booking.com/reviews/' . $strs[1] . '/' . $strs[0] . '/' . $strs[2];
            $base_uri_format = $place_url . '?customer_type=total&order=featuredreviews&page=%d&rows=75&r_lang=%s';
            $start_at = 1;
            $start_at_step = 1;
            $isFinished = false;
            $failedCount = 0;
            while ($failedCount < 2) {
                $client = new Client(['base_uri' => sprintf($base_uri_format, $start_at, $lang)]);
                $response = $client->get('', $this->buildRequestHeader());
                // echo $response->getStatusCode() . PHP_EOL;
                // $contents = json_decode($contents, true);
                $contents = $response->getBody()->getContents();
               // var_dump($contents);die;
                $crawler = new Crawler($contents);
                //$filters = $crawler->filter('.review_item');
                $filters = $crawler->filter('.review_list');
                // $failedCount = 2;
                if (count($filters) < 1) {
                    break;
                }
                foreach ($filters as $filter) {
                    $review_html = $filter->ownerDocument->saveHTML($filter);
                    $review_crawler = new Crawler($review_html);
                    $is_review = $review_crawler->filter('.review_item_reviewer')->filter('div')->count() > 1;
                    if (!$is_review) {
                        $failedCount++;
                        continue;
                    }

                    $failedCount = 0;
                    $result_name = $review_crawler->filter('.review_item_reviewer h4');
                     //echo trim($result_name->text()) . "|";
                    $result_content = $review_crawler->filter('.review_item_review_content span');
                    // echo trim($result_content->text()) . "|";die;
                    $result_rating = $review_crawler->filter('.review-score-badge');
                    // echo trim($result_rating->text()) . "|";
                    // echo trim($result_date->text()) . "|";
                    /*$result_date = $review_crawler->filterXPath('//meta[@itemprop="datePublished"]')->evaluate('string(@content)');
                    if (count($result_date) > 0) {
                      $result_date = strtotime($result_date[0]);
                    } else {
                      $result_date = $review_crawler->filter('p.review_item_date');
                      $result_date = strtotime($result_date->text());
                    }*/
                    $result_date = $review_crawler->filter('p.review_item_date')->text();
                    //$result_date = strtotime($result_date->text());
                   // echo $result_date->text();die;
                    $review = new Review();
                    $review->store_id = $storeId;
                    $review->reviewer_id = utf8_decode(trim($result_name->text()));
                    $review->content = utf8_decode(trim($result_content->text()));
                    $rating_value = floatval(trim($result_rating->text()));
                    //echo $rating_value;die;
                    $review->rating = $rating_value / 2;
                    $review->real_rating = $rating_value;
                    $review->author = utf8_decode(trim($result_name->text()));
                    $review->published_at = date('Y-m-d H:i:00', '134234354543');
                    $review->published_at_text = $result_date;
                    $review->provider_name = ReviewProvider::PROVIDER_BOOKING;
                    $review->url = $place_url;
                    $reviews[] = $review;
                }
                // echo "Total: " . count($reviews);
                $start_at += $start_at_step;
            }
        }

        // echo "COUNT: " . count($reviews);
        return collect($reviews);
    }

    public function getLanguages($reviewURL)
    {
        $client = new Client(['base_uri' => $reviewURL]);
        $response = $client->get('', $this->buildRequestHeader());
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        // $filters = $crawler->filter('#current_language_foldout');
        // $crawler = new Crawler($filters->html());
        // $filters = $crawler->filter('li');
        // foreach ($filters as $filter) {
        //   $review_html = $filter->ownerDocument->saveHTML($filter);
        //   $review_crawler = new Crawler($review_html);
        //   echo $filter->html() . '|===|' . PHP_EOL;
        // }
        // $test = $crawler->filterXPath('//li[contains(@class, "lang_")]')->evaluate('string(@data-lang)');
        $filters = $crawler->filter('#language');
        $crawler = new Crawler($filters->html());
        $languages = $crawler->filterXPath('//option')->evaluate('string(@value)');
        if (in_array('all', $languages))
        {
            unset($languages[array_search('all',$languages)]);
        }
        return array_unique($languages);
    }

    /**
     * @param $reviewId
     * @return string
     */
    public function getReviewLink($reviewId)
    {
        $strs = $this->getURLArray($reviewId);
        //hotel/vn/ocean-haven.html
        // return "http://www.booking.com/reviews/vn/hotel/ocean-haven.html";
        return sprintf("http://www.booking.com/reviews/%s/%s/%s", $strs[1], $strs[0], $strs[2]);
    }

    public function getURLArray($reviewId)
    {
        return explode("/", $reviewId);
    }

    public function getPlaceID($placeID, $lang)
    {
        $strs = explode(".", $placeID);
        $result = '';
        if (count($strs) == 3) {
          $result = $strs[0] . '.' . $lang . '.' . $strs[2];
        } else {
          $result = $strs[0] . '.' . $lang . '.' . $strs[1];
        }
        return $result;
    }

    private function buildRequestHeader()
    {
        return [
            'delay' => config('pushmeup.setting.delay_time_each_request')
        ];
    }

    public function test()
    {
        $result = null;
        try {
            $client = new Client(['base_uri' => 'https://www.booking.com']);
            $contents = $client->get('/reviewlist.vi.html', [
                'query' => [
                    'label' => ';pagename=villa-louise-hue-beach-boutique;cc1=vn;type=total;score=;dist=1;rows=10;r_lang=;rid=;sort=f_recent_asc'
                ]
            ])->getBody()->getContents();
            \Log::info($contents);

            $crawler = new Crawler($contents);
            $result = $crawler->filter('.review_item')->count();
        }catch (\GuzzleHttp\Exception\ClientException $e){
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            \Log::error($responseBodyAsString);
        }
        return $result;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 1/10/2017
 * Time: 9:07 AM
 */

namespace App\Services;


use App\Models\Member;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Auth;
use Faker\Factory;
use Request;
use DB;
class MemberService
{
    /**
     * @var MemberRepository
     */
    private $memberRepository;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct(MemberRepository $memberRepository)
    {
        $this->memberRepository = $memberRepository;
        $this->faker = Factory::create();
    }

    public function create($input){
        $member =Member::create($input);
        $member->save();
        return $member;
    }

    public function update($input){
        $member=Member::update($input);
        $member->save();
        return $member;
    }

    public function getMemberList()
    {
        $store_id = $this->getCurrentUser()->store()->id;
        $member= DB::table('members')->where('member_store_id', $store_id)->get()->toArray();
        $member=json_decode(json_encode($member),TRUE);
        return $member;
    }

    public function updateLevel($member_id, $totlalCheckIn = 0){
        if($totlalCheckIn) {
            $level = 1;
            $regular    = $this->getConfigCheckin('member_regular');
            $vip        = $this->getConfigCheckin('member_vip');
            
            if($totlalCheckIn >= $regular ) {
                $level = 2;
            }
            if($vip <= $totlalCheckIn) {
                $level = 3;
            }
            $input = array( 'member_level' => $level);
            $this->memberRepository->update($input, $member_id);
        }
        return true;
    }

    public function updateMemberAtRick(){
        $current_time   = time();
        $next_time      = $current_time - (2*86400);
        $from = date('Y-m-d h:i:s', $next_time);
        $memberAtRick = DB::table('members')
                            ->where('member_status', 1)
                            ->where('member_created_at','<=',$from)
                            ->get()->toArray();
        $memberAtRick = json_decode(json_encode($memberAtRick),TRUE);
        foreach ($memberAtRick as $key => $member){
            $input = array( 
                'member_level' => 4
            );
            $this->memberRepository->update($input, $member['id']);
        }
        return true;
    }

    public function getConfigCheckin($name) {
        return DB::table('config_checkin')->where(['status' => 1, 'config_checkin_name' => $name])->get()->toArray()[0]->config_checkin_value;
    }
    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
}
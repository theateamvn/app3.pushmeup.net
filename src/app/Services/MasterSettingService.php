<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 3/13/2017
 * Time: 9:45 AM
 */

namespace App\Services;


use App\Models\MasterSetting;

class MasterSettingService
{
    public function get($key, $default = null)
    {
        $masterSetting = MasterSetting::where('key',$key)->first();
        if($masterSetting){
            return $masterSetting->value;
        }
        return $default;
    }

    public function set($key, $value){
        $masterSetting = MasterSetting::where('key',$key)->first();
        if(!$masterSetting){
            $masterSetting = new MasterSetting;
            $masterSetting->key = $key;
        }
        $masterSetting->value = $value;
        $masterSetting->save();
    }
}
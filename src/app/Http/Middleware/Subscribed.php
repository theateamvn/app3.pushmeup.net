<?php

namespace App\Http\Middleware;

use App\Exceptions\UnSubscriptionException;
use App\User;
use Closure;

class Subscribed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param String[] $plans
     * @return mixed
     * @throws UnSubscriptionException
     */
    public function handle($request, Closure $next, ...$plans)
    {
        if ($request->user()) {
            $subscribed = false;
            foreach ($plans as $plan) {
                $subscribed = $subscribed || $request->user()->subscribed('main', $plan);
            }
            if ($request->user()->status === config('settings.roles.admin') || $subscribed) {
                return $next($request);
            }
        }
        if ($request->ajax()) {
            throw new UnSubscriptionException();
        }
        return redirect('billing');
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class RedirectIfAuthenticated
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \Closure $next
   * @param  string|null $guard
   * @return mixed
   */
  public function handle($request, Closure $next, $guard = null)
  {
    if (Auth::guard($guard)->check()) {
      $role = Auth::guard($guard)->user()->role;
      if($role ===  1){
            $redirectPath = 'https://app.pushmeup.net/admin/dashboard';
        } else if($role ===  2) {
            $redirectPath = 'https://app.pushmeup.net/admin/dashboard-agent';
        } else {
            //$redirectPath = '/dashboard';
            $redirectPath = 'https://app.pushmeup.net/dashboard-checkin';
        }
      //$redirectPath = $role === \App\Models\User::ROLE_ADMIN || $role === \App\Models\User::ROLE_AGENT?'/admin/dashboard':'/dashboard';
      return redirect($redirectPath);
    }

    return $next($request);
  }
}
<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Store;
use App\Repositories\InviteRepository;
use App\Repositories\ReviewProviderRepository;
use App\Services\Bijective;
use App\Repositories\SettingRepository;
use App\Criteria\SettingByStoreCriteria;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ReviewRedirectController extends Controller
{
    private $inviteRepository;
    private $bijective;
    private $reviewProviderRepository;
    private $settingRepository;
    /**
     * ReviewRedirectController constructor.
     * @param InviteRepository $inviteRepository
     * @param ReviewProviderRepository $reviewProviderRepository
     * @param Bijective $bijective
     */
    public function __construct(InviteRepository $inviteRepository, ReviewProviderRepository $reviewProviderRepository, Bijective $bijective, SettingRepository $settingRepo)
    {
        $this->inviteRepository = $inviteRepository;
        $this->bijective = $bijective;
        $this->reviewProviderRepository = $reviewProviderRepository;
        $this->settingRepository = $settingRepo;
    }


    public function index($shortId, $step = null)
    {
        $id = $this->bijective->decode($shortId);

        $invite = $this->inviteRepository->find($id);

        $reviewProviders = $this->reviewProviderRepository->findWhere(['store_id' => $invite->store_id]);
       
        return view('review_redirect', [
            'step' => 'type_select',
            'user_id'    => $invite->id,
            'reviewProviders' => $reviewProviders,
            'store' => $invite->store,
            'shortId' => $shortId
        ]);
    }

    public function redirectByStoreSlug($slug, $id = null)
    {
        $store = Store::where('slug', $slug)->first();
        if (!$store) {
            throw new NotFoundHttpException();
        }
        /** @var Setting $setting */
        $setting = $store->settings->first();
        //$reviewProviders = $this->reviewProviderRepository->findWhere(['store_id' => $store->id]);
        $reviewProviders = array(
            0 => [
                'name' => 'GOOGLE',
                'store_id' => $store->id,
                'link'      => $setting['link_review_google']
            ],
            1 => [
                'name' => 'FACEBOOK',
                'store_id' => $store->id,
                'link'      => $setting['link_review_facebook']
            ],
            2 => [
                'name' => 'YEPLP',
                'store_id' => $store->id,
                'link'      => $setting['link_review_yelp']
            ]
        );
        //var_dump($reviewProviders);die;
        return view('review_redirect', [
            'reviewProviders' => $reviewProviders,
            'store' => $store,
            'mode' => $setting->review_page_mode,
            'user_id'    => $id,
            'feedback_message' => $setting->feedback_message
        ]);
    }
}

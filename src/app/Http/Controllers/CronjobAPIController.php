<?php

namespace App\Http\Controllers;

use App\Http\Requests\API\CreateCronjobAPIRequest;
use App\Http\Requests\API\UpdateCronjobAPIRequest;
use App\Models\Cronjob;
use App\Repositories\CronjobRepository;
use App\Services\CronjobService;
use App\Services\MemberService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\CronjobGetByStoreIdCriteria;
use Response;

/**
 * Class FeedbackController
 * @package App\Http\Controllers\API
 */

class CronjobAPIController extends Controller
{
    private $cronjobRepository;
    private $cronjobService;
    private $memberService;

    public function __construct(CronjobRepository $cronjobRepo, CronjobService $cronjobService, MemberService $memberService)
    {
        $this->cronjobRepository    = $cronjobRepo;
        $this->cronjobService       = $cronjobService;
        $this->memberService        = $memberService;
    }

    public function cronjobRun(Request $request)
    {
        $input = $request->all();
        $cronjob = $this->cronjobService->cronjobRunCheck($input);
        return $cronjob;
    }
    public function cronjobBirthday(Request $request)
    {
        $input = $request->all();
        $this->memberService->updateMemberAtRick();
        $cronjob = $this->cronjobService->cronjobBirthdayCheck($input);
        return $cronjob;
    }
}

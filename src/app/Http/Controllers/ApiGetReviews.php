<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Store;
use App\Models\Review;
use App\Repositories\InviteRepository;
use App\Repositories\ReviewProviderRepository;
use App\Services\Bijective;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\Request;
use DB;

class ApiGetReviews extends Controller
{

    public function get(Request $request){
      $word = $request->key_word;
      $key_word = '';
      if(!empty($request->key_word)){
        $key_word = $word;
      }
      if($key_word){
        $data = DB::table('reviews')
              ->leftJoin('stores', 'reviews.store_id', '=', 'stores.id')
              ->select('reviews.*', 'stores.name', 'stores.address', 'stores.slug')
              ->where('content', '<>', '')
              ->where('stores.name', 'like', '%' . $key_word . '%')
              ->paginate(6);
        return $data;
      }
      else{
        $data = DB::table('reviews')
              ->leftJoin('stores', 'reviews.store_id', '=', 'stores.id')
              ->select('reviews.*', 'stores.name', 'stores.address', 'stores.slug')
              ->where('content', '<>', "")
              ->orderBy('reviews.store_id', 'RAND()')
              ->paginate(6);
        return $data;
      }
    }

    public function detail(Request $request){
      $data_response = array();
      $id = $request->id;
      $store_id = 0;
      $data_detail = DB::table('reviews')
            ->leftJoin('stores', 'reviews.store_id', '=', 'stores.id')
            ->select('reviews.*', 'stores.name', 'stores.address', 'stores.slug')
            ->where('reviews.id', '=', $id)->get();
            foreach ($data_detail as $key => $value) {
              $store_id = $value->store_id;
            }
      $data_response['detail'] = $data_detail;
      $data_related = DB::table('reviews')
            ->leftJoin('stores', 'reviews.store_id', '=', 'stores.id')
            ->select('reviews.*', 'stores.name', 'stores.address', 'stores.slug', 'stores.slug')
            ->where('reviews.id', '!=', $id)
            ->where('reviews.store_id', '=', $store_id)
            ->where('content', '<>', '')
            ->orderBy('reviews.id', 'desc')
            ->skip(10)->take(6)->get();
      $data_response['related'] = $data_related;
      // avg rating
      $avgRating = DB::table('reviews')->where('store_id', $store_id)->avg('rating');
      $data_response['avgRating'] = round($avgRating, 1);
      // total review
      $data_response['totalReview'] = DB::table('reviews')->where('store_id', $store_id)->count();

      return $data_response;

    }
    public function insertPhoneByStoreId(Request $request){
      $result = [];
      $key = $request->key;
      if($key != 'eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlBV')
      {
        $result = [
                    'success' => false,
                    'message' => 'Key not correct'
                  ];
      } else {
        //check account exist
        $storeId = isset($request->storeId) ? $request->storeId : '';
        $checkExist = $this->checkExistStore($storeId);
        if(!$storeId || !$checkExist)
        {
          $result = [
                    'success' => false,
                    'message' => 'Store Id null or not has in the system'
                  ];
        } else {
          $listPhone = json_decode($request->listPhone);
          foreach ($listPhone as $phone) {
            if(!$this->checkExistPhone($storeId, $phone)) {
              DB::table('invites')->insert(
                                  ['phone' => '232434324', 'store_id' => '2', 'name' => 'From App']
                              );
            }
          }
          $result = [
                    'success' => true,
                    'message' => 'Insert Success'
                  ];
        }
      }
      return $result;
    }
    private function checkExistStore($storeId)
    {
       $totalAccount = DB::table('stores')->where([
                                            ['id', '=', $storeId]
                                            ])
                                            ->count();
      if($totalAccount > 0){
        return true;
      } else {
        return false;
      }
    }
    private function checkExistPhone($storeId, $phone)
    {
       $total = DB::table('invites')->where([
                                            ['store_id', '=', $storeId],
                                            ['phone', '=', $phone]
                                            ])
                                            ->count();
      if($total > 0){
        return true;
      } else {
        return false;
      }
    }
}

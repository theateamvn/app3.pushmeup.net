<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 27/12/2016
 * Time: 9:15 PM
 */

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;
use App\Models\User;


class StripeWebhookController extends CashierController
{

    public function handleInvoicePaymentSucceeded($payload)
    {
        /**
         * @var User $user
         */
        $user = $this->getUserByStripeId($payload['data']['object']['customer']);
        $user->status = User::STATUS_ACTIVATED;
        $user->save();
    }

    public function handleInvoiceCreated($payload)
    {
        /**
         * @var User $user
         */
        $user = $this->getUserByStripeId($payload['data']['object']['customer']);
        $user->status = User::STATUS_PENDING;
        $user->save();
    }
}
<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Store;
use App\Repositories\InviteRepository;
use App\Repositories\ReviewProviderRepository;
use App\Services\Bijective;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CheckInController extends Controller
{
    private $inviteRepository;
    private $bijective;
    private $reviewProviderRepository;

    /**
     * ReviewRedirectController constructor.
     * @param InviteRepository $inviteRepository
     * @param ReviewProviderRepository $reviewProviderRepository
     * @param Bijective $bijective
     */
    public function __construct(InviteRepository $inviteRepository, ReviewProviderRepository $reviewProviderRepository, Bijective $bijective)
    {
        $this->inviteRepository = $inviteRepository;
        $this->bijective = $bijective;
        $this->reviewProviderRepository = $reviewProviderRepository;
    }

    public function index($shortId, $step = null)
    {
        $id = $this->bijective->decode($shortId);

        $invite = $this->inviteRepository->find($id);

        $reviewProviders = $this->reviewProviderRepository->findWhere(['store_id' => $invite->store_id]);

        return view('check-in', [
            'step' => 'type_select',
            'user_id'    => $invite->id,
            'reviewProviders' => $reviewProviders,
            'store' => $invite->store,
            'shortId' => $shortId
        ]);
    }

    public function checkInByStoreSlug($slug, $id = null)
    {
        $store = Store::where('slug', $slug)->first();
        if (!$store) {
            throw new NotFoundHttpException();
        }
        /** @var Setting $setting */
        $setting = $store->settings->first();

        $reviewProviders = $this->reviewProviderRepository->findWhere(['store_id' => $store->id]);
        return view('check-in', [
            'store' => $store,
            'mode' => $setting->review_page_mode,
            'reviewProviders' => $reviewProviders,
            'user_id'    => $id,
            'feedback_message' => $setting->feedback_message
        ]);
    }
}

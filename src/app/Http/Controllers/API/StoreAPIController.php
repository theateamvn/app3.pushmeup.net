<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStoreAPIRequest;
use App\Http\Requests\API\UpdateStoreAPIRequest;
use App\Http\Requests\API\UploadMMSPictureRequest;
use App\Http\Requests\API\UploadRedirectPageBackgroundRequest;
use App\Http\Requests\API\UploadStoreLogoRequest;
use App\Http\Requests\SubmitComplaintRequest;
use App\Mail\SendCustomerComplaint;
use App\Models\Store;
use App\Repositories\StoreRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Mail;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Storage;
use App\CustomClass\SimpleImage;
use Illuminate\Support\Facades\Log;

/**
 * Class StoreController
 * @package App\Http\Controllers\API
 */
class StoreAPIController extends AppBaseController
{
    /** @var  StoreRepository */
    private $storeRepository;

    public function __construct(StoreRepository $storeRepo)
    {
        $this->storeRepository = $storeRepo;
    }

    /**
     * Display a listing of the Store.
     * GET|HEAD /stores
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->storeRepository->pushCriteria(new RequestCriteria($request));
        $this->storeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $stores = $this->storeRepository->all();

        return $this->sendResponse($stores->toArray(), 'Stores retrieved successfully');
    }

    /**
     * Store a newly created Store in storage.
     * POST /stores
     *
     * @param CreateStoreAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreAPIRequest $request)
    {
        $input = $request->all();

        $stores = $this->storeRepository->create($input);

        return $this->sendResponse($stores->toArray(), 'Store saved successfully');
    }

    /**
     * Display the specified Store.
     * GET|HEAD /stores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Store $store */
        $store = $this->storeRepository->findWithoutFail($id);

        if (empty($store)) {
            return $this->sendError('Store not found');
        }

        return $this->sendResponse($store->toArray(), 'Store retrieved successfully');
    }

    /**
     * Update the specified Store in storage.
     * PUT/PATCH /stores/{id}
     *
     * @param  int $id
     * @param UpdateStoreAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreAPIRequest $request)
    {
        $input = $request->all();

        /** @var Store $store */
        $store = $this->storeRepository->findWithoutFail($id);

        if (empty($store)) {
            return $this->sendError('Store not found');
        }

        $store = $this->storeRepository->update($input, $id);

        return $this->sendResponse($store->toArray(), 'Store updated successfully');
    }

    /**
     * Remove the specified Store from storage.
     * DELETE /stores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Store $store */
        $store = $this->storeRepository->findWithoutFail($id);

        if (empty($store)) {
            return $this->sendError('Store not found');
        }

        $store->delete();

        return $this->sendResponse($id, 'Store deleted successfully');
    }

    public function smsMessage($id)
    {
        $store = $this->storeRepository->findWithoutFail($id);
        $message = $store->inviteMessages()->where('type', '=', 0)->first();
        return $this->sendResponse($message ? $message->toArray() : '', 'SMS message retrieved successfully');
    }

    public function smsCheckinMessage($id)
    {
        $store = $this->storeRepository->findWithoutFail($id);
        $message = $store->inviteMessages()->where('type', '=', 3)->first();
        return $this->sendResponse($message ? $message->toArray() : '', 'SMS message retrieved successfully');
    }

    public function emailMessage($id)
    {
        $store = $this->storeRepository->findWithoutFail($id);
        $message = $store->inviteMessages()->where('type', '=', 1)->first();
        return $this->sendResponse($message ? $message->toArray() : '', 'Email message retrieved successfully');
    }

    public function emailCheckinMessage($id)
    {
        $store = $this->storeRepository->findWithoutFail($id);
        $message = $store->inviteMessages()->where('type', '=', 4)->first();
        return $this->sendResponse($message ? $message->toArray() : '', 'Email message retrieved successfully');
    }

    public function mmsMessage($id)
    {
        $store = $this->storeRepository->findWithoutFail($id);
        $message = $store->inviteMessages()->where('type', '=', 2)->first();
        return $this->sendResponse($message ? $message->toArray() : '', 'MMS message retrieved successfully');
    }
    

    public function uploadCompanyLogo(UploadStoreLogoRequest $request)
    {
        $id = $request->get('id');
        /** @var Store $store */
        $store = $this->storeRepository->findWithoutFail($id);

        if (empty($store)) {
            return $this->sendError('Store not found');
        }

        $path = $request->file('file')->store('company_logo', 'public');
        $store->company_logo = Storage::url($path);
        
        // Resize image
        $storage_url = public_path(substr($store->company_logo, 1));
        $image = new SimpleImage($storage_url);
        $image_width = $image->getWidth();
        if($image_width >= 600) {
            $image->resize(600,400);
        } else if($image_width >= 500) {
            $image->resize(500,333);
        } else if($image_width >= 400) {
            $image->resize(400,267);
        } else {
            $image->resize(300,200);
        }
        $image->save($storage_url);
        Log::info($storage_url);
        
        $store->save();
        return $this->sendResponse($store, 'Upload company logo successfully');
    }

    public function uploadRedirectPageBackground(UploadRedirectPageBackgroundRequest $request)
    {
        $id = $request->get('id');
        /** @var Store $store */
        $store = $this->storeRepository->findWithoutFail($id);

        if (empty($store)) {
            return $this->sendError('Store not found');
        }

        $path = $request->file('file')->store('company_logo', 'public');
        $store->review_redirect_background = Storage::url($path);
        
        $store->save();
        return $this->sendResponse($store, 'Upload redirect background successfully');
    }

    public function getBySlug($slug)
    {
        $store = $this->storeRepository->findWhere(array('slug'=> $slug));
        return $this->sendResponse($store->toArray(), 'Retrived successfully');
    }
}

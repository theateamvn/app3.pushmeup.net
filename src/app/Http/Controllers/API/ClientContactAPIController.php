<?php

namespace App\Http\Controllers\API;

use App\Criteria\ByUserStoreCriteria;
use App\Http\Requests\API\CreateClientContactAPIRequest;
use App\Http\Requests\API\UpdateClientContactAPIRequest;
use App\Models\ClientContact;
use App\Repositories\ClientContactRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ClientContactController
 * @package App\Http\Controllers\API
 */

class ClientContactAPIController extends AppBaseController
{
    /** @var  ClientContactRepository */
    private $clientContactRepository;

    public function __construct(ClientContactRepository $clientContactRepo)
    {
        $this->clientContactRepository = $clientContactRepo;
    }

    /**
     * Display a listing of the ClientContact.
     * GET|HEAD /clientContacts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->clientContactRepository->pushCriteria(new RequestCriteria($request));
        $this->clientContactRepository->pushCriteria(new LimitOffsetCriteria($request));
        $clientContacts = $this->clientContactRepository->all();

        return $this->sendResponse($clientContacts->toArray(), 'Client Contacts retrieved successfully');
    }

    /**
     * Store a newly created ClientContact in storage.
     * POST /clientContacts
     *
     * @param CreateClientContactAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateClientContactAPIRequest $request)
    {
        $input = $request->all();

        $clientContacts = $this->clientContactRepository->create($input);

        return $this->sendResponse($clientContacts->toArray(), 'Client Contact saved successfully');
    }

    /**
     * Display the specified ClientContact.
     * GET|HEAD /clientContacts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ClientContact $clientContact */
        $clientContact = $this->clientContactRepository->findWithoutFail($id);

        if (empty($clientContact)) {
            return $this->sendError('Client Contact not found');
        }

        return $this->sendResponse($clientContact->toArray(), 'Client Contact retrieved successfully');
    }

    /**
     * Update the specified ClientContact in storage.
     * PUT/PATCH /clientContacts/{id}
     *
     * @param  int $id
     * @param UpdateClientContactAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientContactAPIRequest $request)
    {
        $input = $request->all();

        /** @var ClientContact $clientContact */
        $clientContact = $this->clientContactRepository->findWithoutFail($id);

        if (empty($clientContact)) {
            return $this->sendError('Client Contact not found');
        }

        $clientContact = $this->clientContactRepository->update($input, $id);

        return $this->sendResponse($clientContact->toArray(), 'ClientContact updated successfully');
    }

    /**
     * Remove the specified ClientContact from storage.
     * DELETE /clientContacts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ClientContact $clientContact */
        $clientContact = $this->clientContactRepository->findWithoutFail($id);

        if (empty($clientContact)) {
            return $this->sendError('Client Contact not found');
        }

        $clientContact->delete();

        return $this->sendResponse($id, 'Client Contact deleted successfully');
    }

    public function byUser(Request $request)
    {
        $this->clientContactRepository->pushCriteria(new RequestCriteria($request));
        $this->clientContactRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->clientContactRepository->pushCriteria(new ByUserStoreCriteria());
        /** @var ClientContact $clientContacts */
        $clientContacts = $this->clientContactRepository->paginate(5);

        return $this->sendResponse($clientContacts, 'Client Contact retrieved successfully');
    }

    public function countUser($id, Request $request)
    {
      $count = 0;
      if(isset($id)){
          $count = ClientContact::where('store_id', $id)->count();
      }

      return $this->sendResponse($count, 'Client Contact size retrieved successfully');
    }
}

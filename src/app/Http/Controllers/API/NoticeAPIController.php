<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNoticeAPIRequest;
use App\Http\Requests\API\UpdateNoticeAPIRequest;
use App\Models\Notice;
use App\Repositories\NoticeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class NoticeController
 * @package App\Http\Controllers\API
 */

class NoticeAPIController extends AppBaseController
{
    /** @var  NoticeRepository */
    private $noticeRepository;

    public function __construct(NoticeRepository $noticeRepo)
    {
        $this->noticeRepository = $noticeRepo;
    }

    /**
     * Display a listing of the Notice.
     * GET|HEAD /notices
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->noticeRepository->pushCriteria(new RequestCriteria($request));
        $this->noticeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $notices = $this->noticeRepository->all();

        return $this->sendResponse($notices->toArray(), 'Notices retrieved successfully');
    }

    /**
     * Store a newly created Notice in storage.
     * POST /notices
     *
     * @param CreateNoticeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateNoticeAPIRequest $request)
    {
        $input = $request->all();

        $notices = $this->noticeRepository->create($input);

        return $this->sendResponse($notices->toArray(), 'Notice saved successfully');
    }

    /**
     * Display the specified Notice.
     * GET|HEAD /notices/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Notice $notice */
        $notice = $this->noticeRepository->findWithoutFail($id);

        if (empty($notice)) {
            return $this->sendError('Notice not found');
        }

        return $this->sendResponse($notice->toArray(), 'Notice retrieved successfully');
    }

    /**
     * Update the specified Notice in storage.
     * PUT/PATCH /notices/{id}
     *
     * @param  int $id
     * @param UpdateNoticeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNoticeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Notice $notice */
        $notice = $this->noticeRepository->findWithoutFail($id);

        if (empty($notice)) {
            return $this->sendError('Notice not found');
        }

        $notice = $this->noticeRepository->update($input, $id);

        return $this->sendResponse($notice->toArray(), 'Notice updated successfully');
    }

    /**
     * Remove the specified Notice from storage.
     * DELETE /notices/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Notice $notice */
        $notice = $this->noticeRepository->findWithoutFail($id);

        if (empty($notice)) {
            return $this->sendError('Notice not found');
        }

        $notice->delete();

        return $this->sendResponse($id, 'Notice deleted successfully');
    }
}

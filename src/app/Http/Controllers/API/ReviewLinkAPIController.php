<?php

namespace App\Http\Controllers\API;

use App\Models\ReviewLink;
use App\Models\Store;
use App\Repositories\ReviewLinkRepository;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Services\Bijective;
use Response;
use DB;
/**
 * Class FeedbackController
 * @package App\Http\Controllers\API
 */

class ReviewLinkAPIController extends AppBaseController
{
    /** @var  FeedbackRepository */
    private $reviewLinkRepository;
    private $bijective;

    public function __construct(ReviewLinkRepository $reviewLinkRepo, Bijective $bijective)
    {
        $this->reviewLinkRepository = $reviewLinkRepo;
        $this->bijective = $bijective;
    }

    // theateam edited
    public function reviewLink(Request $request)
    {
        $userId =0 ;
        if($request->user_id){
            $userId = $this->bijective->decode($request->user_id);
        }
        $input['inviteId'] = $userId;
        $input['storeId'] = $request->store_id;
        $input['providerName'] = $request->provider_name;
        $input['agent'] = $request->header('User-Agent');
        if($input['storeId'] && $input['providerName']) {
            $data = $this->getUrlByStoreIdAndProvider($request->store_id, $request->provider_name);
            // $input['agent'] = $_SERVER['HTTP_USER_AGENT'];
            $reviewLink = $this->reviewLinkRepository->create($input);
            $store = Store::where('id', $request->store_id)->first();
            if (!$store) {
                throw new NotFoundHttpException();
            }
            /** @var Setting $setting */
            $setting = $store->settings->first();
            $reviewed_url = "";
            if($request->provider_name == "GOOGLE"){
                $reviewed_url = $setting['link_review_google'];
            } else if($request->provider_name == "FACEBOOK") {
                $reviewed_url = $setting['link_review_facebook'];
            } else {
                $reviewed_url = $setting['link_review_yelp'];
            }
            //$url = $data[0]->reviewed_url;
            $url = $reviewed_url;
            return new RedirectResponse($url);
        } else {
            return true;
        }
    }
    private function getUrlByStoreIdAndProvider($storeId, $providerName) {
        return DB::table('review_providers')->where([
                                            ['store_id', '=', $storeId],
                                            ['name', '=', $providerName]
                                            ])
                                            ->get()
                                            ->toArray();
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBlastContactAPIRequest;
use App\Http\Requests\API\UpdateBlastContactAPIRequest;
use App\Models\BlastContact;
use App\Repositories\BlastContactRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BlastContactController
 * @package App\Http\Controllers\API
 */

class BlastContactAPIController extends AppBaseController
{
    /** @var  BlastContactRepository */
    private $blastContactRepository;

    public function __construct(BlastContactRepository $blastContactRepo)
    {
        $this->blastContactRepository = $blastContactRepo;
    }

    /**
     * Display a listing of the BlastContact.
     * GET|HEAD /blastContacts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->blastContactRepository->pushCriteria(new RequestCriteria($request));
        $this->blastContactRepository->pushCriteria(new LimitOffsetCriteria($request));
        $blastContacts = $this->blastContactRepository->all();

        return $this->sendResponse($blastContacts->toArray(), 'Blast Contacts retrieved successfully');
    }

    /**
     * Store a newly created BlastContact in storage.
     * POST /blastContacts
     *
     * @param CreateBlastContactAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBlastContactAPIRequest $request)
    {
        $input = $request->all();

        $blastContacts = $this->blastContactRepository->create($input);

        return $this->sendResponse($blastContacts->toArray(), 'Blast Contact saved successfully');
    }

    /**
     * Display the specified BlastContact.
     * GET|HEAD /blastContacts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BlastContact $blastContact */
        $blastContact = $this->blastContactRepository->findWithoutFail($id);

        if (empty($blastContact)) {
            return $this->sendError('Blast Contact not found');
        }

        return $this->sendResponse($blastContact->toArray(), 'Blast Contact retrieved successfully');
    }

    /**
     * Update the specified BlastContact in storage.
     * PUT/PATCH /blastContacts/{id}
     *
     * @param  int $id
     * @param UpdateBlastContactAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBlastContactAPIRequest $request)
    {
        $input = $request->all();

        /** @var BlastContact $blastContact */
        $blastContact = $this->blastContactRepository->findWithoutFail($id);

        if (empty($blastContact)) {
            return $this->sendError('Blast Contact not found');
        }

        $blastContact = $this->blastContactRepository->update($input, $id);

        return $this->sendResponse($blastContact->toArray(), 'BlastContact updated successfully');
    }

    /**
     * Remove the specified BlastContact from storage.
     * DELETE /blastContacts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BlastContact $blastContact */
        $blastContact = $this->blastContactRepository->findWithoutFail($id);

        if (empty($blastContact)) {
            return $this->sendError('Blast Contact not found');
        }

        $blastContact->delete();

        return $this->sendResponse($id, 'Blast Contact deleted successfully');
    }
}

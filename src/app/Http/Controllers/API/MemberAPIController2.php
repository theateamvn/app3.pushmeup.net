<?php

namespace App\Http\Controllers\API;

use App\Models\Store;
use App\Models\Member;
use App\Repositories\MemberRepository;
use App\Http\Requests\API\CreateMemberAPIRequest;
use App\Http\Requests\API\UpdateMemberAPIRequest;
use App\Services\MemberService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\MemberListCriteria;
use DB;
use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class MemberAPIController extends AppBaseController
{
    /** @var  memberRepository */
    private $memberRepository;

    private $memberService;

    public function __construct(MemberRepository $memRepo, MemberService $memService)
    {
        $this->memberRepository = $memRepo;
        $this->memberService = $memService;
    }
    public function index(){
        $members=$this->memberService->getMemberList();
        return $this->sendResponse($members,"Member retrieved successfully");
    }
    public function index1(){
        $this->memberRepository->pushCriteria(new RequestCriteria($request));
        $this->memberRepository->pushCriteria(new LimitOffsetCriteria($request));
        $members=$this->memberRepository->all();
        return $this->sendResponse($members->toArray(),"Member retrieved successfully");
    }

    public function pagination(Request $request)
    {
        $input=$request->all();
        $this->memberRepository->pushCriteria(new RequestCriteria($request));
        $this->memberRepository->pushCriteria(new MemberListCriteria($request));
        $this->memberRepository->pushCriteria(new LimitOffsetCriteria($request));
        $members = $this->memberRepository->paginate();
        $members = $members->toArray();
        foreach ($members['data'] as $key => $member){
            if(isset($input['from'])&&isset($input['to'])){
                $total_checkin = DB::table('members_checkin')->where([['member_id', '=', $member['id']]])->whereBetween('checkin_at',[$input['from'],$input['to']])->count();
            }
            else{
                $total_checkin = DB::table('members_checkin')->where([['member_id', '=', $member['id']]])->count();
            }
            $members['data'][$key]['total_checkin'] = $total_checkin;
        }
        return $this->sendResponse($members, 'Member retrieved successfully');
    }

    /**
     * Store a newly created User in storage.
     * POST /Member
     *
     * @param CreateMemberAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMemberAPIRequest $request){
        $userId = $this->getCurrentUser()->id;
        $store_id = $this->getCurrentUser()->store()->id;
        $input=$request->all();
        $input['member_created_by'] =   $userId;
        $input['member_store_id']   =   $store_id;
        $Member =$this->memberService->create($input);
        return $this->sendResponse($Member->toArray(), 'Member saved successfully');
    }

    /**
     * Update a User in storage.
     * PUT /Member
     *
     * @param UpdateMemberAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMemberAPIRequest $request){
        $userId=$this->getCurrentUser()->id;
        $input=$request->all();
        $member=$this->memberRepository->findWithoutFail($id);
        if (empty($member)) {
            return $this->sendError('Member not found');
        }
        $input['member_updated_by']=$userId;
        if($input['member_email']==null){
            $input['member_email']='';
        }
        $member=$this->memberRepository->update($input,$id);
        return $this->sendResponse($member->toArray(), 'Member updated successfully');
    }
    public function delete($id)
    {
        $Member = $this->memberRepository->findWithoutFail($id);

        if (empty($Member)) {
            return $this->sendResponse('Member not found');
        }
        $input['member_status'] = -1;
        $member=$this->memberRepository->update($input,$id);
        return $this->sendResponse($id, 'Member deleted successfully');
    }
    
    /**
     * Update a User in storage.
     * DELETE /Member
     *
     * @return Response
     */
    public function destroy($id)
    {
        $Member = $this->memberRepository->findWithoutFail($id);

        if (empty($Member)) {
            return $this->sendResponse('Member not found');
        }
        $Member->delete();
        return $this->sendResponse($id, 'Member deleted successfully');
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
}
<?php

namespace App\Http\Controllers\API;

use App\Models\Store;
use App\Models\Promotion;
use App\Repositories\PromotionRepository;
use App\Http\Requests\API\CreatePromotionAPIRequest;
use App\Http\Requests\API\UpdatePromotionAPIRequest;
use App\Services\PromotionService;
use App\Services\CronjobService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\UploadedFile;
use App\Criteria\PromotionListCriteria;
use Storage;
use File;
use DB;
use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class PromotionAPIController extends AppBaseController
{
    private $promotionRepository;
    private $promotionService;
    private $cronjobService;

    public function __construct(PromotionRepository $proRepo, PromotionService $proService, CronjobService $cronjobService)
    {
        $this->promotionRepository = $proRepo;
        $this->promotionService = $proService;
        $this->cronjobService = $cronjobService;
    }

    public function index(Request $request){
        $this->promotionRepository->pushCriteria(new PromotionListCriteria($request));
        $this->promotionRepository->pushCriteria(new RequestCriteria($request));
        $this->promotionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $promotions=$this->promotionRepository->all();
        return $this->sendResponse($promotions->toArray(),"Promotion retrieved successfully");
    }

    public function pagination(Request $request)
    {
        $store_id=$this->getCurrentUser()->store()->id;
        $input=$request->all();
        $promotions= $this->promotionService->promotionList($store_id,$input);
        return $this->sendResponse($promotions, 'Promotion retrieved successfully');
    }

    public function getBannerPromotion($slug){
        $promotions = $this->promotionService->getBannerPromotion($slug);
        return $this->sendResponse($promotions, 'Promotion retrieved successfully');
    }

    /**
     * Store a newly created Promotion in storage.
     * POST /Promotion
     *
     * @param CreatePromotionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePromotionAPIRequest $request){
        $userId = $this->getCurrentUser()->id;
        $store_id = $this->getCurrentUser()->store()->id;
        $input=$request->all();
        if(!empty($request->file('image'))){
            $path = $request->file('image')->store('promotion', 'public');
            $input['promotion_image']=str_replace('promotion/','',$path);
        }
        $input['promotion_created_by']  =   $userId;
        $input['promotion_store_id']    =   $store_id;
        $promotion = $this->promotionService->create($input);
        $promotion = $promotion->toArray();
        $cronjob_time = $promotion['promotion_send_date'].' 08:00:00';
        $croinjob = [
            'cronjob_type'      => 'promotion',
            'cronjob_type_id'   => $promotion['id'],
            'cronjob_store_id'  => $store_id,
            'cronjob_send_at'   => $cronjob_time
        ];
        $this->cronjobService->createCronjob($croinjob);
        return $this->sendResponse($promotion, 'Promotion saved successfully');
    }

    /**
     * Update a Promotion in storage.
     * PUT /Promotion
     *
     * @param UpdatePromotionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePromotionAPIRequest $request){
        $userId = $this->getCurrentUser()->id;
        $input=$request->all();
        $promotion=$this->promotionRepository->findWithoutFail($id);
        if (empty($promotion)) {
            return $this->sendError('Promotion not found');
        }
        if(!empty($request->file('image'))){
            $path = $request->file('image')->store('promotion', 'public');
            if($input['promotion_image'] != 'null'){
                $path_delete = public_path()."/storage/promotion/".$input['promotion_image'];
                unlink($path_delete);  
            }
            $input['promotion_image']=str_replace('promotion/','',$path);
        }
        $input['promotion_updated_by']=$userId;
        if(isset($input['cronjob_type_id'])){
            $input['id']=$input['cronjob_type_id'];
        }
        //$input['promotion_status'] = 0;
        $promotion=$this->promotionRepository->update($input,$id);
        
        //$cronjob_promotion=$this->cronjobService->checkCronjob($input['id'],'promotion');
        $cronjob_promotion=$this->cronjobService->checkCronjob($id,'promotion');
        $cronjob_status = 3;
        if($cronjob_promotion[0]['cronjob_status'] == 3 && $promotion['promotion_status'] == 1){
            $cronjob_status = 0;
        }
        else if($cronjob_promotion[0]['cronjob_status'] == 0 && $promotion['promotion_status'] == 1){
            $cronjob_status = 0;
        }
        $cronjob_time = $promotion['promotion_send_date'].' 08:00:00';
        $croinjob = [
            'cronjob_type'      => 'promotion',
            'cronjob_type_id'   => $promotion['id'],
            'cronjob_store_id'  => $promotion['promotion_store_id'],
            'cronjob_send_at'   => $cronjob_time,
            'cronjob_status'    => $cronjob_status
        ];
        $this->cronjobService->updateCronjob($croinjob);
        return $this->sendResponse($promotion, 'Promotion updated successfully');
    }

    public function delete($id)
    {
        $promotion = $this->promotionRepository->findWithoutFail($id);

        if (empty($promotion)) {
            return $this->sendResponse('Promotion not found');
        }
        $input['promotion_status'] = -1;
        $promotion = $this->promotionRepository->update($input,$id);
        // delete cronjob
        $croinjob = [
            'cronjob_type_id'   => $promotion['id'],
            'cronjob_store_id'  => $promotion['promotion_store_id']
        ];
        $this->cronjobService->deleteCronjob($croinjob);
        return $this->sendResponse($id, 'Promotion deleted successfully');
    }

    /**
     * Update a Promotion in storage.
     * DELETE /Promotion
     *
     * @return Response
     */
    public function destroy($id)
    {
        $promotion = $this->promotionRepository->findWithoutFail($id);

        if (empty($promotion)) {
            return $this->sendResponse('Promotion not found');
        }
        $promotion->delete();
        return $this->sendResponse($id, 'Promotion deleted successfully');
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
}
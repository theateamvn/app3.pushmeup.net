<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Services\StatisticService;

class StatisticAPIController extends AppBaseController
{
    private $_statisticService;

    /**
     * StatisticController constructor.
     * @param $statisticService
     */
    public function __construct(StatisticService $statisticService)
    {
        $this->_statisticService = $statisticService;
    }

    public function index()
    {
        return $this->sendResponse($this->_statisticService->getOverviewInfoByCurrentUser(),'Statistic information retrieved successfully');
    }

    public function reviewStar($star = 1, $endStar = 5)
    {
        return $this->sendResponse($this->_statisticService->getReviewsByStar($star,  $endStar),'Review count retrieved successfully');
    }

    public function user(){
        return $this->sendResponse($this->_statisticService->getUsersInformation(),'Users statistic information retrieved successfully');
    }

    public function customer($startDate,$endDate = 'now'){
        return $this->sendResponse($this->_statisticService->getCustomerStatistic($startDate,$endDate),'Customer statistic information retrieved successfully');
    }

    public function reviews(){
        return $this->sendResponse($this->_statisticService->getReviewsInformation(),'Reviews statistic information retrieved successfully');
    }

    public function messageStatus($startDate){
        return $this->sendResponse($this->_statisticService->getMessageStatistic($startDate),'Message statistic information retrieved successfully');
    }

    public function reviewLink(){
        return $this->sendResponse($this->_statisticService->getReviewLinkStatistic(),'Review Link statistic information retrieved successfully');
    }

    public function userAgent(){
        return $this->sendResponse($this->_statisticService->getAgentUsersInformation(),'Users statistic information retrieved successfully');
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStoreAPIRequest;
use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateStoreAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Http\Requests\API\UpdatePasswordAPIRequest;
use App\Models\Store;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\UserListByRoleCriteria;
use Carbon\Carbon;
use Response;
use Stores;
use DB;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserRepository $userRepo, UserService $userService)
    {
        $this->userRepository = $userRepo;
        $this->userService = $userService;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //echo Auth::user()->time_zone;
        //echo Carbon::now('-6')->format('Y-m-d H:i:s');die;
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $this->userRepository->pushCriteria(new UserListByRoleCriteria($request));
        $this->userRepository->pushCriteria(new LimitOffsetCriteria($request)); 
        //$users = $this->userRepository->all();
        $users = $this->userRepository->with(['storeUsers','storeUsers.reviewProviders'])->all();
        $users = $users->toArray();
        $users[0]['user_role']=$this->getCurrentUser()->role;
        foreach ($users as $key => $user){
            if(isset($user['store_users'][0]['id'])){ 
                $store_id = $user['store_users'][0]['id'];
                $total_sms = DB::table('checkin_smssendlog')->where([['store_id', '=', $store_id],['type_send', '=', 'sms']])->count();
                $total_email = DB::table('checkin_smssendlog')->where([['store_id', '=', $store_id],['type_send', '=', 'email']])->count();
                $users[$key]['total_sms']   = $total_sms;
                $users[$key]['total_email'] = $total_email;
            }
        }
        
        return $this->sendResponse($users, 'Users retrieved successfully');
    }

    /**
     * Store a newly created User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUserAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        // var_dump($user);die;
        $input = $request->all();
        $input['created_by'] = $userId;
        $input['updated_by'] = $userId;
        $users = $this->userService->createNewUserWithoutPayment($input);

        return $this->sendResponse($users->toArray(), 'User saved successfully');
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        return $this->sendResponse($user->toArray(), 'User retrieved successfully');
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param  int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input = $request->all();
        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        if(isset($input['password']) && $input['password'] != '') {
          $input['password'] = Hash::make($input['password']);
        } else {
          $input['password'] = $user->password;
        }
        $input['updated_by'] = $userId;
        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'User updated successfully');
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->delete();

        return $this->sendResponse($id, 'User deleted successfully');
    }

    public function stores(){
        $stores = Auth::user()->storeUsers;
        return $this->sendResponse($stores?$stores:[],'List stores retrieved successfully');
    }

    public function createStore(CreateStoreAPIRequest $request){
        $store = Store::create($request->all());
        Auth::user()->storeUsers()->save($store);
        return $this->sendResponse($store,'Create store successfully');
    }

    public function updateStore(UpdateStoreAPIRequest $request){
        $store = Stores::getCurrentStore();
        $store->update($request->all());
        return $this->sendResponse($store,'Update store successfully');
    }

    public function updatePassword(UpdatePasswordAPIRequest $request){
        $user = Auth::user();
        $input = $request->all();
        if (Hash::check($input['current_password'], $user->getAuthPassword())) {
            $user->password = Hash::make($input['password']);
            $user->save();
        } else {
          return $this->sendError('Current password is not correct');
        }
        return $this->sendResponse($user->toArray(),'Update password successfully');
    }
    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
    public function getUserCheckin(Request $request)
    {
        $input = $request->all();
        $user = $this->userService->getUserCheckin($input);
        if(!$user){
            return $this->sendError('User not found',403);
        }
        return $this->sendResponse($user,'User retrived successfully');
    }

}

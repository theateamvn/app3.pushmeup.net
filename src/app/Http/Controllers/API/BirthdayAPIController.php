<?php

namespace App\Http\Controllers\API;

use App\Models\Store;
use App\Models\Birthday;
use App\Repositories\BirthdayRepository;
use App\Http\Requests\API\CreateBirthdayAPIRequest;
use App\Http\Requests\API\UpdateBirthdayAPIRequest;
use App\Services\BirthdayService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\BirthdayListbyStoreIdCriteria;
use DB;
use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class BirthdayAPIController extends AppBaseController
{
    /** @var  BirthdayRepository */
    private $birthdayRepository;

    private $birthdayService;

    public function __construct(BirthdayRepository $birthdayRepository, BirthdayService $birthdayService)
    {
        $this->birthdayRepository = $birthdayRepository;
        $this->birthdayService = $birthdayService;
    }
    // public function index(){
    //     $birthdays=$this->birthdayService->getBithdayList();
    //     return $this->sendResponse($birthdays,"Birthday retrieved successfully");
    // }
    public function index(Request $request){
        $this->birthdayRepository->pushCriteria(new RequestCriteria($request));
        $this->birthdayRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->birthdayRepository->pushCriteria(new LimitOffsetCriteria($request));
        $birthdays=$this->birthdayRepository->all();
        return $this->sendResponse($birthdays->toArray(),"Birthday retrieved successfully");
    }

    public function pagination(Request $request)
    {
        $this->birthdayRepository->pushCriteria(new RequestCriteria($request));
        $this->birthdayRepository->pushCriteria(new BirthdayListbyStoreIdCriteria($request));
        $this->birthdayRepository->pushCriteria(new LimitOffsetCriteria($request));
        $birthdays = $this->birthdayRepository->paginate();
        if($birthdays==null||count($birthdays)==0){
            $userId = $this->getCurrentUser()->id;
            $store_id = $this->getCurrentUser()->store()->id;
            $input=array(
                'birthday_created_by' => $userId,
                'birthday_updated_by' => $userId,
                'birthday_store_id'   => $store_id
            );
            $birthday =$this->birthdayService->create($input);
            return $this->pagination($request);
        }
        return $this->sendResponse( $birthdays->toArray(), 'Birthdays retrieved successfully');
    }

    /**
     * Store a newly created User in storage.
     * POST /Birthdays
     *
     * @param CreateBirthdayAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBirthdayAPIRequest $request){
        $userId = $this->getCurrentUser()->id;
        $store_id = $this->getCurrentUser()->store()->id;
        $input=$request->all();
        $input['birthday_created_by'] =   $userId;
        $input['birthday_store_id']   =   $store_id;
        $birthday =$this->birthdayService->create($input);
        return $this->sendResponse($birthday->toArray(), 'Birthday saved successfully');
    }

    /**
     * Update a User in storage.
     * PUT /Birthday
     *
     * @param UpdateBirthdayAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBirthdayAPIRequest $request){
        $userId=$this->getCurrentUser()->id;
        $input=$request->all();
        $birthday=$this->birthdayRepository->findWithoutFail($id);
        if (empty($birthday)) {
            return $this->sendError('Birthday not found');
        }
        $input['birthday_updated_by']=$userId;
        $birthday=$this->birthdayRepository->update($input,$id);
        return $this->sendResponse($birthday->toArray(), 'Birthday updated successfully');
    }
    public function delete($id)
    {
        $birthday = $this->birthdayRepository->findWithoutFail($id);

        if (empty($birthday)) {
            return $this->sendResponse('Birthday not found');
        }
        $input['birthday_status'] = -1;
        $birthday=$this->birthdayRepository->update($input,$id);
        return $this->sendResponse($id, 'Birthday deleted successfully');
    }
    
    /**
     * Update a User in storage.
     * DELETE /birthday
     *
     * @return Response
     */
    public function destroy($id)
    {
        $birthday = $this->birthdayRepository->findWithoutFail($id);

        if (empty($birthday)) {
            return $this->sendResponse('Birthday not found');
        }
        $birthday->delete();
        return $this->sendResponse($id, 'Birthday deleted successfully');
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
}
<?php

namespace App\Http\Controllers\API;

use App\Criteria\ReviewProviderByCurrentUserCriteria;
use App\Criteria\ReviewProviderCriteria;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateReviewProviderAPIRequest;
use App\Http\Requests\API\UpdateReviewProviderAPIRequest;
use App\Models\ReviewProvider;
use App\Repositories\ReviewProviderRepository;
use App\Services\ReviewProviderService;
use App\Services\ReviewService;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ReviewProviderController
 * @package App\Http\Controllers\API
 */
class ReviewProviderAPIController extends AppBaseController
{
    /** @var  ReviewProviderRepository */
    private $reviewProviderRepository;

    /**
     * @var ReviewProviderService
     */
    private $reviewProviderService;

    /**
     * @var ReviewService
     */
    private $reviewService;

    public function __construct(ReviewProviderRepository $reviewProviderRepo,
                                ReviewProviderService $reviewProviderService,
                                ReviewService $reviewService)
    {
        $this->reviewProviderRepository = $reviewProviderRepo;
        $this->reviewProviderService = $reviewProviderService;
        $this->reviewService = $reviewService;
    }

    /**
     * Display a listing of the ReviewProvider.
     * GET|HEAD /reviewProviders
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->reviewProviderRepository->pushCriteria(new RequestCriteria($request));
        $this->reviewProviderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $reviewProviders = $this->reviewProviderRepository->all();

        return $this->sendResponse($reviewProviders->toArray(), 'Review Providers retrieved successfully');
    }

    /**
     * Store a newly created ReviewProvider in storage.
     * POST /reviewProviders
     *
     * @param CreateReviewProviderAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateReviewProviderAPIRequest $request)
    {
        $input = $request->all();
        $reviewProviders = new ReviewProvider($input);
        $this->reviewService->deleteAllReviewByProviderAndCurrentUser($reviewProviders->name);
        if($reviewProviders->reviewed_key == ''){
            $this->reviewProviderService->deleteAllReviewProviderByNameAndCurrentUser($reviewProviders->name);
            return $this->sendResponse($reviewProviders,'Review Provider deleted successfully');
        }
        $reviewProviders = $this->reviewProviderService->save($reviewProviders->toArray());
        $this->reviewService->fetchAndSaveAllReviewInformationByProvider($reviewProviders);
        return $this->sendResponse($reviewProviders, 'Review Provider saved successfully');
    }

    /**
     * Display the specified ReviewProvider.
     * GET|HEAD /reviewProviders/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ReviewProvider $reviewProvider */
        $reviewProvider = $this->reviewProviderRepository->findWithoutFail($id);

        if (empty($reviewProvider)) {
            return $this->sendError('Review Provider not found');
        }

        return $this->sendResponse($reviewProvider->toArray(), 'Review Provider retrieved successfully');
    }

    /**
     * Update the specified ReviewProvider in storage.
     * PUT/PATCH /reviewProviders/{id}
     *
     * @param  int $id
     * @param UpdateReviewProviderAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReviewProviderAPIRequest $request)
    {
        $input = $request->all();

        /** @var ReviewProvider $reviewProvider */
        $reviewProvider = $this->reviewProviderRepository->findWithoutFail($id);

        if (empty($reviewProvider)) {
            return $this->sendError('Review Provider not found');
        }

        $reviewProvider = new ReviewProvider($input);
        $this->reviewService->deleteAllReviewByProviderAndCurrentUser($reviewProvider->name);
        if($reviewProvider->reviewed_key == ''){
            $this->reviewProviderService->deleteAllReviewProviderByNameAndCurrentUser($reviewProvider->name);
            return $this->sendResponse($reviewProvider,'Review Provider deleted successfully');
        }
        $this->reviewService->fetchAndSaveAllReviewInformationByProvider($reviewProvider);
        $reviewProvider = $this->reviewProviderService->update($reviewProvider->toArray(), $id);
        return $this->sendResponse($reviewProvider->toArray(), 'ReviewProvider updated successfully');
    }

    /**
     * Remove the specified ReviewProvider from storage.
     * DELETE /reviewProviders/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ReviewProvider $reviewProvider */
        $reviewProvider = $this->reviewProviderRepository->findWithoutFail($id);

        if (empty($reviewProvider)) {
            return $this->sendError('Review Provider not found');
        }

        $reviewProvider->delete();

        return $this->sendResponse($id, 'Review Provider deleted successfully');
    }

    public function getListProviderByUser(Request $request)
    {
        $this->reviewProviderRepository->pushCriteria(new RequestCriteria($request));
        $this->reviewProviderRepository->pushCriteria(new ReviewProviderByCurrentUserCriteria($request));
        $this->reviewProviderRepository->pushCriteria(new ReviewProviderCriteria($request->all()));
        $this->reviewProviderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $reviewProviders = $this->reviewProviderRepository->all();

        return $this->sendResponse($reviewProviders->toArray(), 'Review Providers retrieved successfully');
    }

    public function getProviderByUser($provider)
    {
        return $this->sendResponse($this->reviewProviderService->findProviderByCurrentUser($provider), 'Review providers setting retrieved successfully');
    }

    public function testAutoReview(){
        $this->reviewService->fetchAndSaveReviewInformationFromService();
        return true;
    }

}

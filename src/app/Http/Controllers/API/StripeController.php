<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Plan;
use Stripe\Stripe;

class StripeController extends AppBaseController
{
    public function getAll(){
        $all = Plan::all();
        return $this->sendResponse($all,'All plans retrieved successfully');
    }
}

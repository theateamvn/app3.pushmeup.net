<?php

namespace App\Http\Controllers\API;

use App\Models\PromotionAtRisk;
use App\Repositories\PromotionAtRiskRepository;
use App\Http\Requests\API\CreatePromotionAtRiskAPIRequest;
use App\Http\Requests\API\UpdatePromotionAtRiskAPIRequest;
use App\Services\PromotionAtRiskService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\PromotionAtRiskByStoreIdCriteria;
use DB;
use Response;
/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class PromotionAtRiskAPIController extends AppBaseController
{
    private $promotionAtRiskRepository;
    private $promotionAtRiskService;

    public function __construct(PromotionAtRiskRepository $proRepo, PromotionAtRiskService $proService)
    {
        $this->promotionAtRiskRepository = $proRepo;
        $this->promotionAtRiskService = $proService;
    }

    public function index(Request $request)
    {
        $store_id = $this->getCurrentUser()->store()->id;
        $this->promotionAtRiskRepository->pushCriteria(new RequestCriteria($request));
        $this->promotionAtRiskRepository->pushCriteria(new PromotionAtRiskByStoreIdCriteria($request));
        $this->promotionAtRiskRepository->pushCriteria(new LimitOffsetCriteria($request));
        $promotions=$this->promotionAtRiskRepository->all();
        if($promotions==null||count($promotions)==0){
            $userId = $this->getCurrentUser()->id;
            $store_id = $this->getCurrentUser()->store()->id;
            $input=array(
                'promotion_value'      => 10,
                'promotion_type'       => '%',
                'promotion_message'    => 'Welcome back',
                'promotion_created_by' => $userId,
                'promotion_updated_by' => $userId,
                'promotion_store_id'   => $store_id,
                'promotion_send_status'=> 1,
                'promotion_status'     => 0
            );
            $promotions =$this->promotionAtRiskService->create($input);
            return $this->index($request);
        }
        return $this->sendResponse($promotions->toArray(),"Promotion at risk retrieved successfully");
    }

    public function store(CreatePromotionAtRiskAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $store_id = $this->getCurrentUser()->store()->id;
        $input=$request->all();
        $input['promotion_created_by']  =   $userId;
        $input['promotion_store_id']    =   $store_id;
        $promotion = $this->promotionAtRiskService->create($input);
        $promotion = $promotion->toArray();
        return $this->sendResponse($promotion, 'Promotion tt risk saved successfully');
    }

    public function update($id, UpdatePromotionAtRiskAPIRequest $request)
    {
        $userId = $this->getCurrentUser()->id;
        $input=$request->all();
        $promotion=$this->promotionAtRiskRepository->findWithoutFail($id);
        if (empty($promotion)) {
            return $this->sendError('Promotion at risk not found');
        }
        $input['promotion_updated_by']=$userId;
        $promotion=$this->promotionAtRiskRepository->update($input,$id);
        return $this->sendResponse($promotion, 'Promotion at risk updated successfully');
    }

    public function destroy($id)
    {
        $promotion = $this->promotionAtRiskRepository->findWithoutFail($id);

        if (empty($promotion)) {
            return $this->sendResponse('Promotion at risk not found');
        }
        $promotion->delete();
        return $this->sendResponse($id, 'Promotion at risk deleted successfully');
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
}
<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCheckInAPIRequest;
use App\Http\Requests\API\UpdateCheckInAPIRequest;
use App\Models\CheckIn;
use App\Models\MemberCheckout;
use App\Repositories\CheckInRepository;
use App\Services\CheckinService;
use App\Services\CronjobService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\CheckInGetByStoreIdCriteria;
use Carbon\Carbon;
use Response;
use DB;

/**
 * Class FeedbackController
 * @package App\Http\Controllers\API
 */

class CheckinAPIController extends AppBaseController
{
    /** @var  CheckInRepository */
    private $checkInRepository;
    private $checkinService;
    private $cronjobService;

    public function __construct(CheckInRepository $checkInRepo, CronjobService $cronjobService, CheckinService $checkinService)
    {
        $this->checkInRepository = $checkInRepo;
        $this->checkinService = $checkinService;
        $this->cronjobService = $cronjobService;
    }

    /**
     * Display a listing of the Feedback.
     * GET|HEAD /feedback
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->checkInRepository->pushCriteria(new CheckInGetByStoreIdCriteria());
        $this->checkInRepository->pushCriteria(new RequestCriteria($request));
        $this->checkInRepository->pushCriteria(new LimitOffsetCriteria($request));
        $checkIn = $this->checkInRepository->all();
        return $this->sendResponse($checkIn->toArray(), 'checkIn retrieved successfully');
    }

    public function pagination(Request $request)
    {
        $this->checkInRepository->pushCriteria(new CheckInGetByStoreIdCriteria());
        $this->checkInRepository->pushCriteria(new RequestCriteria($request));
        $this->checkInRepository->pushCriteria(new LimitOffsetCriteria($request));
        $checkIn = $this->checkInRepository->paginate();

        return $this->sendResponse($checkIn->toArray(), 'Feedback retrieved successfully');
    }

    public function checkInStepOne(Request $request)
    {
        $input = $request->all();
        // checkin new register
        //$phone      = $input['member_phone'];
        $store_id   = $input['member_store_id'];
        // check member 
        $member = $this->checkinService->memberCheck($input);
        // create cron job
        $time_after_checkin = $this->getConfigCheckin($store_id, 'time_after_checkin');
        //$current_time   = time();
        $time_zone = $input['time_zone'];
        if($time_zone) {
            $current_time   = strtotime(Carbon::now($time_zone)->format('Y-m-d H:i:s'));
        } else {
            $current_time   = strtotime(Carbon::now()->format('Y-m-d H:i:s'));
        }
        $next_time      = $current_time + ($time_after_checkin*60);
        $cronjob_time = date('Y-m-d H:i:s', $next_time);
        $croinjob = [
            'cronjob_type'      => 'checkin',
            'cronjob_type_id'   => $member['checkinMemberId'],
            'cronjob_store_id'  => $member['member']->member_store_id,
            'cronjob_send_at'   => $cronjob_time
        ];
        $this->cronjobService->createCronjob($croinjob);
        return $this->sendResponse($member, 'Checkin successfully');
    }
    public function checkInStepTwo(Request $request)
    {
        $input = $request->all();
        // check member 
        $member = $this->checkinService->memberCheckInStep2($input);
        return $this->sendResponse($member, 'Checkin successfully');
    }
    public function checkinCurrent(Request $request) {
        $input=$request->all();
        $checkinList = $this->checkinService->getCheckinCurrent($input);
        return $this->sendResponse($checkinList, 'Checkin list get successfully');
    }
    public function checkOut($id, Request $request)
    {
        /** @var User $user */
        $input = $request->all();
        $checkin = $this->checkInRepository->findWithoutFail($id);
        $store_id = Auth::user()->store()->id;
        $time_zone = $this->getTimeZone($store_id);
        $current_time =  Carbon::now($time_zone)->format('Y-m-d H:i:s');
        if (empty($checkin)) {
            return $this->sendError('Checkin not found');
        }
        $input_update['checkout_at']       = $current_time;
        $input_update['checkin_status']    = 2;
        $checkin = $this->checkInRepository->update($input_update, $id);
        // send sms
        $checkOut = true;
        $getlistCronjob = DB::table('cronjob_send')
                        ->selectRaw('*')
                        ->where('cronjob_status',  0)
                        ->where('cronjob_type_id', $id)
                        ->get()
                        ->toArray();
        $getlistCronjob = json_decode(json_encode($getlistCronjob), True);                
        $infoSendSms = $getlistCronjob[0];
        $infoSendSms =  json_decode(json_encode($infoSendSms), True);
        $this->cronjobService->checkInCronjob($infoSendSms, $checkOut);
        /** checkout price point */
        $price_service = $input['price_service'];
        $member_checkout = MemberCheckout::create([
            'store_id'              => $store_id,
            'member_id'             => $checkin['member_id'],
            'checkin_id'             => $id,
            'price_services'        => $price_service,
            'price_point'           => $price_service
        ]);
        return $this->sendResponse($checkin->toArray(), 'Member checkout successfully');
    }
    public function checkOut_bk($id)
    {
        /** @var User $user */
        $checkin = $this->checkInRepository->findWithoutFail($id);
        $store_id = Auth::user()->store()->id;
        $time_zone = $this->getTimeZone($store_id);
        $current_time =  Carbon::now($time_zone)->format('Y-m-d H:i:s');

        if (empty($checkin)) {
            return $this->sendError('Checkin not found');
        }
        $input['checkout_at']       = $current_time;
        $input['checkin_status']    = 2;
        $checkin = $this->checkInRepository->update($input, $id);
        // send sms
        $checkOut = true;
        $getlistCronjob = DB::table('cronjob_send')
                        ->selectRaw('*')
                        ->where('cronjob_status',  0)
                        ->where('cronjob_type_id', $id)
                        ->get()
                        ->toArray();
        $getlistCronjob = json_decode(json_encode($getlistCronjob), True);                
        $infoSendSms = $getlistCronjob[0];
        $infoSendSms =  json_decode(json_encode($infoSendSms), True);
        $this->cronjobService->checkInCronjob($infoSendSms, $checkOut);
        return $this->sendResponse($checkin->toArray(), 'Member checkout successfully');
    }
    public function checkinStatistic()
    {
        return $this->sendResponse($this->checkinService->getCheckinStatistic(),'Statistic checkin information retrieved successfully');
    }

    //checkin Offline
    public function CheckinOffline(Request $request){
        $input = $request->all();
        $member = $this->checkinService->memberCheckInOffline($input);
        return $this->sendResponse($member, 'Checkin successfully');
    }

    //get all checkin by userid
    public function CheckinByUser($id){
        $checkin=$this->checkinService->checkinbyUser($id);
        return $this->sendResponse($checkin, 'User checkin list get successfully');
    }
    /**
     * Store a newly created Feedback in storage.
     * POST /feedback
     *
     * @param CreateFeedbackAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCheckInAPIRequest $request)
    {
        $input = $request->all();

        $checkIn = $this->checkInRepository->create($input);

        return $this->sendResponse($checkIn->toArray(), 'Feedback saved successfully');
    }

    /**
     * Display the specified Feedback.
     * GET|HEAD /feedback/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Feedback $checkIn */
        $checkIn = $this->checkInRepository->findWithoutFail($id);

        if (empty($checkIn)) {
            return $this->sendError('Feedback not found');
        }

        return $this->sendResponse($checkIn->toArray(), 'Feedback retrieved successfully');
    }

    /**
     * Update the specified Feedback in storage.
     * PUT/PATCH /feedback/{id}
     *
     * @param  int $id
     * @param UpdateFeedbackAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCheckInAPIRequest $request)
    {
        $input = $request->all();

        /** @var Feedback $checkIn */
        $checkIn = $this->checkInRepository->findWithoutFail($id);

        if (empty($checkIn)) {
            return $this->sendError('Feedback not found');
        }

        $checkIn = $this->checkInRepository->update($input, $id);

        return $this->sendResponse($checkIn->toArray(), 'Feedback updated successfully');
    }

    /**
     * Remove the specified Feedback from storage.
     * DELETE /feedback/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Feedback $checkIn */
        $checkIn = $this->checkInRepository->findWithoutFail($id);

        if (empty($checkIn)) {
            return $this->sendError('Feedback not found');
        }

        $checkIn->delete();

        return $this->sendResponse($id, 'Feedback deleted successfully');
    }

    protected function getCurrentUser()
    {
        $user = Auth::user();
        if (!isset($user)) {
            throw new DataRequiredException('User');
        }
        return $user;
    }
    protected function getConfigCheckin($storeId, $config_checkin_name) {
        $result = DB::table('config_checkin')->where('status', 1)
                                            ->where('store_id', $storeId)
                                            ->where('config_checkin_name', $config_checkin_name)
                                            ->get();
        return $result[0]->config_checkin_value;
    }
    public function getTimeZone($storeId) {
        $result = DB::table('users')
                    ->select('users.*')    
                    ->join("store_users","store_users.user_id","=","users.id")        
                    ->where('store_users.store_id', $storeId)
                    ->where('users.status', 1)
                    ->get();
        if($result[0]->time_zone == 0){
            return '+7:00';
        }
        return $result[0]->time_zone;
    }
}

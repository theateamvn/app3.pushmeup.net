<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 11/02/2017
 * Time: 4:25 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\SendSupportRequest;
use App\Mail\SendSupportTicket;

class CommonAPIController extends AppBaseController
{
    public function supportAndRequest(SendSupportRequest $request)
    {
        \Mail::to(config('pushmeup.setting.support_mail'))->send(new SendSupportTicket($request));
        return $this->sendResponse(null,'Your ticket has been sent');
    }
}
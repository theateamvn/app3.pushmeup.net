<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateMasterRedirectBackgroundAPIRequest;
use App\Http\Requests\API\UpdateMasterRedirectBackgroundAPIRequest;
use App\Http\Requests\API\UploadMasterRedirectBackgroundRequest;
use App\Models\MasterRedirectBackground;
use App\Repositories\MasterRedirectBackgroundRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Storage;

/**
 * Class MasterRedirectBackgroundController
 * @package App\Http\Controllers\API
 */
class MasterRedirectBackgroundAPIController extends AppBaseController
{
    /** @var  MasterRedirectBackgroundRepository */
    private $masterRedirectBackgroundRepository;

    public function __construct(MasterRedirectBackgroundRepository $masterRedirectBackgroundRepo)
    {
        $this->masterRedirectBackgroundRepository = $masterRedirectBackgroundRepo;
    }

    /**
     * Display a listing of the MasterRedirectBackground.
     * GET|HEAD /masterRedirectBackgrounds
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->masterRedirectBackgroundRepository->pushCriteria(new RequestCriteria($request));
        $this->masterRedirectBackgroundRepository->pushCriteria(new LimitOffsetCriteria($request));
        $masterRedirectBackgrounds = $this->masterRedirectBackgroundRepository->all();

        return $this->sendResponse($masterRedirectBackgrounds->toArray(), 'Master Redirect Backgrounds retrieved successfully');
    }

    /**
     * Store a newly created MasterRedirectBackground in storage.
     * POST /masterRedirectBackgrounds
     *
     * @param CreateMasterRedirectBackgroundAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMasterRedirectBackgroundAPIRequest $request)
    {
        $input = $request->all();

        $masterRedirectBackgrounds = $this->masterRedirectBackgroundRepository->create($input);

        return $this->sendResponse($masterRedirectBackgrounds->toArray(), 'Master Redirect Background saved successfully');
    }

    /**
     * Display the specified MasterRedirectBackground.
     * GET|HEAD /masterRedirectBackgrounds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MasterRedirectBackground $masterRedirectBackground */
        $masterRedirectBackground = $this->masterRedirectBackgroundRepository->findWithoutFail($id);

        if (empty($masterRedirectBackground)) {
            return $this->sendError('Master Redirect Background not found');
        }

        return $this->sendResponse($masterRedirectBackground->toArray(), 'Master Redirect Background retrieved successfully');
    }

    /**
     * Update the specified MasterRedirectBackground in storage.
     * PUT/PATCH /masterRedirectBackgrounds/{id}
     *
     * @param  int $id
     * @param UpdateMasterRedirectBackgroundAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMasterRedirectBackgroundAPIRequest $request)
    {
        $input = $request->all();

        /** @var MasterRedirectBackground $masterRedirectBackground */
        $masterRedirectBackground = $this->masterRedirectBackgroundRepository->findWithoutFail($id);

        if (empty($masterRedirectBackground)) {
            return $this->sendError('Master Redirect Background not found');
        }

        $masterRedirectBackground = $this->masterRedirectBackgroundRepository->update($input, $id);

        return $this->sendResponse($masterRedirectBackground->toArray(), 'MasterRedirectBackground updated successfully');
    }

    /**
     * Remove the specified MasterRedirectBackground from storage.
     * DELETE /masterRedirectBackgrounds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MasterRedirectBackground $masterRedirectBackground */
        $masterRedirectBackground = $this->masterRedirectBackgroundRepository->findWithoutFail($id);

        if (empty($masterRedirectBackground)) {
            return $this->sendError('Master Redirect Background not found');
        }

        $masterRedirectBackground->delete();

        return $this->sendResponse($id, 'Master Redirect Background deleted successfully');
    }

    public function upload(UploadMasterRedirectBackgroundRequest $request)
    {
        $path = $request->file('file')->store('public' . DIRECTORY_SEPARATOR . 'master_background');
        $masterRedirectBackgrounds = $this->masterRedirectBackgroundRepository->create([
            'path' => $path,
            'url' => Storage::url($path)
        ]);
        return $this->sendResponse($masterRedirectBackgrounds->toArray(), 'Master Redirect Background saved successfully');
    }
}

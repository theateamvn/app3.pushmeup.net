<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInviteMessageAPIRequest;
use App\Http\Requests\API\UpdateInviteMessageAPIRequest;
use App\Http\Requests\API\UploadMMSPictureRequest;
use App\Models\InviteMessage;
use App\Repositories\InviteMessageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Storage;
use App\CustomClass\SimpleImage;

/**
 * Class InviteMessageController
 * @package App\Http\Controllers\API
 */
class InviteMessageAPIController extends AppBaseController
{
    /** @var  InviteMessageRepository */
    private $inviteMessageRepository;

    public function __construct(InviteMessageRepository $inviteMessageRepo)
    {
        $this->inviteMessageRepository = $inviteMessageRepo;
    }

    /**
     * Display a listing of the InviteMessage.
     * GET|HEAD /inviteMessages
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->inviteMessageRepository->pushCriteria(new RequestCriteria($request));
        $this->inviteMessageRepository->pushCriteria(new LimitOffsetCriteria($request));
        $inviteMessages = $this->inviteMessageRepository->all();

        return $this->sendResponse($inviteMessages->toArray(), 'Invite Messages retrieved successfully');
    }

    /**
     * Store a newly created InviteMessage in storage.
     * POST /inviteMessages
     *
     * @param CreateInviteMessageAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInviteMessageAPIRequest $request)
    {
        $input = $request->all();

        $inviteMessages = $this->inviteMessageRepository->create($input);

        return $this->sendResponse($inviteMessages->toArray(), 'Invite Message saved successfully');
    }

    /**
     * Display the specified InviteMessage.
     * GET|HEAD /inviteMessages/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var InviteMessage $inviteMessage */
        $inviteMessage = $this->inviteMessageRepository->findWithoutFail($id);

        if (empty($inviteMessage)) {
            return $this->sendError('Invite Message not found');
        }

        return $this->sendResponse($inviteMessage->toArray(), 'Invite Message retrieved successfully');
    }

    /**
     * Update the specified InviteMessage in storage.
     * PUT/PATCH /inviteMessages/{id}
     *
     * @param  int $id
     * @param UpdateInviteMessageAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInviteMessageAPIRequest $request)
    {
        $input = $request->all();

        /** @var InviteMessage $inviteMessage */
        $inviteMessage = $this->inviteMessageRepository->findWithoutFail($id);

        if (empty($inviteMessage)) {
            return $this->sendError('Invite Message not found');
        }

        $inviteMessage = $this->inviteMessageRepository->update($input, $id);

        return $this->sendResponse($inviteMessage->toArray(), 'InviteMessage updated successfully');
    }

    /**
     * Remove the specified InviteMessage from storage.
     * DELETE /inviteMessages/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var InviteMessage $inviteMessage */
        $inviteMessage = $this->inviteMessageRepository->findWithoutFail($id);

        if (empty($inviteMessage)) {
            return $this->sendError('Invite Message not found');
        }

        $inviteMessage->delete();

        return $this->sendResponse($id, 'Invite Message deleted successfully');
    }

    public function uploadMmsPicture(UploadMMSPictureRequest $request)
    {
        $id = $request->get('id');
        /** @var InviteMessage $inviteMessage */
        $inviteMessage = $this->inviteMessageRepository->findWithoutFail($id);

        if (empty($inviteMessage)) {
            return $this->sendError('Invite Message not found');
        }

        $mediaPath = array_get($inviteMessage->extra_data, 'mediaPath');
        if ($mediaPath) {
            Storage::delete($mediaPath);
        }

        $path = $request->file('file')->store('mms', 'public');
        $url = Storage::url($path);
        $inviteMessage->extra_data = [
            'mediaPath' => $path,
            'mediaLink' => $url,
            'mediaUrl' => $url
        ];
        
        // Resize image
        $storage_url = substr($url, 1);
        $image = new SimpleImage($storage_url);
        $image_width = $image->getWidth();
        if($image_width >= 600) {
            $image->resize(600,800);
        } else if($image_width >= 500) {
            $image->resize(500,667);
        } else if($image_width >= 400) {
            $image->resize(400,533);
        } else {
            $image->resize(300,400);
        }
        $image->save($storage_url);
        
        $inviteMessage->save();
        return $this->sendResponse($inviteMessage, 'Upload mms picture successfully');
    }
}

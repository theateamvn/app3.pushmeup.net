<?php

namespace App\Http\Controllers\API;

use App\Criteria\SettingByUserCriteria;
use App\Criteria\SettingCheckinByUserCriteria;
use App\Http\Requests\API\CreateSettingAPIRequest;
use App\Http\Requests\API\UpdateSettingAPIRequest;
use App\Models\Setting;
use App\Models\ConfigCheckin;
use App\Repositories\SettingRepository;
use App\Repositories\SettingCheckinRepository;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SettingController
 * @package App\Http\Controllers\API
 */

class SettingCheckinAPIController extends AppBaseController
{
    /** @var  SettingRepository */
    private $settingCheckinRepository;

    public function __construct(SettingCheckinRepository $settingRepo)
    {
        $this->settingCheckinRepository = $settingRepo;
    }

    /**
     * Display a listing of the Setting.
     * GET|HEAD /settings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->settingCheckinRepository->pushCriteria(new RequestCriteria($request));
        $this->settingCheckinRepository->pushCriteria(new LimitOffsetCriteria($request));
        $settings = $this->settingCheckinRepository->all();

        return $this->sendResponse($settings->toArray(), 'Settings retrieved successfully');
    }

    /**
     * Store a newly created Setting in storage.
     * POST /settings
     *
     * @param CreateSettingAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSettingAPIRequest $request)
    {
        $input = $request->all();

        $settings = $this->settingCheckinRepository->create($input);

        return $this->sendResponse($settings->toArray(), 'Setting saved successfully');
    }

    /**
     * Display the specified Setting.
     * GET|HEAD /settings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Setting $setting */
        $setting = $this->settingCheckinRepository->findWithoutFail($id);

        if (empty($setting)) {
            return $this->sendError('Setting not found');
        }

        return $this->sendResponse($setting->toArray(), 'Setting retrieved successfully');
    }

    /**
     * Update the specified Setting in storage.
     * PUT/PATCH /settings/{id}
     *
     * @param  int $id
     * @param UpdateSettingAPIRequest $request
     *
     * @return Response
     */
    

    /**
     * Remove the specified Setting from storage.
     * DELETE /settings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Setting $setting */
        $setting = $this->settingCheckinRepository->findWithoutFail($id);

        if (empty($setting)) {
            return $this->sendError('Setting not found');
        }

        $setting->delete();

        return $this->sendResponse($id, 'Setting deleted successfully');
    }

    public function getSettingByUser(){
        $this->settingCheckinRepository->pushCriteria(new SettingCheckinByUserCriteria());
        $setting = $this->settingCheckinRepository->all();
        return $this->sendResponse($setting->toArray(), 'Setting retrieved successfully');
    }
    public function update($id, UpdateSettingAPIRequest $request)
    {
        $input = $request->all();
        
        if (empty($id)) {
            return $this->sendError('Setting not found');
        }
        foreach ($input as $key => $data){
            $input_ = array(
                'config_checkin_value' => $data['config_checkin_value']
            );
            $setting = $this->settingCheckinRepository->update($input_, $data['id']);
        }   
        return $this->sendResponse($input, 'Setting updated successfully');
    }
}

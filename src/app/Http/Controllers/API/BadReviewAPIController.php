<?php

namespace App\Http\Controllers\API;

use App\Criteria\ByUserStoreCriteria;
use App\Criteria\ReviewCriteria;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateReviewAPIRequest;
use App\Http\Requests\API\UpdateReviewAPIRequest;
use App\Models\Review;
use App\Models\ReviewProvider;
use App\Repositories\ReviewRepository;
use App\Services\ReviewService;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ReviewController
 * @package App\Http\Controllers\API
 */
class BadReviewAPIController extends AppBaseController
{
    /** @var  ReviewRepository */
    private $reviewRepository;

    /**
     * @var ReviewService
     */
    private $reviewService;

    public function __construct(ReviewRepository $reviewRepo, ReviewService $reviewService)
    {
        $this->reviewRepository = $reviewRepo;
        $this->reviewService = $reviewService;
    }

    /**
     * Display a listing of the Review.
     * GET|HEAD /reviews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->reviewRepository->pushCriteria(new RequestCriteria($request));
        $this->reviewRepository->pushCriteria(new ReviewCriteria($request));
        $this->reviewRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->reviewRepository->pushCriteria(new ByUserStoreCriteria());
        $reviews = $this->reviewRepository->all();

        return $this->sendResponse($reviews->toArray(), 'Bad Reviews retrieved successfully');
    }

    /**
     * Display the specified Review.
     * GET|HEAD /reviews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Review $review */
        $review = $this->reviewRepository->findWithoutFail($id);

        if (empty($review)) {
            return $this->sendError('Review not found');
        }

        return $this->sendResponse($review->toArray(), 'Bad Review retrieved successfully');
    }

    /**
     * Remove the specified Review from storage.
     * DELETE /reviews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Review $review */
        $review = $this->reviewRepository->findWithoutFail($id);

        if (empty($review)) {
            return $this->sendError('Review not found');
        }

        $review->delete();

        return $this->sendResponse($id, 'Bad Review deleted successfully');
    }

    public function all()
    {
        $reviewInformation = $this->reviewService->getReviewInformationsByUser();
        return $this->sendResponse($reviewInformation, 'Review retrieved successfully');
    }

    public function get(){
        $this->reviewService->fetchAndSaveAllReviewByCurrentUser();
        return $this->sendResponse(null,'Fetch all review successfully');
    }
}

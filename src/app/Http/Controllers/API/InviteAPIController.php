<?php

namespace App\Http\Controllers\API;

use App\Criteria\ByUserStoreCriteria;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateInviteAPIRequest;
use App\Http\Requests\API\InviteListImportRequest;
use App\Http\Requests\API\SendInviteRequest;
use App\Http\Requests\API\UpdateInviteAPIRequest;
use App\Models\Invite;
use App\Repositories\InviteRepository;
use App\Services\InviteService;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Maatwebsite\Excel\Facades\Excel;
use Prettus\Repository\Criteria\RequestCriteria;
use GuzzleHttp\Client;
use Response;
use DB;
use Auth;
/**
 * Class InviteController
 * @package App\Http\Controllers\API
 */
class InviteAPIController extends AppBaseController
{
    /** @var  InviteRepository */
    private $inviteRepository;

    private $inviteService;

    public function __construct(InviteRepository $inviteRepo, InviteService $inviteService)
    {
        $this->inviteRepository = $inviteRepo;
        $this->inviteService = $inviteService;
    }

    /**
     * Display a listing of the Invite.
     * GET|HEAD /invites
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->inviteRepository->pushCriteria(new RequestCriteria($request));
        $this->inviteRepository->pushCriteria(new LimitOffsetCriteria($request));
        $invites = $this->inviteRepository->all();

        return $this->sendResponse($invites->toArray(), 'Invites retrieved successfully');
    }

    /**
     * Store a newly created Invite in storage.
     * POST /invites
     *
     * @param CreateInviteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInviteAPIRequest $request)
    {
        $input = $request->all();
        // the a team edited
        // check exist data
        $checkExxist = $this->inviteService->checkExistInvite($input['store_id'], $input['phone']);
        if($checkExxist == 1) {
                $getDetailInvite = $this->inviteService->getDetailByStoreIdAndPhone($input['store_id'], $input['phone']);
                $data['data']    = $getDetailInvite;
                 return $this->sendResponse($data['data'][0], 'Invite get successfully');
        } else  {
            $invites = $this->inviteRepository->create($input);
            //var_dump($invites);
            return $this->sendResponse($invites->toArray(), 'Invite saved successfully! Waiting for send Invitation');
        }
    }

    /**
     * Display the specified Invite.
     * GET|HEAD /invites/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Invite $invite */
        $invite = $this->inviteRepository->findWithoutFail($id);

        if (empty($invite)) {
            return $this->sendError('Invite not found');
        }

        return $this->sendResponse($invite->toArray(), 'Invite retrieved successfully');
    }

    /**
     * Update the specified Invite in storage.
     * PUT/PATCH /invites/{id}
     *
     * @param  int $id
     * @param UpdateInviteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInviteAPIRequest $request)
    {
        $input = $request->all();

        /** @var Invite $invite */
        $invite = $this->inviteRepository->findWithoutFail($id);

        if (empty($invite)) {
            return $this->sendError('Invite not found');
        }

        $invite = $this->inviteRepository->update($input, $id);

        return $this->sendResponse($invite->toArray(), 'Invite updated successfully');
    }

    /**
     * Remove the specified Invite from storage.
     * DELETE /invites/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Invite $invite */
        $invite = $this->inviteRepository->findWithoutFail($id);

        if (empty($invite)) {
            return $this->sendError('Invite not found');
        }

        $invite->delete();

        return $this->sendResponse($id, 'Invite deleted successfully');
    }

    public function pagination(Request $request)
    {
        $this->inviteRepository->pushCriteria(new ByUserStoreCriteria());
        $this->inviteRepository->pushCriteria(new RequestCriteria($request));
        $this->inviteRepository->pushCriteria(new LimitOffsetCriteria($request));
        $invites = $this->inviteRepository->paginate();

        return $this->sendResponse($invites->toArray(), 'Invites retrieved successfully');
    }

    public function import(InviteListImportRequest $request)
    {
        $path = $request->file('file')->store('imports');
        $invites = Excel::load(storage_path('app' . DIRECTORY_SEPARATOR . $path), function ($reader) {
        })->get();
        $result = $this->inviteService->importInvites($invites->toArray());
        if ($result) {
            return $this->sendResponse($result, 'Import invites successfully');
        }
        return $this->sendError($result, 'Import invites fail');
    }

    public function send(SendInviteRequest $request)
    {
        set_time_limit(0);
        $this->inviteService->sendListInvites($request->get('list'), $request->get('via'));
        return $this->sendResponse(null, 'Send invites successfully');
    }

    public function sendAll(SendInviteRequest $request)
    {
        $this->inviteService->sendAll($request->get('via'));
        return $this->sendResponse(null, 'Send invites successfully');
    }

    public function getCustomerInformation($startDate = null, $endDate = null)
    {
        if($startDate){
            $result = $this->inviteService->getCustomerInformation($startDate);
        }else if($startDate && $endDate){
            $result = $this->inviteService->getCustomerInformation($startDate,$endDate);
        }else{
            $result = $this->inviteService->getCustomerInformation();
        }

        return $this->sendResponse($result, "Customer information retrieved successfully");
    }
    public function importFromApp(Request $request)
    {
       // var_dump(Auth::user()->email);die;
        set_time_limit(0);
        $input = $request->all();
        //$email = $input['email'];
        $email  = Auth::user()->email;
        $storeId = $input['store_id'];
        $storeIdApp = 0;
        // get email by store id

        $checkStore = $this->inviteService->checkExistStore($storeId);

            $client = new Client;
            $res = $client->request('POST', "http://ios.pushmeup.net/oauth/token", [
                'form_params' => [
                    'client_id' => '11',
                    'grant_type' => 'password',
                    'client_secret' => 'GkSSAgKvFvfFCNlzUxgaZFCDlGfIwJ22rFpFDP8w',
                    'username' => 'onekhoa@gmail.com',
                    'password' => 'taka7801',
                ]
            ]);
            $result = json_decode((string) $res->getBody(), true);
            $auth = $result['token_type'].' '.$result['access_token'];

            // get store id by email
            $acccount_get = $client->request('GET', "http://ios.pushmeup.net/api/users", [
                'headers' => [
                'Authorization'  => $auth
                ]
            ]);

            $result_acccount_get = json_decode((string) $acccount_get->getBody(), true);
            $data_account = $result_acccount_get['data'];
            foreach ($data_account as $account) {
                if($account['email'] == $email)
                {
                    $accountId = $account['id'];
                    break;
                }
            }

            // get store id by id
            $acccount_get_detail = $client->request('GET', "http://ios.pushmeup.net/api/stores?search=user_id:".$accountId, [
                'headers' => [
                'Authorization'  => $auth
                ]
            ]);
            $result_acccount_get_detail = json_decode((string) $acccount_get_detail->getBody(), true);
            $data_account_detail = $result_acccount_get_detail['data'];
            $storeIdApp = $data_account_detail[0]['id'];
            // get list by storeid
            $res_get = $client->request('GET', "http://ios.pushmeup.net/api/service_histories?search=store_id:".$storeIdApp, [
                'headers' => [
                'Authorization'     => $auth
                ]
            ]);
            $result_get = json_decode((string) $res_get->getBody(), true);
            if(!$result_get['success'] || !$storeIdApp || $result_get['data'] == null)
            {
                return $this->sendError('Can not find data from App, or data null');
            } else {
                $data = $result_get['data'];
                foreach ($data as $phone) {
                    //if($phone['store_id'] == $storeId)
                   // {
                        $checkPhone = $this->inviteService->checkExistInviteByStoreApp($storeId, $storeIdApp, str_replace("+","",$phone['phone']));
                        if(!$checkPhone) {
                            $this->inviteService->insertPhoneFromApp($storeId, $storeIdApp, str_replace("+","",$phone['phone']));
                        }
                  //  }
                }
                return $this->sendResponse(null, 'Import invites from app successfully');
            }
    }
    public function click_review_link($id = 0){
        return $this->sendResponse($this->inviteService->getReviewLinkByCustomer($id),'Review Link clicked information retrieved successfully');
    }
}

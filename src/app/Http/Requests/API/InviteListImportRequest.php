<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 10/12/2016
 * Time: 8:23 PM
 */

namespace App\Http\Requests\API;


use App\Http\Requests\APIRequest;

class InviteListImportRequest extends APIRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|mimes:xls,xlsx'
        ];
    }


}
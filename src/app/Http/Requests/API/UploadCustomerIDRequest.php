<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 12/14/2016
 * Time: 4:03 PM
 */

namespace App\Http\Requests\API;


use App\Http\Requests\APIRequest;

class UploadCustomerIDRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'=>'required|image',
            'id'=>'required'
        ];
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 11/02/2017
 * Time: 4:26 PM
 */

namespace App\Http\Requests\API;


use App\Http\Requests\APIRequest;

class SendSupportRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'content' => 'required'
        ];
    }
}
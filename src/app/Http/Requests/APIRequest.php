<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 11/30/2016
 * Time: 2:49 PM
 */

namespace App\Http\Requests;

use InfyOm\Generator\Request\APIRequest as BaseAPIRequest;
use InfyOm\Generator\Utils\ResponseUtil;
use Response;

class APIRequest extends BaseAPIRequest
{
    public function response(array $errors)
    {
        $messages = implode(' ', array_flatten($errors));
        return Response::json(ResponseUtil::makeError($messages,$errors), 400);
    }

}
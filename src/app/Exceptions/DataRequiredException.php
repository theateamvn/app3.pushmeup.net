<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 22/11/2016
 * Time: 8:34 PM
 */

namespace App\Exceptions;


class DataRequiredException extends \Exception
{
    public $fieldName;

    /**
     * DataRequiredException constructor.
     * @param $fieldName
     */
    public function __construct($fieldName)
    {
        $this->fieldName = $fieldName;
        $this->message = trans('exceptions.data_required_exception', ['fieldName' => $this->fieldName]);
    }

    public function __toString()
    {
        return trans('exceptions.data_required_exception', ['fieldName' => $this->fieldName]);
    }


}
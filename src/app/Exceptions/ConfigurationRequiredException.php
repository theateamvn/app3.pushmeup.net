<?php
/**
 * Created by PhpStorm.
 * User: Thien
 * Date: 11/30/2016
 * Time: 10:38 AM
 */

namespace App\Exceptions;


class ConfigurationRequiredException extends \Exception
{
    private $configurationName;

    /**
     * ConfigurationNotFoundException constructor.
     * @param $configurationName
     */
    public function __construct($configurationName)
    {
        $this->configurationName = $configurationName;
        $this->message = trans('exceptions.configuration_required',['configurationName'=>$this->configurationName]);
    }


}
<?php

namespace App\Mail;

use App\Http\Requests\API\SendSupportRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendSupportTicket extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $email;
    public $content;

    /**
     * Create a new message instance.
     */
    public function __construct(SendSupportRequest $request)
    {
        $this->user = $request->user();
        $this->content = $request->get('content');
        $this->email = $request->get('email');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from([
                'address' => $this->email,
                'name' => $this->user->full_name
            ])
            ->subject("Support ticket from {$this->user->username}")
            ->view('emails.support.ticket');
    }
}

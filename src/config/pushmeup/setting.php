<?php
/**
 * Created by PhpStorm.
 * User: Lai Vu
 * Date: 1/6/2017
 * Time: 11:01 AM
 */

return [
    'delay_time_each_request' => env('DELAY_TIME_EACH_REQUEST', 15000),
    'auto_proxy' => env('PUSHMEUP_AUTO_PROXY', false),
    'proxy_enabled' => env('PUSMEUP_PROXY_ENABLED', false),
    'proxy' => env('PUSHMEUP_PROXY', ''),
    'admin_mail' => explode(',',env('ADMIN_MAIL', '')),
    'facebook_fanpage' => env('FACEBOOK_FANPAGE', ''),
    'support_mail' => explode(',',env('SUPPORT_MAIL', ''))
];
<?php
/**
 * Created by PhpStorm.
 * User: Lai Vu
 * Date: 1/6/2017
 * Time: 11:01 AM
 */

return [
    'member_regular'        => 500,
    'member_vip'            => 1000,
    'time_after_checkin'    => 90,
];
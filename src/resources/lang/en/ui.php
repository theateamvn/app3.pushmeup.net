<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 20/11/2016
 * Time: 8:38 AM
 */

return [
  'my_profile'=>'My Profile',
  'logout'=>'Log Out',
  'send_invite_request'=>'Send Invite Request',
  'copyright'=>'2020 &copy; Fosive Team'
];
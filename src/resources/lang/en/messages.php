<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 20/11/2016
 * Time: 8:16 AM
 */

return [
    'note_store_must_config' => 'NOTE: Store haven\'t been setting, you must set it first.',
    'we_appreciate_you_so_much_for_leaving_your_review_and_rating_us'=>'We appreciate you so much for leaving your review and rating us',
    'survey_message_1'=>'Thank you for giving us the opportunity to better serve you. Please help us by taking a few minutes to tell us about the service that you have received so far. Are you happy with service today ?',
    'survey_message_2'=>'Thank you for your feedback, we will contact you soon!',
];
<?php
/**
 * Created by IntelliJ IDEA.
 * User: acesi
 * Date: 22/11/2016
 * Time: 8:41 PM
 */

return [
    'data_required_exception' => ':fieldName is required',
    'configuration_required' => 'You must been set :configurationName'
];
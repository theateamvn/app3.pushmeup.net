<?php if(Auth::user()->role == 2) { ?>
<li class="{!! Request::is('admin') || Request::is('admin/dashboard-agent*') ? 'active' : '' !!}">
    <a href="{!! url('admin/dashboard-agent') !!}" class="nav-link nav-toggle">
        <i class="icon-home"></i>
        <span class="title">Dashboard</span>
    </a>
</li>
<li class="{!! Request::is('admin/users') ? 'active' : '' !!}">
    <a href="{!! url('admin/users') !!}" class="nav-link nav-toggle">
        <i class="icon-users"></i>
        <span class="title">Users</span>
    </a>
</li>
<?php } else { ?>
<li class="{!! Request::is('admin') || Request::is('admin/dashboard*') ? 'active' : '' !!}">
    <a href="{!! url('admin/dashboard') !!}" class="nav-link nav-toggle">
        <i class="icon-home"></i>
        <span class="title">Dashboard</span>
    </a>
</li>

<li class="{!! Request::is('admin/notices') ? 'active' : '' !!}">
    <a href="{!! url('admin/notices') !!}" class="nav-link nav-toggle">
        <i class="icon-flag"></i>
        <span class="title">Notices</span>
    </a>
</li>
<li class="{!! Request::is('admin/agents') ? 'active' : '' !!}">
    <a href="{!! url('admin/agents') !!}" class="nav-link nav-toggle">
        <i class="icon-users"></i>
        <span class="title">Agents</span>
    </a>
</li>
<li class="{!! Request::is('admin/users') ? 'active' : '' !!}">
    <a href="{!! url('admin/users') !!}" class="nav-link nav-toggle">
        <i class="icon-users"></i>
        <span class="title">Users</span>
    </a>
</li>
<li class="{!! Request::is('admin/manage-background') ? 'active' : '' !!}">
    <a href="{!! url('admin/manage-background') !!}" class="nav-link nav-toggle">
        <i class="icon-picture"></i>
        <span class="title">Backgrounds</span>
    </a>
</li>
<li class="{!! Request::is('admin/settings') ? 'active' : '' !!}">
    <a href="{!! url('admin/settings') !!}" class="nav-link nav-toggle">
        <i class="icon-power"></i>
        <span class="title">Setting</span>
    </a>
</li>
<?php } ?>



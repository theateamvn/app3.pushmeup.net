<!-- BEGIN FOOTER -->
<div class="page-footer">
  <div class="page-footer-inner"> @lang('ui.copyright')
  </div>
  <div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
  </div>
</div>
<!-- END FOOTER -->
<li class="{!! Request::is('dashboard/*') || Request::is('/') ? 'active' : '' !!}" id="menu-1">
    <a href="{!! url('/dashboard') !!}" class="nav-link nav-toggle">
        <i class="icon-home"></i>
        <span class="title">Dashboard</span>
    </a>
</li>
<li class="{!! Request::is('reviews*') ? 'active' : '' !!}" id="menu-2">
    <a href="{!! url('/reviews') !!}" class="nav-link nav-toggle">
        <i class="icon-speech"></i>
        <span class="title">Reviews</span>
    </a>
</li>
<li class="{!! Request::is('bad-reviews*') ? 'active' : '' !!}" id="menu-2">
    <a href="{!! url('/bad-reviews') !!}" class="nav-link nav-toggle">
        <i class="icon-speech"></i>
        <span class="title text-danger">Bad Reviews</span>
    </a>
</li>
<li class="{!! Request::is('import_customers*') ? 'active' : '' !!}" id="menu-6">
    <a href="{!! url('/import_customers') !!}" class="nav-link nav-toggle">
        <i class="icon-user-follow"></i>
        <span class="title">Import Customer</span>
    </a>
</li>
<li class="{!! Request::is('customers*') ? 'active' : '' !!}" id="menu-3">
    <a href="{!! url('/customers') !!}" class="nav-link nav-toggle">
        <i class="icon-users"></i>
        <span class="title">Customers</span>
    </a>
</li>
<li class="{!! Request::is('send-sms*') ? 'active' : '' !!}" id="menu-3">
    <a href="{!! url('/send-sms') !!}" class="nav-link nav-toggle">
        <i class="icon-bubbles"></i>
        <span class="title">Send SMS</span>
    </a>
</li>
<li class="{!! Request::is('templates*') ? 'active' : '' !!}" id="menu-4">
    <a href="{!! url('/templates') !!}" class="nav-link nav-toggle">
        <i class="icon-envelope"></i>
        <span class="title">Content templates</span>
    </a>
</li>
<li class="{!! Request::is('settings*') ? 'active' : '' !!}" id="menu-5">
    <a href="{!! url('/settings') !!}" class="nav-link nav-toggle">
        <i class="icon-settings"></i>
        <span class="title">Settings</span>
    </a>
</li>
<li class="{!! Request::is('feedback*') ? 'active' : '' !!}" id="menu-5">
    <a href="{!! url('/feedback') !!}" class="nav-link nav-toggle">
        <i class="icon-feed"></i>
        <span class="title">Feedback</span>
    </a>
</li>
<li class="heading">
    <h3 class="uppercase">Check-In Function</h3>
</li>
<li class="{!! Request::is('dashboard-checkin*') ? 'active' : '' !!}" id="menu-7">
    <a href="{!! url('/dashboard-checkin') !!}" class="nav-link nav-toggle">
        <i class="fa fa-check-square"></i>
        <span class="title">CheckIn</span>
    </a>
</li>
<li class="{!! Request::is('members*') ? 'active' : '' !!}" id="menu-7">
    <a href="{!! url('/members') !!}" class="nav-link nav-toggle">
        <i class="icon-user"></i>
        <span class="title">Members</span>
    </a>
</li>
<li class="{!! Request::is('promotions*') ? 'active' : '' !!}" id="menu-8">
    <a href="{!! url('/promotions') !!}" class="nav-link nav-toggle">
        <i class="fa fa-gg-circle"></i>
        <span class="title">Promotion</span>
    </a>
</li>
<li class="{!! Request::is('promotion-at-risk*') ? 'active' : '' !!}" id="menu-8">
    <a href="{!! url('/promotion-at-risk') !!}" class="nav-link nav-toggle">
        <i class="fa fa-gg-circle"></i>
        <span class="title">Promotion At Risk</span>
    </a>
</li>
<li class="{!! Request::is('birthdays*') ? 'active' : '' !!}" id="menu-8">
    <a href="{!! url('/birthdays') !!}" class="nav-link nav-toggle">
        <i class="fa fa-gg-circle"></i>
        <span class="title">Birthday</span>
    </a>
</li>
<li class="{!! Request::is('setting-checkin*') ? 'active' : '' !!}" id="menu-9">
    <a href="{!! url('/setting-checkin') !!}" class="nav-link nav-toggle">
        <i class="icon-settings"></i>
        <span class="title">Setting Checkin</span>
    </a>
</li>
<support-and-request email="{{!Auth::guest()?Auth::user()->email:''}}" username="{{!Auth::guest()?Auth::user()->username:''}}"/>

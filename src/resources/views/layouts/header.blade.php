<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
  <!-- BEGIN HEADER INNER -->
  <div class="page-header-inner ">
    <!-- BEGIN LOGO -->
    <div class="page-logo">
      <a href="{!! route('root') !!}">
        <img src="../assets/layouts/layout4/img/logo-light.png" alt="logo" class="logo-default"/> </a>

      <div class="menu-toggler sidebar-toggler">
        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
      </div>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
       data-target=".navbar-collapse"> </a>
    <!-- END RESPONSIVE MENU TOGGLER -->
    <!-- BEGIN PAGE ACTIONS -->
    <!-- DOC: Remove "hide" class to enable the page header actions -->
    <send-request></send-request>
    <!-- END PAGE ACTIONS -->
    <!-- BEGIN PAGE TOP -->
    <div class="page-top">
      <!-- BEGIN HEADER SEARCH BOX -->
      <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
      {{--<form class="search-form" action="page_general_search_2.html" method="GET">--}}
        {{--<div class="input-group">--}}
          {{--<input type="text" class="form-control input-sm" placeholder="Search..." name="query">--}}
                            {{--<span class="input-group-btn">--}}
                                {{--<a href="javascript:;" class="btn submit">--}}
                                  {{--<i class="icon-magnifier"></i>--}}
                                {{--</a>--}}
                            {{--</span>--}}
        {{--</div>--}}
      {{--</form>--}}
      <!-- END HEADER SEARCH BOX -->
      <!-- BEGIN TOP NAVIGATION MENU -->
      <div class="top-menu">
        <ul class="nav navbar-nav pull-right">
          <li class="separator hide"></li>
          <!-- BEGIN NOTIFICATION DROPDOWN -->
          <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
          {{--<li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">--}}
            {{--<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"--}}
               {{--data-close-others="true">--}}
              {{--<i class="icon-bell"></i>--}}
              {{--<span class="badge badge-success"> 7 </span>--}}
            {{--</a>--}}
            {{--<ul class="dropdown-menu">--}}
              {{--<li class="external">--}}
                {{--<h3>--}}
                  {{--<span class="bold">12 pending</span> notifications</h3>--}}
                {{--<a href="page_user_profile_1.html">view all</a>--}}
              {{--</li>--}}
              {{--<li>--}}
                {{--<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">--}}
                  {{--<li>--}}
                    {{--<a href="javascript:;">--}}
                      {{--<span class="time">just now</span>--}}
                                                    {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-success">--}}
                                                            {{--<i class="fa fa-plus"></i>--}}
                                                        {{--</span> New user registered. </span>--}}
                    {{--</a>--}}
                  {{--</li>--}}
                  {{--<li>--}}
                    {{--<a href="javascript:;">--}}
                      {{--<span class="time">3 mins</span>--}}
                                                    {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-danger">--}}
                                                            {{--<i class="fa fa-bolt"></i>--}}
                                                        {{--</span> Server #12 overloaded. </span>--}}
                    {{--</a>--}}
                  {{--</li>--}}
                  {{--<li>--}}
                    {{--<a href="javascript:;">--}}
                      {{--<span class="time">10 mins</span>--}}
                                                    {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-warning">--}}
                                                            {{--<i class="fa fa-bell-o"></i>--}}
                                                        {{--</span> Server #2 not responding. </span>--}}
                    {{--</a>--}}
                  {{--</li>--}}
                  {{--<li>--}}
                    {{--<a href="javascript:;">--}}
                      {{--<span class="time">14 hrs</span>--}}
                                                    {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-info">--}}
                                                            {{--<i class="fa fa-bullhorn"></i>--}}
                                                        {{--</span> Application error. </span>--}}
                    {{--</a>--}}
                  {{--</li>--}}
                  {{--<li>--}}
                    {{--<a href="javascript:;">--}}
                      {{--<span class="time">2 days</span>--}}
                                                    {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-danger">--}}
                                                            {{--<i class="fa fa-bolt"></i>--}}
                                                        {{--</span> Database overloaded 68%. </span>--}}
                    {{--</a>--}}
                  {{--</li>--}}
                  {{--<li>--}}
                    {{--<a href="javascript:;">--}}
                      {{--<span class="time">3 days</span>--}}
                                                    {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-danger">--}}
                                                            {{--<i class="fa fa-bolt"></i>--}}
                                                        {{--</span> A user IP blocked. </span>--}}
                    {{--</a>--}}
                  {{--</li>--}}
                  {{--<li>--}}
                    {{--<a href="javascript:;">--}}
                      {{--<span class="time">4 days</span>--}}
                                                    {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-warning">--}}
                                                            {{--<i class="fa fa-bell-o"></i>--}}
                                                        {{--</span> Storage Server #4 not responding dfdfdfd. </span>--}}
                    {{--</a>--}}
                  {{--</li>--}}
                  {{--<li>--}}
                    {{--<a href="javascript:;">--}}
                      {{--<span class="time">5 days</span>--}}
                                                    {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-info">--}}
                                                            {{--<i class="fa fa-bullhorn"></i>--}}
                                                        {{--</span> System Error. </span>--}}
                    {{--</a>--}}
                  {{--</li>--}}
                  {{--<li>--}}
                    {{--<a href="javascript:;">--}}
                      {{--<span class="time">9 days</span>--}}
                                                    {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-danger">--}}
                                                            {{--<i class="fa fa-bolt"></i>--}}
                                                        {{--</span> Storage server failed. </span>--}}
                    {{--</a>--}}
                  {{--</li>--}}
                {{--</ul>--}}
              {{--</li>--}}
            {{--</ul>--}}
          {{--</li>--}}
          <!-- END NOTIFICATION DROPDOWN -->
          <li class="separator hide"></li>
          <!-- BEGIN USER LOGIN DROPDOWN -->
          <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
          <li class="dropdown dropdown-user dropdown-dark" id="edit-profile">
            <a href="javascript:;" class="dropdown-toggle hidden-xs" data-toggle="dropdown" data-hover="dropdown"
               data-close-others="true">
              <span class="username username-hide-on-mobile"> {{ Auth::user()->full_name }} </span>
              <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
              <img alt="" class="img-circle" src="../assets/layouts/layout4/img/avatar.png"/> </a>
            <ul class="dropdown-menu dropdown-menu-default">
              <li>
                <a href="{!! url('profile') !!}">
                  <i class="icon-user"></i> @lang('ui.my_profile') </a>
              </li>

              @if (Auth::user()->role === 1)
                <li>
                  <a href="{!! url('admin') !!}">
                    <i class="icon-users"></i> Admin Panel </a>
                </li>
              @elseif(Auth::user()->role === 2)
                <li>
                  <a href="{!! url('admin/dashboard-agent') !!}">
                    <i class="icon-users"></i> Admin Panel </a>
                </li>
              @else
                  <li>
                    <view-tour-button></view-tour-button>
                  </li>
              @endif

              <li class="divider"></li>
              <li>
                <a href="{!! route('logout') !!}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                  <i class="icon-key"></i> @lang('ui.logout') </a>

                <form id="logout-form" action="{!! route('logout') !!}" method="POST" style="display: none;">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
              </li>
            </ul>
          </li>
          <!-- END USER LOGIN DROPDOWN -->
        </ul>
      </div>
      <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END PAGE TOP -->
  </div>
  <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->

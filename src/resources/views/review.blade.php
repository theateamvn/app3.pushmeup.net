@extends('layouts.app')

@section('page-title')
  <h1>Reviews
  </h1>
@endsection

@section('css')
  <link href="https://file.myfontastic.com/n6vo44Re5QaWo8oCKShBs7/icons.css" rel="stylesheet">
@endsection

@section('content')
  <div class="row">
      <review-list></review-list>
  </div>
@endsection

@section('scripts')
  <script>
//    App.initSlimScroll('.scroller');
  </script>
@endsection

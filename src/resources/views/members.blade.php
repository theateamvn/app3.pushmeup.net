@extends('layouts.app')

@section('page-title')
  <h1>Members
    <small>management</small>
  </h1>
@endsection


@section('content')
  <member-list></member-list>
@endsection

@section('scripts')
@endsection
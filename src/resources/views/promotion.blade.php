@extends('layouts.app')

@section('page-title')
  <h1>Promotion
    <small>management</small>
  </h1>
@endsection


@section('content')
  <promotion-list></promotion-list>
@endsection

@section('scripts')
@endsection
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<div style="background-color:#f5f5f5;width:100%;padding:10px;font-family:'Open Sans';">
    <div class="logo" style="padding: 20px;">
        <img class="header-logo" src="{{$message->embed(asset('/assets/layouts/layout4/img/logo-blue.png'))}}"
             alt="" style="height: 70px;margin: 0 auto;display: block;">
    </div>
    <div class="wrapper" style="width: 600px;background-color:#ffffff;margin:0 auto;padding: 20px;">
        <div class="content"
             style="color: #848484;text-align: left;padding-left: 10px;padding-right: 10px;height: 300px;">
            <div class="content-wrapper">
                <p style="color: #848484;">Dear <b style="color: #848484;text-decoration: none;">{{$user->full_name}}
                        ,</b>
                </p>
                <p style="color: #848484;">Thank you for choosing us, I know your brand's repution is very important so
                    that all we try our best to help you. Your success business is our success business.</p>
                <p style="color: #848484;">
                    Your account: <b style="color: #848484;text-decoration: none;">{{$user->username}}</b><br>
                    Email:
                    <b style="color: #848484;text-decoration: none;">
                      <a href="mailto:{{$user->email}}" style="color: #848484;text-decoration: none;">{{$user->email}}</a>
                    </b><br>
                    Login your account:
                    <b style="color: #848484;text-decoration: none;">
                      <a href="{{config('app.url')}}" style="color: #848484;text-decoration: none;">{{config('app.url')}}</a>
                    </b>
                </p>
                <p style="color: #848484;">Let me help you to push your brand's repution today. <b
                            style="color: #848484;text-decoration: none;"><a href="{{route('login')}}"
                                                                             style="color: #848484;text-decoration: none;">Login
                            your account</a></b></p>
            </div>
        </div>
    </div>
    <div class="ft" style="display: block;width: 100%;clearfix: both;;text-align:center">
        <div class="footer-left" style="display: block;padding: 10px;"><a href="{{config('app.url')}}"
                                                            style="color: #848484;text-decoration: none;padding-top: 10px">www.PushMeUp.net
                | Push your branch UP today</a>
        </div>
    </div>
</div>

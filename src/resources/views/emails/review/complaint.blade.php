<?php
/** @var \App\Models\Feedback $feedback */
?>
<b>User name typing:</b> <br>
{{$feedback->name}} <br>
<b>User email typing:</b> <br>
{{$feedback->email}} <br>
<b>User phone typing:</b> <br>
{{$feedback->phone}} <br>
<b>Message user typing:</b> <br>
{{$feedback->content}} <br>
<b>Store info:</b> {{$feedback->store->name}} <br>
<b>Date:</b> [{{(new DateTime())->format('Y-m-d H:i:s')}}] <br>
<b>Zone:</b> {{config('app.url')}} <br>
<b>Message user typing:</b> <br>
{{$content}} <br>
<b>Ticket support from:</b> {{$user->username}} - {{$user->email}} <br>
<b>Date:</b> [{{(new DateTime())->format('Y-m-d H:i:s')}}] <br>
<b>Zone:</b> {{config('app.url')}} <br>
@extends('layouts.admin.app')

@section('page-title')
  <h1>Dashboard
    <small>Overview</small>
  </h1>
@endsection

@section('content')
  <dashboard-admin-agent-main></dashboard-admin-agent-main>
@endsection

@section('scripts')
  <script src="/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/counterup/jquery.counterup.js" type="text/javascript"></script>
@endsection

@extends('layouts.admin.app')

@section('page-title')
  <h1>Users
    <small>management</small>
  </h1>
@endsection

@section('content')
  <user-list></user-list>
@endsection

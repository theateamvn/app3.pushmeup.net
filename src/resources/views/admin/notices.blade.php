@extends('layouts.admin.app')

@section('page-title')
  <h1>Notices
    <small>management</small>
  </h1>
@endsection

@section('content')
  <notice-list></notice-list>
@endsection

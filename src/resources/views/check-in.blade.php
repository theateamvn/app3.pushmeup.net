<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8"/>
    <title>{{ config('app.name', 'Laravel') }} | Checkin</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- FACEBOOK OPENGRAPH CRAWLER -->
    <meta property="og:url"
          content="{{Request::url()}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Review for {{$store->name}}"/>
    <meta property="og:description" content="{{$store->invitation_introducing}}"/>
    <meta property="og:image"
          content="{{$store->company_logo?$store->company_logo:asset('/assets/global/img/logo-pushmeup.jpg')}}"/>

    <!-- TWITTER OPENGRAPH CRAWLER -->
    <meta property="twitter:card" content="summary"/>
    <meta property="twitter:description" content="{{$store->invitation_introducing}}"/>
    <meta property="twitter:title" content="Checkin for {{$store->name}}"/>
    <meta property="twitter:image" content="{{$store->company_logo?$store->company_logo:asset('/assets/global/img/logo-pushmeup.jpg')}}"/>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/css/checkin.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="../assets/global/css/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="../assets/global/css/slick/slick-theme.css" />
    <!-- END PAGE LEVEL STYLES -->

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <script>
        window.AppData = <?php echo json_encode([
            'store_id' => (Auth::check() && Auth::user()->store()) ? Auth::user()->store()->id : '',
            'userId' => Auth::check() ? Auth::user()->id : '',
            'timeZone' => Auth::check() ? Auth::user()->time_zone : '',
            'baseUrl' => url(''),
            'tourSteps' => [],
            'tourPageID' => ''
        ]); ?>
    </script>

    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<body class=" login" style="background: url('{!! $store->review_redirect_background !!}') 0 0 no-repeat;background-size: 100%;">
<!-- BEGIN LOGIN -->
<div class="page-wrap1" id="app">
    <check-in-form :store="{{ json_encode($store) }}"
        :review-providers="{{ json_encode($reviewProviders) }}"
        :mode="{{json_encode($mode)}}"
        :feedback_message="{{json_encode($feedback_message)}}"></check-in-form>
</div>
<script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
<script src="/assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../assets/global/css/slick/slick.min.js"></script>
<script src="/js/app.js"></script>
<script>
    window.onbeforeunload = renderLoading;
    function renderLoading() {
        Pace.stop()
        Pace.bar.render();
    }
    $(document).ready(function () {
        //Slide-Mobile
        $('.single-item-mb').slick({
            infinite: false,
            adaptiveHeight: true,
            slidesToShow: 1.25,
            slidesToScroll: 1
        });
        //Slide-Desktop
        $('.single-item-desk').slick({
            infinite: false,
            adaptiveHeight: true,
            vertical: true,
            autoplay: true,
            autoplaySpeed: 2000,
            nextArrow: '<span class="glyphicon  glyphicon-menu-down"></span>',
            prevArrow: '<span class="glyphicon glyphicon-menu-up"></span>',
            slidesToShow: 1,
            slidesToScroll: 1
        });
        
    });
</script>
<?php
$setting = \App\Models\MasterSetting::where('key', 'google_analytic')->first();
$google_analytic = null;
if ($setting) {
    $google_analytic = $setting->value;
}
?>
{!! $google_analytic !!}
<!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>

@extends('layouts.app')

@section('page-title')
  <h1>Birthday
    <small>management</small>
  </h1>
@endsection


@section('content')
  <birthday-list></birthday-list>
@endsection

@section('scripts')
@endsection
@extends('layouts.app')

@section('page-title')
  <h1>Promotion At Risk</h1>
@endsection


@section('content')
  <promotionatrisk-list></promotionatrisk-list>
@endsection

@section('scripts')
@endsection
@extends('layouts.auth')

@section('title')
    Password Reset
@endsection
@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">

                    <!-- BEGIN FORGOT PASSWORD FORM -->
                    <form class="login-form" action="{!! url('/password/email') !!}" method="post">
                        {!! csrf_field() !!}
                        <div class="form-group has-feedback {!! $errors->has('email') ? ' has-error' : '' !!}">
                            <label class="control-label visible-ie8 visible-ie9">Email</label>
                            <input class="form-control placeholder-no-fix" type="text" autocomplete="off"
                                   name="email"/>
                            @if ($errors->has('email'))
                                <span class="help-block">
                    <strong>{!! $errors->first('email') !!}</strong>
                </span>
                            @endif</div>
                        <div class="form-actions">
                            {{--<button type="button" id="back-btn" class="btn btn-default">Back</button>--}}
                            <button type="submit" class="btn btn-primary uppercase pull-right">Submit</button>
                        </div>
                    </form>
                    </form>
                    <!-- END REGISTRATION FORM -->
                </div>
            </div>
        </div>
    </div>
@endsection

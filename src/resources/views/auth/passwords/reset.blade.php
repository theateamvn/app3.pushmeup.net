@extends('layouts.auth')

@section('title')
    Password Reset
@endsection
@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="login-form" method="post" action="{!! url('/password/reset') !!}">
                        {!! csrf_field() !!}

                        <input type="hidden" name="token" value="{!! $token !!}">

                        <div class="form-group has-feedback {!! $errors->has('email') ? ' has-error' : '' !!}">
                            <input type="email" class="form-control" name="email" value="{!! old('email') !!}"
                                   placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="help-block">
                    {!! $errors->first('email') !!}
                </span>
                            @endif
                        </div>

                        <div class="form-group has-feedback{!! $errors->has('password') ? ' has-error' : '' !!}">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                    {!! $errors->first('password') !!}
                </span>
                            @endif
                        </div>

                        <div class="form-group has-feedback{!! $errors->has('password_confirmation') ? ' has-error' : '' !!}">
                            <input type="password" name="password_confirmation" class="form-control"
                                   placeholder="Confirm password">

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                    {!! $errors->first('password_confirmation') !!}
                </span>
                            @endif
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn red btn-block btn-flat pull-right">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

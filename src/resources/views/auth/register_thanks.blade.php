@extends('layouts.auth')

@section('css')
    <style>
        .register-thanks h4, p {
            color: #5C98BD
        }
        .register-thanks .content{
            padding: 40px
        }
    </style>
@endsection

@section('content')
    <div class="row register-thanks">
        <div class="col-md-4 col-md-offset-4">
            <div class="row center-wrap">
                <div class="col-md-12 content">
                    <h4>Your account has been created. Now this is the time for pushing your brand UP</h4>

                    <p>
                        If you need our help, please contact us via: <br>
                        Email: {!! env('MAIL_FROM_ADDRESS') !!} <br>
                        Facebook Fanpage:
                    </p>

                    <div class="row text-center button">
                        <a class="btn btn-primary" href="{!! route('login') !!}">LOGIN YOUR ACCOUNT</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
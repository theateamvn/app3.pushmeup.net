@extends('layouts.app')

@section('page-title')
    <h1>Setting Checkin
    </h1>
@endsection

@section('content')
    @if(empty(Auth::user()->store()))
        <div class="note note-danger">
            <p> @lang('messages.note_store_must_config') </p>
        </div>
    @endif
    <checkin-setting></checkin-setting>
    <sms-checkin-content></sms-checkin-content>
    <email-checkin-content></email-checkin-content>
@endsection

@section('scripts')
    <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
    <script src="../assets/pages/scripts/modernizr.custom.js" type="text/javascript"></script>
@endsection

@section('css')
  <link href="../assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
@endsection
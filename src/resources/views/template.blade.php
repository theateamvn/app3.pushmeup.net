@extends('layouts.app')

@section('page-title')
  <h1>Email & SMS/MMS templates
  </h1>
@endsection

@section('content')
  @if(empty(Auth::user()->store()))
    <div class="note note-danger">
      <p> @lang('messages.note_store_must_config') </p>
    </div>
  @endif
  <div class="row">
    <div class="col-md-6">
      <mms-content></mms-content>
    </div>
    <div class="col-md-6">
      <sms-content></sms-content>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <email-content></email-content>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
  <script src="../assets/pages/scripts/modernizr.custom.js" type="text/javascript"></script>
@endsection

@section('css')
  <link href="../assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
@endsection

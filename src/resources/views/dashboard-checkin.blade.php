@extends('layouts.app')

@section('page-title')
  <h1>Checkin
    <small>Check in</small>
  </h1>
@endsection

@section('content')

<dashboard-checkin-main inline-template>
  <div class="portlet" id="dashboard-block">
    <div class="row widget-row" id="step-1">
        <div class="col-md-3">
            <counter-number-portlet v-bind:number="model.totalMember"
                                    icon="icon-star  bg-green"
                                    subtitle="members"
                                    link="/members"
                                    title="TOTAL MEMBERS"></counter-number-portlet>
        </div>
        <div class="col-md-3">
            <counter-number-portlet v-bind:number="model.totalCheckinDate"
                                    icon="icon-layers bg-red"
                                    subtitle="On Date"
                                    link="#"
                                    title="CHECKIN"></counter-number-portlet>
        </div>
        <div class="col-md-3">
            <counter-number-portlet v-bind:number="model.totalCheckinMonth"
                                    icon="icon-calendar bg-yellow"
                                    subtitle="This Month"
                                    title="CHECKIN"></counter-number-portlet>
        </div>
        <div class="col-md-3">
            <counter-number-portlet v-bind:number="model.totalCheckin"
                                    icon="icon-notebook bg-blue"
                                    subtitle="All"
                                    title="TOTAL CHECKIN"></counter-number-portlet>
        </div>
    </div>
    <div class="row" id="step-2">
        <div class="col-xs-12">
            <checkin-list></checkin-list>
        </div>
    </div>
</div>
</dashboard-checkin-main>

@endsection

@section('css')
  <link href="/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
  <link href="/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
  <link href="/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
  <script src="/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/counterup/jquery.counterup.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
@endsection

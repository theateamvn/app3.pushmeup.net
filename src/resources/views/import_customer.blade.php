@extends('layouts.app')

@section('page-title')
    <h1>Import customer
    </h1>
@endsection

@section('content')
    <import-customer></import-customer>
@endsection

@section('scripts')
@endsection

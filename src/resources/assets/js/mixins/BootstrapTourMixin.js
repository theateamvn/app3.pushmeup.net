/**
 * Created by acesi on 18/12/2016.
 */
export default {
    methods: {
        startTourGlobal(){
            eventHub.$emit('START_TOUR');
        },
        startTourGlobalFirst(tourSteps, pageID){
            this.setTourGlobal(tourSteps, pageID)
            eventHub.$emit('START_TOUR_FIRST');
        },
        setTourGlobal(tourSteps, pageID){
            AppData.tourSteps = tourSteps;
            AppData.tourPageID = pageID;
        },
        resetTour(){
           eventHub.$emit('CLEAR_TOUR');
        }
    }
}
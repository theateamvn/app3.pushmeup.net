/**
 * Created by Lai Vu on 3/20/2017.
 */
import moment from 'moment';
export default {
    methods: {
        moment(date){
            return moment(date);
        },
        formatDate(date){
            return moment(date).format('MM/DD/YY - hh:mm (A)');
        }
    }
}
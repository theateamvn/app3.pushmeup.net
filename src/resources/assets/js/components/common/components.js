/**
 * Created by acesi on 19/11/2016.
 */
Vue.component('response-alert',require('./ResponseAlert.vue'));
Vue.component('send-request',require('./SendRequest.vue'));
Vue.component('form-error',require('./FormError.vue'));
Vue.component('modal',require('./Modal.vue'));
Vue.component('image-input',require('./ImageInput.vue'));
Vue.component('image-small-input',require('./ImageSmallInput.vue'));
Vue.component('boostrap-tour',require('./BootstrapTour.vue'));
Vue.component('view-tour-button',require('./ViewTourButton.vue'));  
Vue.component('time-range',require('./TimeRange.vue'));
Vue.component('month-picker',require('./MonthPicker.vue'));
Vue.component('support-and-request',require('./SupportAndRequest.vue'));

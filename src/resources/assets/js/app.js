/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component(
  'passport-clients',
  require('./components/passport/Clients.vue')
);

Vue.component(
  'passport-authorized-clients',
  require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
  'passport-personal-access-tokens',
  require('./components/passport/PersonalAccessTokens.vue')
);

require('./components/setting/components');
require('./components/setting_checkin/components');
require('./components/common/components');
require('./components/review/components');
require('./components/bad-review/components');
require('./components/customer/components');
require('./components/send_sms/components');
require('./components/dashboard/components');
require('./components/template/components');
require('./components/admin/dashboard/components');
require('./components/admin/dashboard-agent/components');
require('./components/admin/background/components');
require('./components/admin/settings/components');
require('./components/review_redirect/components');
require('./components/feedback/components');
require('./components/check_in/components');
require('./components/members/components');
require('./components/promotions/components');
require('./components/promotion_at_risk/components');
require('./components/dashboard_checkin/components');
require('./components/birthdays/components');

Vue.component('user-list',      require('./components/admin/user/List.vue'));
Vue.component('birthday-list',  require('./components/birthdays/List.vue'));
Vue.component('promotion-list', require('./components/promotions/List.vue'));
Vue.component('promotionatrisk-list', require('./components/promotion_at_risk/List.vue'));
Vue.component('member-list',    require('./components/members/List.vue'));
Vue.component('profile-show',   require('./components/profile/Show.vue'));
Vue.component('notice-list',    require('./components/admin/notice/List.vue'));
Vue.component('notices',        require('./components/common/Notices.vue'));
Vue.component('import-customer', require('./components/common/ImportCustomer.vue'));

console.log('%c WELCOME TO ALIOSTEK TEAM! ', 'background: #9CB8B3; color: #ffffff', 'Developer area!');

const app = new Vue({
  el: '#app'
});

window.showToast = function (title, message, type) {
  toastr[type](message, title);
}

window.showToastFromResponse = function (response) {
  var message = "";
  var type = "success";
  if (response) {
    if (response.status < 200 || response.status > 299) {
      if (response.data && response.data.message) {
        message = response.data.message
      }
      if (response.data && response.data.error) {
        message = response.data.error
      }
      type = 'error';
    } else {
      if (response.data && response.data.message) {
        message = response.data.message;
        type = 'success'
      }
    }
    window.showToast('',message,type);
  }
};
